package org.arm.crse.sq.testing.dtesting.testsuite.blackbox;

import org.arm.crse.sq.testing.dtesting.example.Triangle;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

class TriangleTest {
    Triangle testObj;

    @BeforeEach
    void setUp() {
        testObj = new Triangle();
    }

    @AfterEach
    void tearDown() {
        testObj = null;
    }

    @ParameterizedTest(name = "[{index}] {0}, {1}, {2} => {3}")
    @CsvFileSource(resources = "/usecases/blackbox/triangle_ecp_and_bva.csv")
    @DisplayName("getType in Triangle: (int x , int y ,int z) => (String expect)")
    void getTypeTest(int x, int y, int z, String expect) {
        String actual = testObj.getType(x, y, z);
        Assertions.assertEquals(expect, actual);
    }
}