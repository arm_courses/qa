package org.arm.crse.sq.blackbox;

import org.arm.crse.sq.Triangle;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

class TriangleTest {
    Triangle testObj;

    @BeforeEach
    void setUp() {
        testObj = new Triangle();
    }

    @AfterEach
    void tearDown() {
        testObj = null;
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/usecases/blackbox/triangle_ecp_and_bva.csv")
    void getTypeTest(int x, int y, int z, String expect) {
        String actual = testObj.getType(x, y, z);
        Assertions.assertEquals(expect, actual);
    }
}