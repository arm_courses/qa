package org.arm.crse.sq.evolution.step06.suite.categorized;

import org.arm.crse.sq.evolution.step04.categorized.BVATesting;
import org.arm.crse.sq.evolution.step04.categorized.BloodPressureTest;
import org.arm.crse.sq.evolution.step04.categorized.BodyMassIndexTest;
import org.arm.crse.sq.evolution.step04.categorized.ECPTesting;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Categories.class)
@Categories.IncludeCategory({ECPTesting.class, BVATesting.class})
@Suite.SuiteClasses({BodyMassIndexTest.class, BloodPressureTest.class})
public class TestSuite {
}
