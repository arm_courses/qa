package org.arm.crse.sq.evolution.test;

import org.arm.crse.sq.evolution.BodyMassIndex;

/**
 * 1. 使用test目录下放置测试代码，调用"main"函数
 * 2. 自动设置输入值（身高、体重）
 * 3. 自动运行被测方法
 * 4. 自动比较实际值和预期值
 * 5. 自动记录执行过程和测试结果
 */
public class TestScheme03 {
    BodyMassIndex testObj;//被测类

    //定义构造方法
    public TestScheme03() {
        testObj = null;
    }

    public static void main(String[] args) {
        TestScheme03 test = new TestScheme03();
        test.testGetBMIType2();
        test.testGetBMIType4();
        test.testGetBMIType1();
        test.testGetBMIType3();
    }

    //创建被测对象
    public void createObject(double w, double h) {
        testObj = new BodyMassIndex(w, h);
    }

    //销毁被测对象
    public void destroyObject() {
        testObj = null;
    }

    //校验执行结果：预期输出和实际输出
    public boolean verifyResult(String expect, String actual) {
        return expect == actual;
    }

    //记录执行过程：测试结果，输入，预期输出，实际输出
    public String recordResult(boolean testResult, double w, double h, String expect, String actual) {
        //如果测试结果是通过，则直接返回PASS，否则，返回FAIL，并记录相关数据
        String output = "";
        if (testResult) {
            output += "PASS.";
        } else {
            output += "FAIL." +
                    " 体重：" + w +
                    "，身高：" + h +
                    "，预期：" + expect +
                    "，实际输出：" + actual;
        }
        return output;
    }

    //定义测试方法
    // 测试用例1：45，1.6
    public void testGetBMIType1() {
        //1.创建被测对象
        createObject(45.0, 1.6);

        //2.调用被测方法
        String actual = testObj.getLevel();

        //3.校验执行结果
        String expected = "偏瘦";
        boolean testResult = verifyResult(expected, actual);

        //4.记录执行过程
        String output = recordResult(testResult, 45.0, 1.6, expected, actual);

        //5.结果输出到屏幕
        System.out.println(output);

        //6.销毁被测对象
        destroyObject();
    }

    // 测试用例2:55.0,1.6
    public void testGetBMIType2() {
        createObject(55.0, 1.6);
        String actual = testObj.getLevel();
        String exp = "正常";
        boolean tr = verifyResult(exp, actual);
        String out = recordResult(tr, 55.0, 1.6, exp, actual);
        System.out.println(out);
        destroyObject();
    }

    // 测试用例3:68.0,1.6
    public void testGetBMIType3() {
        createObject(68.0, 1.6);
        String actual = testObj.getLevel();
        String exp = "偏胖";
        boolean tr = verifyResult(exp, actual);
        String out = recordResult(tr, 68.0, 1.6, exp, actual);
        System.out.println(out);
        destroyObject();
    }

    // 测试用例4：80.0,1.6
    public void testGetBMIType4() {
        createObject(80.0, 1.6);
        String actual = testObj.getLevel();
        String exp = "肥胖";
        boolean tr = verifyResult(exp, actual);
        String out = recordResult(tr, 80.0, 1.6, exp, actual);
        System.out.println(out);
        destroyObject();
    }
}
