package org.arm.crse.sq.evolution.step02.independent;

import org.arm.crse.sq.evolution.BodyMassIndex;
import org.junit.*;

import static org.junit.Assert.*;

public class BodyMassIndexTest {

    private BodyMassIndex testObj;

    @BeforeClass
    public static void prepareEnvironment() {
        System.out.println("Run @BeforeClass method");
    }

    @AfterClass
    public static void RestoreEnvironment() {
        System.out.println("Run @AfterClass method");
    }

    //TODO: how to improve it
    @Before
    public void setUp() throws Exception {
        testObj = new BodyMassIndex();
        System.out.println();
        System.out.println("Run @Before method at memory address ==> " + System.identityHashCode(this));
    }

    //TODO: how to improve it
    @After
    public void tearDown() throws Exception {
        testObj = null;
        System.out.println("Run @After method");
    }

    @Test
    public void testGetBMIType_Thin() {
        testObj.setValues(45.0, 1.6);
        String expected = "偏瘦";
        assertEquals(expected, testObj.getLevel());
    }

    @Test
    public void testGetBMIType_Normal() {
        testObj.setValues(55.0, 1.6);
        String expected = "正常";
        assertEquals(expected, testObj.getLevel());
    }

    @Test
    public void testGetBMIType_SlightlyFat() {
        testObj.setValues(68.0, 1.6);
        String expected = "偏胖";
        assertEquals(expected, testObj.getLevel());
    }

    @Test
    public void testGetBMIType_Fat() {
        testObj.setValues(80.0, 1.6);
        String expected = "肥胖";
        assertEquals(expected, testObj.getLevel());
    }
}