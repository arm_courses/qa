package org.arm.crse.sq.evolution.step04.categorized;

import org.arm.crse.sq.evolution.BodyMassIndex;
import org.junit.*;
import org.junit.experimental.categories.Category;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BodyMassIndexTest {
    BodyMassIndex testObj;//被测类

    @BeforeClass
    public static void prepareEnvironment() {
        System.out.println("Run @BeforeClass method");
    }

    @AfterClass
    public static void RestoreEnvironment() {
        System.out.println("Run @AfterClass method");
    }

    @Before
    public void setUp() throws Exception {
        testObj = new BodyMassIndex();
        System.out.println("Run @Before method");
    }

    @After
    public void tearDown() throws Exception {
        testObj = null;
        System.out.println("Run @After method");
    }
    
    @Test
    @Category({ECPTesting.class})
    public void testGetTypeECP(){
        System.out.println("running testGetTypeECP");
    }

    @Category({BVATesting.class})
    @Test
    public void testGetTypeBVA(){

    }

    @Category({ECPTesting.class})
    @Test
    public void getBMIType_Thin() {
        System.out.println("Run getLevel");
        testObj.setValues(45.0, 1.6);
        String actual = testObj.getLevel();
        String expected = "偏瘦";
        assertEquals(expected, actual);
    }

    @Category({ECPTesting.class})
    @Test
    public void getBMIType_Normal() {
        System.out.println("Run getBMIType_Normal");
        testObj.setValues(55.0, 1.6);
        String exp = "正常";
        assertEquals(exp, testObj.getLevel());
    }

    @Category({ECPTesting.class})
    @Test
    public void getBMIType_SlightlyFat() {
        System.out.println("Run getBMIType_SlightlyFat");
        testObj.setValues(68.0, 1.6);
        String exp = "偏胖";
        assertEquals(exp, testObj.getLevel());
    }

    @Category({ECPTesting.class})
    @Test
    public void testGetBMIType_SlightlyFat() {
        System.out.println("Run testGetBMIType_SlightlyFat");
        testObj.setValues(68.0, 1.6);
        String exp = "偏胖";
        assertEquals(exp, testObj.getLevel());
    }

    @Category({ECPTesting.class})
    @Test
    public void testGetBMIType_Fat() {
        System.out.println("Run testGetBMIType_Fat");
        testObj.setValues(80.0, 1.6);
        String exp = "肥胖";
        assertEquals(exp, testObj.getLevel());
    }

    @Category({BVATesting.class})
    @Test
    public void testGetBMIType_Boundary1() {
        testObj.setValues(47.10, 1.6);
        String exp = "偏瘦";
        assertEquals(exp, testObj.getLevel());
    }

    @Category({BVATesting.class})
    @Test
    public void testGetBMIType_Boundary2() {
        testObj.setValues(47.37, 1.6);
        String exp = "正常";
        assertEquals(exp, testObj.getLevel());
    }

    @Category({BVATesting.class})
    @Test
    public void testGetBMIType_Boundary3() {
        testObj.setValues(61.18, 1.6);
        String exp = "正常";
        assertEquals(exp, testObj.getLevel());
    }

    @Category({BVATesting.class})
    @Test
    public void testGetBMIType_Boundary4() {
        testObj.setValues(61.45, 1.6);
        String exp = "偏胖";
        assertEquals(exp, testObj.getLevel());
    }

    @Category({BVATesting.class})
    @Test
    public void testGetBMIType_Boundary5() {
        testObj.setValues(71.42, 1.6);
        String exp = "偏胖";
        assertEquals(exp, testObj.getLevel());
    }

    @Category({BVATesting.class})
    @Test
    public void testGetBMIType_Boundary6() {
        testObj.setValues(71.69, 1.6);
        String exp = "肥胖";
        assertEquals(exp, testObj.getLevel());
    }

    @Category({BVATesting.class})
    @Test
    public void testGetBMIType_Boundary7() {
        testObj.setValues(71.94, 1.6);
        String exp = "肥胖";
        assertEquals(exp, testObj.getLevel());
    }
}
