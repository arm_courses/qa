package org.arm.crse.sq.evolution.step03.parameterized.ci;

import org.arm.crse.sq.evolution.BodyMassIndex;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class BodyMassIndexTest {
    //定义被测类
    BodyMassIndex testObj;
    private final double weight;
    private final double height;
    private final String expected;

    public BodyMassIndexTest(double weight, double height, String expected) {
        this.weight = weight;
        this.height = height;
        this.expected = expected;
    }

    //准备测试数据集
    @Parameterized.Parameters(name = "{index}:getLevel[{0},{1}]==[{2}]")
    public static Collection testDataset() {
        return Arrays.asList(
            new Object[][]{
                {45.0, 1.6, "偏瘦"},
                {55.0, 1.6, "正常"},
                {68.0, 1.6, "偏胖"},
                {80.0, 1.6, "肥胖"}
            }
        );
    }

    @Before
    public void setUp() throws Exception {
        testObj = new BodyMassIndex(weight, height);
    }

    @After
    public void tearDown() throws Exception {
        testObj = null;
    }

    @Test
    public void testGetType() {
        assertEquals(expected, testObj.getLevel());
    }
}
