package org.arm.crse.sq.evolution.step07.parameterized.categorized;

import org.arm.crse.sq.evolution.BloodPressure;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * Constructor Inject
 */
@RunWith(Parameterized.class)
@Category(SampleCategory.class)
public class BloodPressureTest {

    private final int systolicPressure;
    private final int diastolicPressure;
    private final String expected;
    private BloodPressure testObj;

    public BloodPressureTest(int systolicPressure, int diastolicPressure, String expected) {
        this.systolicPressure = systolicPressure;
        this.diastolicPressure = diastolicPressure;
        this.expected = expected;
    }

    @Parameterized.Parameters(name = "{index}:getLevel[{0},{1}]==[{2}]")
    public static Collection testDataset() {
        return Arrays.asList(
            new Object[][]{
                {100, 70, "正常"},
                {130, 85, "正常高值"},
                {150, 95, "1级高血压"},
                {170, 105, "2级高血压"},
                {190, 120, "3级高血压"}
            }
        );
    }

    @Before
    public void setUp() throws Exception {
        testObj = new BloodPressure(systolicPressure, diastolicPressure);
    }

    @After
    public void tearDown() throws Exception {
        testObj = null;
    }

    @Test
    public void testGetLevel() {
        assertEquals(expected, testObj.getLevel());
    }
}