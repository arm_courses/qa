package org.arm.crse.sq.evolution.step03.parameterized.mi;

import org.arm.crse.sq.evolution.BodyMassIndex;
import org.junit.*;

import static org.junit.Assert.assertEquals;

public class BodyMassIndexTest {

    private BodyMassIndex testObj;

    @BeforeClass
    public static void prepareEnvironment() {
        System.out.println("Run @BeforeClass method");
    }

    @AfterClass
    public static void RestoreEnvironment() {
        System.out.println("Run @AfterClass method");
    }

    @Before
    public void setUp() throws Exception {
        testObj = new BodyMassIndex();
        System.out.println("Run @Before method");
    }

    @After
    public void tearDown() throws Exception {
        testObj = null;
        System.out.println("Run @After method");
    }

    //FIXME: the JUnit will recognize only one test case
    @Test
    public void getBMIType() {
        double[][] testData = {{45.0, 1.6}, {55.0, 1.6}, {68.0, 1.6}, {80.0, 1.6}};
        String[] expRets = new String[]{"偏瘦", "正常", "偏胖", "肥胖"};

        for (int i = 0; i < testData.length; ++i) {
            System.out.println("Running testcase:" + i);
            testObj.setValues(testData[i][0], testData[i][1]);
            assertEquals(testObj.getLevel(),expRets[i]);
        }
    }
}