# 软件质量：质量保证与测试

## 目录结构

```sh {.line-numbers}
.
├── CP01_Overview
├── CP02_SQC
│   ├── CS01_SQC_Approaches
│   │   ├── 01_Static_Testing
│   │   ├── 02_Dynamic_Testing
│   │   ├── 03_Black_Box_Testing
│   │   ├── 04_White_Box_Testing
│   │   ├── 05_Grey_Box_Testing
│   │   ├── 06_Manual_Testing
│   │   ├── 07_Automation_Testing
│   │   └── 08_Other_Methods
│   ├── CS02_SQC_Objects
│   │   ├── 01_Functional_Testing
│   │   ├── 02_Non-Functional_Testing
│   │   ├── 03_Program_Testing
│   │   ├── 04_Document_Testing
│   │   └── 05_Data_Testing
│   └── CS03_SQC_Phases
│       ├── 01_Unit_Testing
│       ├── 02_Integration_Testing
│       ├── 03_System_Testing
│       ├── 04_Acceptance_Testing
│       └── 05_Regression_Testing
├── CP03_SQA
│   ├── CS01_SQA_Approaches
│   │   ├── 01_Standard
│   │   ├── 02_Metric
│   │   ├── 03_Baseline
│   │   └── 04_Review
│   └── CS02_SQA_Models
│       ├── 01_ISO9001
│       └── 02_CMMI
├── CP04_SQM
│   ├── CS01_SQM_Approaches
│   │   ├── 01_PDCA
│   │   └── 02_Risk_Management
│   ├── CS02_SQM_Objects
│   │   ├── 01_Organization
│   │   └── 02_SDLC
│   └── CS03_SQM_Models
│       ├── 01_TQM
│       ├── 02_Six-Sigma
│       └── 03_QFD
└── EA01_Terminology
```

文件前缀含义：

1. `CP(Content Part)`
1. `CC(Content Chapter)`
1. `CS(Content Section)`
1. `EA(Extra Appendix)`
