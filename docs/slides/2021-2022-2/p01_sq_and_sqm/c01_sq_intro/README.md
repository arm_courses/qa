---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Introduction to Software Quality_"
footer: "@aRoming"
math: katex
---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Introduction to Software Quality软件质量概述

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [⭐ 软件与软件行业](#软件与软件行业)
2. [🌟 软件质量与软件缺陷](#软件质量与软件缺陷)
3. [🌟 软件质量度量、软件质量模型与软件缺陷模型](#软件质量度量-软件质量模型与软件缺陷模型)
4. [⭐ 软件质量管理与PDCA](#软件质量管理与pdca)
5. [🌟 软件质量保证与软件质量控制](#软件质量保证与软件质量控制)
6. [⭐ 软件企业的组织架构](#软件企业的组织架构)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 课程特点与学习方法

---



---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## ⭐ 软件与软件行业

---

🤔 为什么软件这么特别，有专门的"软件危机" 🤔

---

### 软件危机的独特原因

1. 需求难于无歧义描述
    1. 用户难于准确描述需求，开发难于理解用户的实质需求
    1. 要求使用形式化语言描述需求不现实
1. 需求变更代价"低"
    1. 软件需求变更的成本主要体现在用户或客户"看不见"的成本（时间）
    1. 要求用户或客户理解软件需求变更背后涉及的复杂度难度大

---

1. 难于工件化/制品化`artifact`
    1. 软件功能强大、逻辑灵活，每个组件都"独特"
    1. 软件与具体业务紧密相关，具体业务各异从而致使每个组件都"独特"

---

### 软件产品生命周期`SPLC`与软件开发生命周期`SDLC`

- `SPLC` = 软件定义阶段 + 软件开发阶段 + 软件运维阶段
- `SDLC` = 软件定义阶段 + 软件开发阶段
- `SPLC` = `SDLC` + 软件运维阶段
- `SPLC`也称为`SLC(Software Life Cycle, 软件生命周期)`

---

#### 软件定义阶段/软件需求阶段/软件分析阶段

- 集中于 **"做什么"（what to do）/"做对的事情"（do right thing）**
    1. 定义困难 ==> 问题定义
    1. 定义需求 ==> 需求分析
    1. 确认需求 ==> 需求确认

---

#### 软件开发阶段/软件构造阶段

- 集中于 **"如何做"（how to do）/"将事情做对"（do thing right）**
    1. 映射需求 ==> 概要设计、详细设计
    1. 实现需求 ==> 编码
    1. 构建需求 ==> 构建
    1. 验证需求 ==> 测试
    1. 交付需求 ==> 发布

⚠️ 有时将"软件定义阶段 + 设计"合并称为"计划阶段" ⚠️

---

#### 软件运维阶段

- 集中于 **"如何实现价值"（how to achieve）** 和 **"如何发现新需求"（how to upgrade）**
    1. 实现需求价值 ==> 部署、运维（运行+维护）
    1. 发掘新需求 ==> 业务分析、迭代周期
    1. 无法满足需求 ==> 软件退出

---

### `SPLC`, `Agile` and `DevOps`

![height:520](./.assets/image/splc_agile_cicd_devops.png)

---

### Dev, Ops, QA and DevOps

![height:480](./.assets/image/dev_ops_qa_and_devops.png)

❗ ⚠️ 图中的`QA`包括本课程的`SQA`和`SQC` ⚠️ ❗

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 软件质量与软件缺陷

---

- 软件 = 程序 + 数据 + 文档
- 软件质量`software quality(SQ)`(noun)：指软件与明确的和隐含的定义的需求相一致的程度
- 软件缺陷`software defect`(noun)：指存在于软件中的不希望或不可接受的偏差

<!--

### 软件缺陷Software Defect

A `Software Defect/Bug/Fault` is a condition in a software product which _**does not meet a software requirement (as stated in the requirement specifications)**_ or _**end-user expectation (which may not be specified but is reasonable)**_. In other words, a defect is _**an error**_ in _**coding**_ or _**logic**_ that causes a program to malfunction or to produce _**incorrect/unexpected**_ results.

><https://softwaretestingfundamentals.com/defect/>

-->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 软件质量度量、软件质量模型与软件缺陷模型

>1. [quality-models](https://github.com/AstroMatt/book-dev/blob/master/sonarqube/quality-models.rst)

---

1. 测量measure: **_ascertain_** the size, amount, or degree of something
1. 测量法measurement: a **_unit or system_** of measuring
1. 度量metric: a **_system or standard_** of measurement
1. 指标indicator: a thing that indicates the **_state or level_** of something

❗ metric有时也翻译成"指标"，如：monitor metrics监控指标 ❗

![height:260](./.assets/diagram/metric.png)

<!--

1. `measure`：度量（名词），是根据一定的规则赋予软件过程或产品属性的数值或类
1. `measure`：度量（动词），按照度量过程中的过程定义，对软件过程或软件产品实施度量，表示实际的动作
1. `measurement`：测量法（名词），是按照一定的尺度用度量（名词）给软件实体属性赋值的过程的方法
1. `metric`：度量，是已定义的测量方法和测量尺度
1. `indicator`：指标，用于评价或预测其他度量的度量

👉 meter- = to measure 👈
-->

---

1. 质量模型定义了度量体系，包括属性、指标、测试法等，通过测量产品或过程得到数值，进而得到指标值，从而回答质量的好坏
1. 三种度量方法论
    1. 从模型到对象：先建立（选择或创建）质量模型，再按质量模型测量对象
    1. 从对象到模型：先分析对象，再建立质量模型
    1. 结合应用上述方法
1. 软件质量模型分为软件产品质量模型和软件过程质量模型

---

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 从对象到模型

---

#### `GQM(Goal-Question-Metric, 目标问题度量法)`

1. `GQM`是什么
    - `GQM`是一种系统的、定量化的构建度量体系（模型）的方法
    - `GQM`是一种通过确定软件质量目标、并且连续监视这些目标是否达成、从而保证软件质量的方法
1. `GQM`的目标： 客户所希望的质量需求的 **_定量_** 的说明

><https://blog.csdn.net/binnacler/article/details/5776226>

---

#### `GQM`的设计步骤

1. 目标（概念级conceptual level）： 对一个项目的各个方面（产品、过程和资源）规定具体的目标，这些目标的表达应非常明确
1. 问题（操作级operational level）： 对每一个目标，要引出一系列能反映出这个目标是否达到要求的问题，并要求对这些问题进行回答。这些问题的答案将有助于使目标定量化
1. 度量（量化级quantitative level）： 将回答这些问题的答案映射到对软件质量等级的度量上，根据这种度量得出软件目标是否达到的结论，或确认哪些做好了，哪些仍需改善

<!--
1. 数据： 收集数据，要为收集和分析数据做出计划
-->

---

1. G：确定一种新的编程语言Jae的使用效果
1. Q
    1. 使用Jae语言的程序员是谁？
    1. 使用Jae语言编写的软件代码质量如何？
    1. 使用Jae语言编写代码的生产率如何？
1. M
    1. 具有N年编程经验的开发者的百分比
    1. 每千行代码中的缺陷数
    1. 每月编写代码的行数

><https://blog.csdn.net/binnacler/article/details/5776226>

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 软件质量度量

---

#### 度量的目的和价值

- 目的：管理的需要，利用度量改进产品和过程
- 价值
    - 度量已发生的事物是为了进行评估和跟综
    - 度量未发生的事物关联的事物是为了进行预测

👉 无法记录则无法度量，无法度量则无法管理 👈

---

#### 度量对于软件及软件学科的价值

1. 可度量性是一个学科是否达到成熟的一大标志，度量使软件开发逐渐趋向专业、标准和科学
1. **_CMU-SEI: Software Measurement Guidebook_**
    1. 通过软件度量增加理解
    1. 通过软件度量管理软件项目，主要是计划与估算、跟踪与确认
    1. 通过软件度量指导软件过程改善，主要是理解、评估和包装，软件度量对于不同的实施对象，具有不同的效用

👉 度量的作用 ==> 可知、可控 👈

---

#### 度量对软件公司的作用的具体表现

1. 改善产品质量
1. 改善产品交付
1. 提高生产能力
1. 降低生产成本
1. 建立项目估算的基线
1. 了解使用新的软件工程方法和工具的效果和效率
1. 提高顾客满意度
1. 创造更多利润
1. 构筑员工自豪感

---

#### 度量对项目经理的作用的具体表现

1. 分析产品的错误和缺陷
1. 评估现状
1. 建立估算的基础
1. 确定产品的复杂度
1. 建立基线
1. 从实际上确定最佳实践

---

#### 度量对软件开发人员的作用的具体表现

1. 可建立更加明确的作业目标
1. 可作为具体作业中的判断标准
1. 便于有效把握自身的软件开发项目
1. 便于在具体作业中实施渐进性软件开发改善活动

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 软件产品质量模型

---

#### `Bass`

1. 分类方法：one way to classify quality attributes distinguishes those characteristics that are discernible through execution of the software (external quality) from those that are not (internal quality)
1. 该分类方法未包括功能的正确性

>_"Software Requirement (3e) - 14.Beyond functionality"_

---

1. external quality外部质量属性（10个属性）
    - primarily important to users
    - describe characteristics that are observed when the software is executing
1. internal quality内部质量属性（5个属性）
    - more significant to development and maintenance
    - are not directly observed during execution of the software
    - that a developer or maintainer perceives while looking at the design or code to modify it, reuse it, or move it to another platform

---

##### external quality

|quality|description|
|--|--|
|Availability     |The extent      to            which       the system      's services are available when |and where they are needed|
|Installability   |How easy        it     is     to              correctly    install, uninstall, and reinstall the application|
|Integrity        |The extent      to            which       the system       protects against data inaccuracy and loss|
|Interoperability |How easily  the system        can             interconnect and exchange data with other systems or components|
|Performance      |How quickly     and           predictably the system       responds to user inputs or other events|

---

|quality|description|
|--|--|
|Reliability      |How long    the system        runs            before       experiencing a failure|
|Robustness       |How well    the system        responds        to           unexpected operating conditions|
|Safety           |How well    the system        protects        against      injury or damage|
|Security         |How well    the system        protects        against      unauthorized access to the application and its data|
|Usability        |How easy        it     is for people          to           learn, remember, and use the system|

---

##### internal quality

|quality|description|
|--|--|
|Efficiency    |How efficiently the           system     uses      computer  resources                                                              |
|Modifiability |How easy        it         is to         maintain, change  , enhance  , and restructure the system                                  |
|Portability   |How easily      the           system     can       be        made      to work in other operating environments                      |

---

|quality|description|
|--|--|
|Reusability   |To  what        extent        components can       be        used      in other systems                                             |
|Scalability   |How easily      the           system     can       grow      to        handle more users, transactions, servers, or other extensions|
|Verifiability |How readily     developers    and        testers   can       confirm   that the software was implemented correctly                  |

---

#### 🌟 `McCall`

><https://www.geeksforgeeks.org/mccalls-quality-model/>

---

![height:600](./.assets/image/mccall_quality_factors.jpg)

---

1. 3个维度，11个属性

---

##### 产品运行维度Product Operation

1. 正确性`correctness`：满足需求规格和实现用户任务目标的程度
1. 高效性`efficiency`：完成其功能所需的计算资源和代码的程度
1. 完整性`integrity`：对未授权人员访问软件或数据的可控制程度
1. 可靠性`reliability`：在一定时间内、一定条件下无故障地执行功能的能力
1. 可用性`usability`：学习、操作、理解软件功能所需的工作量

---

##### 产品修正维度Product Revision

1. 可维护性`maintainability`：定位和修复程序中一个错误所需的工作量
1. 灵活性`flexibility`：修改一个运行的程序所需的工作量
1. 可测试性`testability`：测试一个程序以确保她完成所期望的功能所需的工作量

---

##### 产品转移维度Product Transition

1. 可移植性`portability`：将一个程序从一个硬件和（或）软件系统环境移植到另一个环境所需的工作量
1. 可复用性`re-usability`：使一个程序可以在另外一个应用程序中复用的程度
1. 互操作性`interoperability`：连接一个系统和另一个系统所需的工作量

---

#### 🌟 `ISO/IEC 9126`(replaced by `ISO/IEC 25010:2011`)

1. `ISO`在`McCall`等人所提出的软件质量模型的基础上，于1991年发布`ISO/IEC 9126`质量模型国际标准
1. `ISO/IEC 9126`：6大类，21个属性six product quality characteristics, and 21 sub-characteristics

1. `ISO/IEC 25010:2011`：8大类，31个属性eight product quality characteristics, and 31 sub-characteristics

---

1. 功能性`functionality`：适合性、准确性、互操作性/互用性、依从性、安全性
1. 可靠性`reliability`：成熟性、容错性、可恢复性
1. 可用性`usability`：可理解性、易学性、易操作性
1. 高效性`efficiency`：时间特性、资源特性
1. 可维护性`maintainability`：可分析性、可改变性、稳定性、可测试性
1. 可移植性`portability`：适应性、可安装性、一致性、可替换性

>[wikipedia.ISO/IEC 9126 Software engineering — Product quality.](https://en.wikipedia.org/wiki/ISO/IEC_9126)

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 软件过程质量模型

---

1. `CMMI`
1. `ISO9000`

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 软件缺陷模型
<!--TODO: update it...-->

---

#### `bug` VS `defect` VS `fault` VS `failure`

1. static concept
    1. `bug臭虫`：指 **_存在于_** 软件（起初专指程序）中的不希望或不可接受的 **_错误_**
    1. `defect缺陷`：指 **_存在于_** 软件中的不希望或不可接受的 **_偏差_**
1. dynamic concept
    1. `fault故障`：指软件 **_运行时_** 出现的不希望或不可接受的  **_内部状态_**
    1. `failure失效`：指软件 **_运行时_** 出现的不希望或不可接受的 **_外部表现_**

---

1. `bug/defect` --> `fault` --> `failure`
1. `bug`和`defect`目前一般视为同义词，`bug`偏口语，`defect`偏书面语

---

#### 软件缺陷的特征Characters of Software Defect

1. 🌟 `has cause有原因`： 缺陷均有原因
1. 🌟 `reproducible可重现性`： 无法重现的缺陷则无法被定位，无法被定位则无法被修复
1. 🌟 `introduce new defects引入新缺陷`： 修复一个缺陷可能会引入新的缺陷
1. `cumulative累积性/magnification放大性`： 缺陷发现得越晚、处理得越晚，付出的代价越大

---

#### 软件缺陷的属性Attributes of Software Defect

1. 🌟 `identifier标识`：唯一标识一个缺陷的标识符（建议使用工具进行管理）
1. ⭐ `title标题`：缺陷的一句话描述
1. `type类型`：根据缺陷自然属性划分的缺陷类型
1. 🌟 `severity严重级`：因缺陷引起的故障的影响程度
1. 🌟 `priority优先级`：缺陷被修复的优先程度
1. 🌟 `status状态`：缺陷当前被跟踪的所处状态

---

1. `origin起源/由来`：首次发现缺陷时所处的阶段
1. `source来源/源头`：产生缺陷的阶段
1. ⭐ `root cause根源/原因`：缺陷产生的根本原因（修复阶段填写❓）

👉 严重级、优先级的级别数不宜过多，一般3至5个级别 👈

---

#### 软件缺陷的主体Body of Software Defect

1. 简要描述
1. 软件及其版本
1. 运行上下文/运行环境
1. 重现步骤
1. 期望结果
1. 实际结果
1. 其他信息

---

##### 软件缺陷的类型

1. 缺陷类型没有公认的、统一的标准
1. 常见的缺陷类型：功能缺陷、用户界面缺陷、文档缺陷、软件配置缺陷、性能缺陷、接口缺陷等

---

##### 软件缺陷的严重级

1. Blocker: Blocks development and/or testing work
1. Critical: crashes, loss of data, severe memory leak
1. Major: major loss of function
1. Minor: minor loss of function, or other problem where easy workaround is present
1. Trivial: cosmetic problem like misspelled words or misaligned text
1. Enhancement: Request for enhancement

---

##### 软件缺陷的严重级的判断

1. 严重级（Severity）：软件缺陷对软件质量的破坏程度，反应其对产品和用户的影响，即此软件缺陷的存在将对软件的功能和性能产生怎样的影响
1. 软件缺陷的严重性的判断 **应从软件最终用户的观点** 做出判断，即判断缺陷的严重性要为用户考虑，考虑缺陷对用户使用造成的恶劣后果的严重性

👉 避免"伪严重" 👈

---

##### 软件缺陷的修复优先级

1. Resolve Immediately： 缺陷必须被立即解决
1. Normal Queue： 缺陷需要正常排队等待修复
1. Not Urgent： 缺陷可以在方便时进行修复

---

##### 软件缺陷的修复优先级的判断

1. 优先级表示修复缺陷的重要程度和应该何时修复，是表示处理和修正软件缺陷的先后顺序的指标，即哪些缺陷需要优先修正，哪些缺陷可以稍后修正
1. 确定软件缺陷优先级，更多的是站在软件开发工程师的角度考虑问题，因为缺陷的修正顺序是个复杂的过程，有些不是纯粹的技术问题，而且开发人员更熟悉软件代码，能够比测试工程师更清楚修正缺陷的难度和风险（❗ 该观点有争议 ❗）

---

>在提交Bug时，一般只定义bug的severity，而将priority交给project leader 或team leader来定义，由他们来决定该bug被修复的优先等级。某种意义上来说，priority的定义要依赖于severity，在大多数情况下，severity越严重，那这个bug的priority就越高。
>
>[Bug的严重程度、优先级如何定义](https://cloud.tencent.com/developer/article/1733135)

---

##### 软件缺陷的状态

1. Submitted： 已提交的缺陷（已提交未排序）
1. Open： 确认"提交的缺陷"，等待处理
1. Rejected： 拒绝"提交的缺陷"，不需要修复或不是缺陷
1. Resolved： 缺陷被修复
1. Closed： 确认被修复的缺陷，将其关闭（确认完成修复）

---

##### 软件缺陷的起源/由来

1. Requirement： 在需求阶段发现的缺陷
1. Architecture： 在构架阶段发现的缺陷
1. Design： 在设计阶段发现的缺陷
1. Code： 在编码阶段发现的缺陷
1. Test： 在测试阶段发现的缺陷

---

##### 软件缺陷的来源/源头

1. Requirement：由于需求的问题引起的缺陷
1. Architecture：由于构架的问题引起的缺陷
1. Design：由于设计的问题引起的缺陷
1. Code：由于编码的问题引起的缺陷
1. Test：由于测试的问题引起的缺陷（测试会引起缺陷吗❓）
1. Integration：由于集成的问题引起的缺陷

---

##### 缺陷的根源/原因

1. 目标：如，错误的范围，误解了目标，超越能力的目标等（应该是"需求"❓）
1. 过程，工具和方法：如，无效的需求收集过程，过时的风险管理过程，不适用的项目管理方法，没有估算规程，无效的变更控制过程等
1. 人： 如，项目团队职责交叉，缺乏培训。没有经验的项目团队，缺乏士气和动机不纯等
1. 缺乏组织和通讯： 如，缺乏用户参与，职责不明确，管理失败等

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## ⭐ 软件质量管理与PDCA

>1. [PDCA](https://en.wikipedia.org/wiki/PDCA)
>1. [Project Management](https://en.wikipedia.org/wiki/Project_management)

---

### PDCA

1. "Plan–Do–Check–Act" or "Plan–Do–Check–Adjust"
1. `PDCA`是管理的基本方法

>1. It is also known as the `Deming circle/cycle/wheel`, the `Shewhart cycle`, the `control circle/cycle`, or `plan–do–study–act (PDSA)`
>1. Later in Deming's career, he modified `PDCA` to "Plan, Do, Study, Act" (`PDSA`) because he felt that "check" emphasized inspection over analysis.

---

![height:600](./.assets/image/pdca_cycle.png)

---

![width:1120](./.assets/image/pdca_multi-loop.png)

---

### Project Management Processes

![width:1120](./.assets/image/pm_phases.png)

---

### SQ and PM Iron Triangle

![height:450](./.assets/image/pm_triangle.png)

>[Iron Triangle — Triple Constraints of Project Management](https://medium.com/@harpreet.dhillon/iron-triangle-triple-constraints-of-project-management-e818e631826c)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 软件质量保证与软件质量控制

>1. [Software Quality Management](https://en.wikipedia.org/wiki/Software_quality_management)
>1. [Software Quality Control](https://softwaretestingfundamentals.com/software-quality-control/)
>1. [软件测试与质量保障之间的关系](https://blog.csdn.net/QA_Aliance/article/details/6778108)
>1. [质量保证、质量控制让你不再傻傻分不清楚](https://zhuanlan.zhihu.com/p/43732189)
>1. [Quality Assurance, Quality Control and Testing — the Basics of Software Quality Management](https://www.altexsoft.com/whitepapers/quality-assurance-quality-control-and-testing-the-basics-of-software-quality-management/)

---

### `SQM`, `SQP`, `SQA`, `SQC`

1. `SQM` = `SQP` + `SQA` + `SQC`
1. `SQP(Software Quality Planing)`：识别和定义质量管理框架、质量标准及其评估方法
1. `SQA(Software Quality Assurance)`：定义、评估和跟综软件过程的适当性，以向核心干系人表明软件过程是合适的、保证其生产的软件产品质量将符合预期目标
1. `SQC(Software Quality Control)`：验证开发的产品符合预期目标开展的一系列活动，提供产品质量的最终反馈

<!--
1. `testing`：验证软件在有限的测试用例集时是否可产生期望的结果
-->

---

![height:500](./.assets/image/ISO9001_certificate.jpg)

>[ISO9001认证/ISO9000认证中文证书含义介绍](http://www.hzbh.com/show.asp?id=797)

---

![height:500](./.assets/image/cmmi_dev_v1.3_certificate.jpg)

><http://www.only-china.com/About_524.html>

---

![height:600](./.assets/image/qualified_certificate.jpg)

---

1. `SQC` VS `testing`
    1. 本文观点：在软件业界，`SQC`与`testing`为同义词
    1. 其他观点：`SQC` = `testing` + `review`
1. `SQA` VS `SQC`
    1. 本文观点： `SQC`主要针对 **结果**，`SQA`主要针对 **过程**，`SQC`属于 **技术领域**，`SQA`属于 **工程项目领域**，`SQA`使用了`SQC`的技术
    1. 其他观点： `SQC`是`SQA`工作的一个活动，即`SQC`是`SQA`的一个子集
1. `QC`的概念早于`QA`约10年诞生

---

### 🌟 软件质量保证与软件质量控制的联系Relationships of SQA and SQC

1. `SQA`和`SQC`都是为了提升`SQ`
1. `SQA`和`SQC`都属于`SQM`范畴
1. `SQC`这一过程属于`SQA`所关注的过程之一，`SQC`活动在`SQA`的管控范围内
1. `SQA`的实现手段使用了`SQC`的技术

⚠️ 软件质量保证 != 保证软件质量 ⚠️

---

### 🌟 软件质量保证与软件质量控制的区别Differences between SQA and SQC

1. `SQA`关注生产产品的过程（反馈是否按照过程规范执行过程活动），`SQC`关注产品本身（反馈产品质量是否合格）
1. `SQA`关注`SPLC`全生命周期各阶段，`SQC`主要关注`SPLC`的`coding`和`testing`（也关注`requirement`和`design`阶段的产出物）

---

1. `SQA`属于事前预防、事中监督、事后定责的管理手段，`SQC`属于事末检查、事末纠偏的技术手段
    1. `SQA`：监控软件工程流程和方法以确保质量的一系列手段，`SQA`贯穿整个`SPLC`
    1. `SQC`：对过程中的产出物（代码和文档）进行走查或运行，以找出问题，报告质量，并对发现的问题的进行分析、追踪与回归测试
1. `SQA`主要属于工程管理领域，`SQC`主要属于技术领域

---

>1. <https://baike.baidu.com/item/%E8%BD%AF%E4%BB%B6%E8%B4%A8%E9%87%8F%E4%BF%9D%E8%AF%81>
>1. <https://zh.wikipedia.org/wiki/%E8%BD%AF%E4%BB%B6%E8%B4%A8%E9%87%8F%E4%BF%9D%E8%AF%81>

---

### Test vs Testing

1. test
    - countable noun
    - specificity: refer to a specific type of action
1. testing
    - uncountable noun
    - generalization: refers to a broader range of actions

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## ⭐ 软件企业的组织架构

---

### 开发团队Development Team

1. 开发团队：`architect` + `engineer`/`designer` + `developer`/`coder`
1. 大开发团队：开发团队 + `testing engineer`/`tester`

---

### 质量团队Quality Team

1. `QA团队`：`SQA Engineer`
1. 质量团队：`tester` + `SQA Engineer`

❗ ❗ ❗

1. 有些书籍或文档或组织将`tester`称为`QA小组`
1. 理论上，不存在一个实际的"质量团队"，因为`QA`应独立审查

❗ ❗ ❗

---

### 软件质量保证工程师SQA Engineer

1. `SQA Engineer`不属于大开发团队，而是归属于一个独立的团队
1. 以独立审查方式，从第三方的角度监督和控制`SPLC`(特别是`SDLC`)的执行

---

### 软件测试工程师级别Level of Testing Engineer

1. 测试员：入门级工作，负责 **执行** 测试
1. 测试分析师：负责测试生命周期 **计划与执行**，包括：计划测试、设计测试、执行测试、报告测试
1. 高级测试分析师：测试团队中的 **主角**
1. 测试团队领导（测试总监）：为测试而 **负责**
1. 测试顾问：**制定或协助** 制定测试策略、**监管** 测试
1. 高级测试顾问：充当多个测试团队的顾问
1. 首席测试顾问：定义、实现公司级的测试策略、监管公司级的测试

---

### Google开发团队角色

1. `SWE(Software Engineer, 软件开发工程师)`：负责功能实现和这些功能的质量，`SWE`和`SET`一起编写测试代码
1. `SET(Software Engineer in Test, 软件测试开发工程师)`：负责提供测试支持的开发人员，即，`SET`编写代码，通过这些代码提供的功能让`SWE`能够自已测试他们的功能
1. `TE(Test Engineer, 测试工程师)`：负责从用户角度的测试，即，验证是否满足用户的需求

>_"How Google Tests Software"_.

---

![height:600](./.assets/image/how_google_tests_software.jpeg)

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
