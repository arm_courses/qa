---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_White Box Testing_"
footer: "@aRoming"
---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# 白盒测试White Box Testing

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [🌟 白盒测试概述White-Box Testing Overview](#白盒测试概述white-box-testing-overview)
2. [🌟 逻辑分支覆盖测试Logic Branch Coverage Testing](#逻辑分支覆盖测试logic-branch-coverage-testing)
3. [🌟 基本路径测试Basis-Path Testing](#基本路径测试basis-path-testing)
4. [循环测试](#循环测试)
5. [白盒测试的工程手段](#白盒测试的工程手段)
6. [白盒测试的工具](#白盒测试的工具)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 白盒测试概述White-Box Testing Overview

---

### 白盒测试是什么

1. **_"C-SWEBOK 2018"_**：如果测试用例的生成依赖于软件的设计与编码等信息，则将此类测试技术归类为白盒测试
1. **_"IEEE 24765:2010"_**: glass box玻璃盒/white box白盒
    1. a system or component whose internal contents or implementation are known
    1. pertaining to an approach that treats a system or component as in (1)

---

1. 白盒测试与黑盒测试相反，白盒测试的前提是`tester` **_完全知道_** 程序的结构和算法
1. 白盒测试关注测试对源码覆盖程度
1. 完全的白盒测试是将每条路径都执行
1. 白盒测试的主要依赖信息
    1. `src`
    1. `SDD`：程序结构相关图表等信息
1. 白盒测试也称为：玻璃盒测试、结构测试、逻辑驱动测试、基于代码的测试

---

![height:550](./.assets/image/white-box-testing.jpg)

---

### 白盒测试有什么/白盒测试的分类

1. 静态白盒测试
1. 动态白盒测试

---

#### 静态白盒测试

- 关键功能：
    1. 软件实现是否符合规范，是否存在已知的缺陷模式
    1. 软件实现与软件需求是否一致、是否有冲突或歧义
- 执行方式
    1. 手工方式：如，代码审查`code review`
    1. 自动方式：可以借助工具进行自动化执行测试，如，数据流分析器、语法分析器等

---

#### 动态白盒测试

1. 控制流覆盖：基于控制流（执行流）设计测试用例
    1. 🌟 逻辑分支覆盖：基于判定逻辑设计`test case suite`，目标是将机器代码的所有分支均执行一次
    1. 🌟 逻辑路径覆盖：基于路径空间设计`test case suite`，目标是将机器代码的所有路径均执行一次
1. 数据流覆盖：基于数据流设计测试用例
1. 其他方法：符号测试等

<!--TODO:
程序插装测试、程序变异测试是一种白盒测试方法？还是一种测试工程手段？
-->

---

### 控制流覆盖Control Flow Coverage

<!--
1. 控制流覆盖（Control Flow Coverage）：是一种在考虑测试对象的控制流情况下导出测试用例的测试方法，并且借助于控制流图能评估测试的完整性（覆盖率）

---

-->

1. 逻辑分支覆盖法：也称为，逻辑覆盖法、逻辑判断覆盖法、分支结构法、基于分支的测试法等
    1. 分支和循环包括判定结点，判定结点是导致执行流程变化和程序结构复杂的主要原因
    1. 语句覆盖、分支覆盖/判定覆盖、条件覆盖、分支-条件覆盖/判定-条件覆盖、条件组合覆盖
1. 路径覆盖法、基本路径覆盖法
1. 专题测试：循环结构测试/基于循环的测试

<!--TODO:
1. 路径覆盖是否属于路径测试技术，而不属于逻辑覆盖测试技术
1. 循环结构测试是否属于路径覆盖测试，而不属于逻辑覆盖测试技术
-->

---

#### 路径覆盖Path Coverage

1. 定义：`test case suite`使所有可能的路径均至少出现一次
1. 优缺点：
    1. 优点：覆盖率最高，完全测试
    1. 缺点：`test case suite`量大，事实上是"穷举测试"（包含循环时，往往不能穷尽）

---

### 白盒测试的基本原则

1. 对程序模块的所有独立的执行路径至少测试一次
1. 对所有的逻辑判定，取"真"与取"假"的两种情况都至少测试一次
1. 在循环的上下边界和运行范围内执行循环体
1. 对内部数据结构的有效性进行测试

---

### 白盒测试的实施步骤

1. 测试计划阶段：根据`SRS`，制定测试进度
1. 测试设计阶段：依据`SDD`和`src`，按照一定规范化的方法进行软件结构划分和设计测试用例
1. 测试执行阶段：输入`test case suite`，得到测试结果
1. 测试总结阶段：对比测试的结果和代码的预期结果，分析错误原因，找到并解决错误

---

### 白盒测试的优缺点

1. 优点：代码级别的测试、细节的测试
    1. 容易自动化
    1. 针对性强，便于缺陷定位和缺陷修复
    1. 有助于了解测试的覆盖程度
    1. 有助于代码优化和缺陷预防
1. 缺点：细节的庞大
    1. 对`tester`要求高
    1. 系统庞大时，测试开销大

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 逻辑分支覆盖测试Logic Branch Coverage Testing

>1. <https://blog.csdn.net/xiadanying/article/details/91419065>
>1. <https://blog.csdn.net/write6/article/details/78702977>
>1. <https://jirehchan.github.io/software-test-02/>

---

```cpp {.line-numbers}
#include <iostream>

using namespace std;

int main(int argc, const char *argv[])
{
    double a, b, x;
    cin >> a >> b >> x;
    if ((a > 1) && (b == 0)) {x = x / a;}
    if ((a == 2) || (x > 1)) {x = x + 1;}
    cout << x;
    return 0;
}
```

---

![height:550](./.assets/diagram/example_flow.svg)

---

### 语句覆盖Statement Coverage

1. 定义：`test case suite`使每条可执行语句至少被执行一次
1. 优缺点：
    1. 优点： 直观地从代码中得到测试用例
    1. 缺点： 对于隐藏的条件和可能到达的隐式分支是无法测试的，语句覆盖只在乎运行一次，而不考虑其他情况
1. `test case`：
    1. a = 2, b = 0, x = 3：达到语句覆盖
    1. a = 2, b = 1, x = 3：未达到语句覆盖

---

### 分支覆盖/判定覆盖Branch Coverage/Decision Coverage

1. 定义：`test case suite`使每个判定表达式的`true`和`false`结果都至少经历一次（即，使每个分支都至少经历一次），并且每条语句都至少被执行一次
1. 优缺点：
    1. 优点： 比语句覆盖更强的测试能力，比语句覆盖多几乎一倍的测试路径
    1. 缺点： 大部分的判定表达式由多个条件组合而成，若仅仅判断其最终结果，而忽略每个条件的取值必然会遗漏测试路径

👉 分支覆盖只需判定条件整体分别经历`true`和`false`即可

---

1. `test case suite 01`
    1. a = 2, b = 0, x = 3
    1. a = 1, b = 0, x = 1
1. `test case suite 02`
    1. a = 3, b = 0, x = 3
    1. a = 2, b = 1, x = 1

---

### 条件覆盖Condition Coverage

1. 定义：`test case`使每个判定表达式的每个条件的`true`和`false`都至少经历一次
1. 优缺点：
    1. 优点： 条件覆盖比分支覆盖增加了对符合判定情况的测试，增加了测试的路径
    1. 缺点： 条件覆盖并不能保证分支覆盖，条件覆盖只保证每个逻辑条件至少均取一次`true`和`false`，而不考虑判定结果

---

### 分支-条件覆盖Branch-Condition Coverage

1. 定义：`test case`使程序中每个条件的`true`和`false`都至少经历一次，且每个判定本身也至少经历一次`true`和`false`
1. 优缺点：
    1. 优点： 分支-条件覆盖弥补了分支覆盖和条件覆盖的不足
    1. 缺点： 分支-条件覆盖未考虑条件的组合情况

---

### 分支-条件组合覆盖/条件组合覆盖 Branch Condition Combination Coverage

1. `test case`使每个判定表达式的各个条件的组合情况均至少出现一次
1. 优点：
    1. 满足分支-条件覆盖、分支覆盖和条件覆盖
    1. 方法简单： 列出所有条件>>>列出真值表>>>穷尽所有组合
1. 缺点： `test case`量大，冗余度高

---

### 小结

1. 覆盖率：语句覆盖 <= 分支覆盖、条件覆盖 <= 分支-条件覆盖 <= 条件组合覆盖 <= 路径覆盖

<!--TODO:
路径覆盖不能保证条件组合覆盖 ==> 不能保证吗？还是该条件不可能满足，所以没有对应的路径
-->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 基本路径测试Basis-Path Testing

>1. <https://zhuanlan.zhihu.com/p/120418243>
>1. <https://blog.csdn.net/zzzmmmkkk/article/details/4288980>

---

### 什么是基本路径测试法

1. ❗ 基本路径测试法中的"基本"是"路径"的定语（而不是"路径测试法"的定语），即，基本路径的测试法
1. 基本路径测试法，也称为独立路径测试法，即，通过独立路径集设计`test case suite`，从而达到等价于对整个路径空间进行测试的效果

---

>In software engineering, basis path testing, or structured testing, is a white box method for designing test cases. The method analyzes the control flow graph of a program to find **_a set of linearly independent paths_** of execution. The method normally uses McCabe' cyclomatic complexity to determine the number of linearly independent paths and then generates test cases for each path thus obtained. Basis path testing **_guarantees complete branch coverage (all edges of the control flow graph)_**, but achieves that without covering all possible paths of the control flow graph - the latter is usually too costly. Basis path testing has been widely used and studied.
>
>><https://en.wikipedia.org/wiki/Basis_path_testing>

---

### 基本路径测试法的理论依据

1. 基本路径测试法的原理与向量空间中的基向量类似： 向量空间中任意一向量，均可以唯一地表示成基向量的线性组合
1. 基本路径/独立路径
    1. 线性无关的路径向量
    1. 任意路径均可由独立路径集表示

---

### 程序控制流图Control Flow Graph, CFG

1. 程序控制流图是压缩后的程序流程图（简称"流程图"），程序控制流图有时也简称为"程序图"或"控制流图"（以下使用"控制流图"）
1. 控制流图只由结点和边（有向弧）两种图形符号组成
    1. 结点： 顺序语句块结点、条件结点、汇聚结点
    1. 边： 执行方向

---

### 环复杂度

1. 环复杂度（圈复杂度）是度量程序复杂度的一种方式，$`环复杂度 == 独立路径数`$
1. 环复杂度的3种计算方法
    1. 从控制流图的封闭区域数得出： $`V(G) = \text{区域数} = \text{封闭区域数} + 1`$
    1. 从控制流图的结点数和边数得出： $`V(G) = E - N +2`$
    1. 从控制流图的判定结点数得出： $`V(G) = P + 1`$

---

### 基本路径测试法的步骤

1. 从`src`得出控制流图
    1. 🌟 增加汇聚结点：分支汇聚处应新增一个汇聚结点
    1. 🌟 分解组合判定：条件组合的判定表达式应分解成只有单条件的嵌套的多个判定表达式
1. 计算环复杂度
1. 导出独立路径集
    1. 确定主路径：包含判定结点最多的路径（风险最高的路径）
    1. 根据主路径导出其他独立路径： 根据判定结点新增未经历的路径
1. 根据独立路径集和判定条件得出`test case suite`

---

1. 1 -> 2 -> 3 -> 6 -> 7 -> 9 -> 10 -> 1 -> 11（主路径）
1. 1 -> 2 -> 3 -> 6 -> 8 -> 9 -> 10 -> 1 -> 11（主路径）
1. 1 -> 2 -> 3 -> 4 -> 5 -> 10 -> 1 -> 11
1. 1 -> 11

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 循环测试

<!--TODO:
循环测试是否是一种路径测试？
-->

---

1. 循环测试专注于测试循环结构的有效性
1. 循环测试的原则：在循环的边界和最大循环次数内执行循环体
1. 循环的类型：
    1. 简单循环
    1. 嵌套循环
    1. 串联循环/连锁循环
    1. 非结构循环（不应出现非结构循环）

---

### 简单循环的循环测试

1. 0次循环
1. 1次循环
1. 2次循环
1. m次循环（m < n 且 m是典型值）
1. n-1次循环（如果n-1能取值）
1. n次循环（如果n能取值）

👉 假定n为最大循环次数

---

### 嵌套循环的循环测试

1. 从内层循环开始，将其他循环设置为最小值
2. 逐步外推，对其外面一层循环进行测试
    1. 保持当前循环的所有外层循环的循环变量取最小值
    1. 保持当前循环的所有内层循环的循环变量取典型值
3. 反复进行，直到所有各层循环测试完毕

---

### 串联循环的循环测试

1. 如果各个循环的判定条件之间相互独立 ==> 参照简单循环进行测试
1. 如果各个循环的判定条件之间不相互独立 ==> 参照嵌套循环进行测试

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 白盒测试的工程手段

---

### 程序插桩Program Instrumentation

1. `debug`：通过`breakpoint`, `print`，`logging`等方式，了解程序动态信息
1. 程序插桩：
    1. 向被测程序中插入操作以实现测试目的的方法，程序插桩不应该影响到被测程序的运行过程和功能
    1. 有时将插入的语句称为"探测器"（实现"探查"和"监控"的功能）

---

### 程序变异测试Program Mutation Testing

1. 主要目的；提供了基于缺陷的对测试充分性进行度量的角度，针对测试用例集的充分性进行评估和改进
1. 次要作用：可以查出被测软件在做过其他测试后还剩余一些的小差错

>1. <https://www.jianshu.com/p/c9ff2589ad3e>
>1. <https://www.cnblogs.com/tongwee/p/4505289.html>

---

>Mutation testing (or mutation analysis or program mutation) is used to **_design new software tests_** and **_evaluate the quality of existing software tests_**. Mutation testing involves modifying a program in small ways.
>
><https://en.wikipedia.org/wiki/Mutation_testing>
>
>**_IEEE 24765:2020_** : mutation testing: a testing methodology in which two or more program mutations are **_executed using the same test cases_** to **_evaluate the ability of the test cases_** to detect differences in the mutations

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 白盒测试的工具

---

1. [`Parasoft C/C++ Test`](https://www.parasoftchina.com/products/ctest/)： 
    1. 静态分析, 代码审查, 单元测试, 运行时错误检测
    1. 自动测试代码构造（白盒测试）、测试代码功能性（黑盒测试）和维护代码完整性（回归测试）
1. `JUnit`： `xUnit`的一个实例

:point_right: see more in [UT with JUnit](../../../addons/Unit_Testings/UT_with_JUnit/README.md)

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
