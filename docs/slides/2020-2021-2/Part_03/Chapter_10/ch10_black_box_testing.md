---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Black Box Testing_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 黑盒测试Black Box Testing

---

## Outlines

1. 黑盒测试的定义、目标、优劣势、典型的黑盒测试方法
1. :star2: `ECP`等价类划分法
1. :star2: `BVA`边界值分析法
1. `CEGA`因果图法
1. `FDA`功能图法
1. 黑盒测试方法的选择
1. 黑盒测试工具

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 黑盒测试概述Black Box Testing Overview

---

### What is Black Box Testing

1. **_"C-SWEBOK 2018"_**：如果测试用例的生成仅仅依赖于软件的输入/输出，则将此类测试技术归类为黑盒测试
1. 黑盒测试也称为：`功能测试`、`数据驱动测试`或`基于规格说明的测试`
1. 黑盒测试适合软件测试的各个阶段

![height:300](./assets_image/black-box-testing.jpg)

---

### What is Black Box Testing(contd.)

+ 测试者不了解程序的内部情况，不需具备应用程序的代码、内部结构和编程语言的专门知识。只知道程序的输入、输出和系统的功能
+ 从用户的角度针对软件界面、功能及外部结构进行测试，而不考虑程序内部逻辑结构
+ 测试案例是依应用系统应该做的功能，照规范、规格或要求等设计
+ 测试者选择有效输入和无效输入来验证是否正确的输出

>1. <https://zh.wikipedia.org/wiki/%E9%BB%91%E7%9B%92%E6%B5%8B%E8%AF%95>

---

### 黑盒测试的目标The Purposes of Black Box Testing

1. 功能缺陷
1. 非功能缺陷
1. 界面缺陷
1. 外部数据源访问缺陷
1. 初始化或终止缺陷

---

### 黑盒测试的优劣势

1. 优势
    1. 简单有效
    1. 整体测试： 可以整体测试系统的行为
    1. 并行测试： 开发与测试可以并行
    1. 要求较低： 对`tester`的技术要求相对较低
1. 劣势
    1. 覆盖率低： 难以覆盖所有代码
    1. 门槛较低： `tester`的入门门槛较低

---

### 测试方法的评价标准

1. 在最短时间内（快），以最少的人力（省），发现最多的（多）、最严重的缺陷（好）
    1. 精准测试： 针对性强
    1. 完备测试： 覆盖全面
    1. 冗余度小： 减少不必要的冗余
    1. 执行简单： 执行测试简单
    1. 定位快速： 定位引起测试失败的原因的难度小、速度快

---

### 典型黑盒测试方法Main Methods of Black Box Testing

1. 基于输入输出数据
    1. :star2: `ECP(Equivalence Class Partitioning, 等价类划分法)`
    1. :star2: `BVA(Boundary Value Analysis, 边界值分析法)`
1. 基于输入输出关系
    1. `CEG(Cause Effect Graphing, 因果图法)`
    1. `DTT(Decision Table Testing, 判定表驱动法)`
1. 基于状态迁移
    1. `FDA(Function Diagram Analysis, 功能图分析法)`
1. 基于业务场景/业务流程
    1. `Scenario Testing(场景法)`

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## ECP (Equivalence Class Partitioning, 等价类划分法)

><https://zhuanlan.zhihu.com/p/112810758>

---

### What is ECP

+ `ECP`将输入域或输出域划分成若干子集，从每个子集中选取少量具有代表性的数据作为测试用例数据
    1. `ECP`是一种典型的、基本的黑盒测试方法
    1. :star2: `ECP`不考虑程序内部结构，设计测试用例的依据是`SRS`
+ `ECP`适用场景： 有输入或输出

:exclamation: 以下将`IO域`代替`输入域或输出域`

><https://en.wikipedia.org/wiki/Equivalence_partitioning>

---

### EC等价类

1. `valid partition`有效等价类：有效的、符合规定的
1. `invalid partition`无效等价类：无效的、不符合规定的

---

### How to Decide EC

1. 规定了 **取值范围** 或 **值的个数** ==> 确定1个`有效等类`和2个`无效等价类`
1. 规定了 **值的集合** 或规定了 **“必须如何”** ==> 确定1个`有效等价类`和1个`无效等价类`
1. 规定了 **是一个`boolean`** ==> 确定1个`有效等价类`和1个`无效等价类`

---

### How to Decide EC (contd.)

1. 规定了 **是一组值（假定n个）** 并且 **要对每个值分别处理** ==> 确定n个`有效等价类`和1个`无效等价类`
1. 规定了 **必须遵守的规则集** ==> 确定1个`有效等价类`（符合规则集）和若干个`无效等价类`

:exclamation: 如果已划分的`ECP`中各元素在程序处理中的方式不同的情况下，应再将该等价类进一步划分为更小的等价类 ==> 即，同一`ECP`的元素产生的预期测试效果应一致

---

### How to Design ECP Test Case

1. 确定`EC`
1. 建立`ECT(Equivalence Class Table)`： 为每个`EC`编号
1. 设计`Test Case`
    1. 设计一个新的测试用例，使其 _**尽可能多地覆盖尚未被覆盖**_ 的 `有效等价类`，重复这一步，直到所有的有效等价类都被覆盖为止
    1. 设计一个新的测试用例，使其 _**仅覆盖一个尚未被覆盖**_ 的 `无效等价类`，重复这一步，直到所有的无效等价类都被覆盖为止

---

### Example of ECP -- EMail Register

#### 邮箱名要求

1. 6至18个字符
1. 可使用字母、数字、下划线
1. 必须以字母开头

---

### Example of ECP -- EMail Register -- EC

|`valid partition`|`NO.`|`invalid partition`|`NO.`|
|--|--|--|--|
|6 ~ 18个字符|1|少于6个字符<br/>多于18个字符|2<br/>3|
|字母、数字、下划线|4|特殊字符<br/>非打印字符<br/>中文字符|5<br/>6<br/>7|
|字母开头|8|数字或下划线开头|9|

---

### Example of ECP -- EMail Register -- Test Cases

|`NO.`|input|`EC`|except output|
|--|--|--|--|
|1|test_123|1, 4, 8|valid input|
|2|test|2, 4, 8|invalid input|
|3|test_123456789_123456789|3, 4, 8|invalid input|
|4|test&&123|1, 5, 8|invalid input|
|5|test 123|1, 6, 8|invalid input|
|6|test测试123|1, 7, 8|invalid input|
|7|123_test|1, 4, 9|invalid input|

---

### ECP的优缺点

1. 优点：减少了穷举法带来的无限测试用例，化无限测试为有限测试
1. 缺点：
    1. 输入之间（或输出之间）的关系较少考虑
    1. 假设等价类中所有的测试用例均等价

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## BVA (Boundary Value Analysis, 边界值分析法)

><https://zhuanlan.zhihu.com/p/121133074>

---

### What is BVA

1. `BVA`指在`IO域`的边界或附近上选择测试用例进行的测试
1. `BVA`通常作为`ECP`的补充，其测试用例来自`EC`的边界
1. 适用场景： 有范围的输入或输出
1. `BV`指相对于输入或输出`EC`而言

![height:200](./assets_image/bva.png)

><https://en.wikipedia.org/wiki/Boundary-value_analysis>

---

### Why Uses BVA

1. `ECP`可能忽略了某些特定类型的测试用例，`BVA`进行弥补
1. 大量的`bug`发生在边界，而不是在中间区域
1. `BVA`大多数是冗余测试，适当的冗余测试有利于发现未知`bug`

---

### How to Decide BV

1. 边界
1. 次边界： `delta`$\Delta$
1. 其他边界
    + `NULL`
    + `""`
    + `...`

---

### How to Decide BV (example)

1. 如果程序的输入或输出是一个有序序列（例如顺序的文件、线性列表或表格）
    1. 首元素
    1. 尾元素
1. 如果程序中使用了一个内部数据结构
    1. 该内部数据结构的边界上的值
1. 分析规格说明，找出其他可能的边界条件

---

### How to Design BVA Test Cases

1. 确定`BV`
1. 设计`test case`

---

### Example -- Tests and Types

1. <https://stitcher.io/blog/tests-and-types>
1. <https://sikasjc.github.io/2019/07/07/tests-and-types/>

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## CEGA (Cause Effect Graphing Analysis, 因果图法)

><https://zhuanlan.zhihu.com/p/129342944>

---

### What is CEGA

1. `CEG (Cause Effect Graph, 因果图)`是一种描述输入条件的组合以及每种组合对应的输出的图形化工具
1. `CEGA`
    1. 基于`CEG`设计测试用例的测试
    1. 根据关系（输入条件的组合、约束关系和输出条件的因果关系等），利用图解法分析输入条件的各种组合情况，制定判定表，从而设计测试用例
1. 适用场景： 多种输入条件组合

---

### Why Uses CEGA

1. `ECP`和`BVA`没有考虑输入间的组合、制约等关系
1. `ECP`和`BVA`没有考虑输入与输出的关系

---

### 因果图的符号 -- 原因与结果的因果关系

1. `恒等/等价`：若原因出现，则结果也出现；若原因不出现，则结果也不出现
1. `非(～)`：若原因出现，则结果不出现；若原因不出现，则结果出现
1. `或(∨)`：若几个原因中有一个出现，则结果出现；若几个原因均不出现，则结果不出现
1. `与(∧)`：若几个原因均出现，则结果才出现；若其中有一个原因不出现，则结果不出现

---

![height:250](./assets_image/basic_symbols_of_CEG.png)

---

### 因果图的符号 -- 约束关系

1. 原因与原因的约束关系
    1. `E(Exclusive, 互斥/异)`：多个原因中最多有一个可能出现
    1. `I(Include, 包含/或)`：多个原因中至少有一个出现
    1. `O(Only, 惟一)`：多个原因中有且仅有一个出现
    1. `R(Required,要求)`：`a`出现则`b`必出现
1. 原因与结果的约束关系
    1. `M(Masked,强制/屏蔽)`：`a`出现则`b`必不出现，`a`不出现则`b`不确定

---

![height:300](./assets_image/constraint_symbols_of_CEG.png)

---

### How to Design CEGA Test Cases

1. 分析程序规格说明的描述中，哪些是原因，哪些是结果。原因常常是输入条件或是输入条件的等价类，而结果是输出条件。
1. 分析程序规格说明的描述中语义的内容，并将其表示成连接各个原因与各个结果的“因果图”。
1. 标明约束条件。由于语法或环境的限制，有些原因和结果的组合情况是不可能出现的。为表明这些特定的情况，在因果图上使用若干个标准的符号标明约束条件。
1. 把因果图转换成判定表。
1. 为判定表中每一列表示的情况设计测试用例。

---

### CEGA案例 -- 案例描述

1. 处理销售单价为1.5元饮料的自动售货机软件
1. 若投入1.5元硬币，按下“可乐”、“雪碧”或“红茶”按钮，相应的饮料被送出
1. 若投入的是2元硬币，在送出饮料的同时退还0.5元硬币

---

### CEGA案例 -- 状态表

|-|-|
|--|--|
|原因|c1:投入1.5元硬币<br/>c2:投入2元硬币<br/>c3:按“可乐”按钮<br/>c4:按“雪碧”按钮<br/>c5:按“红茶”按钮|
|中间|11：已投币<br/>12：已按钮|
|结果|a1:退还5角硬币<br/>a2:送出“可乐”饮料<br/>a3:送出“雪碧”饮料<br/>a4:送出“红茶”饮料|

---

![height:600](./assets_image/CEG_of_example.png)

---

![height:600](./assets_image/decision_table_of_example.png)

---

### CEGA案例 -- 测试用例（ECP）

|用例编号|测试用例|预期输出|
|--|--|--|
|1|投入1.5元，按“可乐”|送出“可乐”饮料|
|2|投入1.5元，按“雪碧”|送出“雪碧”饮料|
|3|投入1.5元，按“红茶”|送出“红茶”饮料|
|4|投入2元，按“可乐”|找0.5元，送出“可乐”|
|5|投入2元，按“雪碧”|找0.5元，送出“雪碧”|
|6|投入2元，按“红茶”|找0.5元，送出“红茶”|

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## FDA(Function Diagram Analysis, 功能图分析法)

---

1. 程序的功能说明通常由动态说明和静态说明组成
    + 动态说明描述了输入数据的次序或转移的次序
    + 静态说明描述了输入条件与输出条件之间的对应关系
1. 复杂的程序，存在大量的组合情况，因此，仅用静态说明组成的规格说明对于测试是不够的，必须用动态说明来补充功能说明

---

### What is FDA

1. 功能图法： 利用程序的功能图生成测试用例的测试方法
1. 适合场景： 状态迁移（测试路径）与逻辑模型
1. 功能图模型
    + `STD (状态迁移图)`： 输入数据和当前状态决定输出数据和后续状态
    + 逻辑功能图

---

### Gray Box Testing

1. 功能图法是混合黑盒和白盒的`灰盒`测试方法
1. 功能图法中用到的`逻辑覆盖`和`路径测试`具有白盒测试方法的特点
    + 为区别于白盒测试中测试程序内部结构，功能图法的`逻辑覆盖`和`路径测试`是系统级的测试

![height:300](./assets_image/gray-box-testing.jpg)

---

![height:600](./assets_image/FD.png)

---

### 功能图法生成测试用例

1. 生成局部测试用例： 在每个状态中，从因果图生成局部测试用例。局部测试库由原因值（输入数据）组合与对应的结果值（输出数据或状态）构成。
1. 测试路径生成： 利用上面的规则生成从初始状态到最后状态的测试路径。
1. 测试用例合成： 合成测试路径与功能图中每个状态的局部测试用例。结果是视状态到最后状态的一个状态序列，以及每个状态中输入数据与对应输出数据组合。
测试用例的合成算法： 采用条件构造树。

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 黑盒测试方法的比较与选择Comparison and Selection of Black Box Testing

---

1. 进行`ECP`（包括输入和输出的`EC`），将无限测试变成有限测试
1. 进行`BVA`
1. 可以用错误推测法追加一些测试用例（依靠`tester`的智慧和经验）
1. 对照程序功能，检查已设计出的测试用例的覆盖程度。如未达到要求的覆盖标准，应补充足够的测试用例
1. 如果`SRS`中含有输入条件的组合情况，则一开始就可选用`CEGA`
1. 对于参数配置类软件，用正交试验法选择较少的组合达到最佳效果
1. 考虑进行`FDA`
1. 对于业务流清晰的系统，可以利用场景法贯穿整个测试过程，综合使用各种测试方法

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 黑盒测试工具介绍Black Box Testing CATT

---

1. `HP Mercury WinRunner` (migrated to `HP Functional Testing`)： 企业级应用的功能测试工具
1. `HP Mercury LoadRunner`： 企业级应用的负载测试工具
1. `HP Mercury QuickTest Professional`： 企业级应用的自动化测试工具

---

### 自动化测试

1. 通过“录制”和“回放”实现“自动化”
    1. 录制： 生成脚本
    1. 回放： 运行脚本

---

### HP Mercury WinRunner (Migrated to HP Functional Testing)

---

![height:600](./assets_image/winrunner_01.png)

---

![height:600](./assets_image/winrunner_02.png)

---

![height:600](./assets_image/winrunner_03.png)

---

![height:600](./assets_image/winrunner_04.png)

---

### HP Mercury LoadRunner

---

![height:600](./assets_image/loadrunner_01.png)

---

![height:600](./assets_image/loadrunner_02.png)

---

![height:450](./assets_image/loadrunner_03.png)

---

![height:600](./assets_image/loadrunner_05.png)

---

![height:600](./assets_image/loadrunner_06.png)

---

### HP Mercury QuickTest Professional

---

![height:600](./assets_image/qtp_01.png)

---

![height:600](./assets_image/qtp_02.png)

---

![height:600](./assets_image/qtp_03.png)

---

![height:600](./assets_image/qtp_04.png)

---

![height:600](./assets_image/qtp_05.png)

---

![height:600](./assets_image/qtp_06.png)

---

![height:600](./assets_image/qtp_07.png)

---

![height:600](./assets_image/qtp_08.png)

---

![height:600](./assets_image/qtp_09.png)

---

![height:600](./assets_image/qtp_10.png)

---

![height:600](./assets_image/qtp_11.png)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Futhermore

---

1. [Black Box Testing: An In-Depth Tutorial With Examples And Techniques](https://www.softwaretestinghelp.com/black-box-testing/)
1. [Software Engineering | Black box testing](https://www.geeksforgeeks.org/software-engineering-black-box-testing/)
1. [Black-box testing](https://en.wikipedia.org/wiki/Black-box_testing)
1. [Gray box testing](https://en.wikipedia.org/wiki/Gray_box_testing)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok:End of This Chapter:ok:
