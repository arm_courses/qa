---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Defect Management_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 软件缺陷管理

---

## Contents

1. :star2: 软件缺陷的相关定义： 软件缺陷、软件缺陷管理、软件缺陷特征等
1. :star2: 软件缺陷的属性： 标识、类型、严重级、优先级等
1. :star2: 软件缺陷报告
1. 软件缺陷管理与软件缺陷管理系统
1. 软件缺陷管理与`CMM`

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 软件缺陷概述

---

### `bug` vs `defect` vs `fault` vs `failure`

1. `error错误/bug臭虫`：指 **_存在于_** 软件中的不希望或不可接受的 **_错误_** （一般指程序的错误），**_static concept_**
1. `defect缺陷`：指 **_存在于_** 软件中的不希望或不可接受的 **_偏差_** （程序、数据或文档），**_static concept_**
1. `fault故障`：指软件 **_运行时_** 出现的一种不希望或不可接受的  **_内部状态_**，**_dynamic concept_**
1. `failure失效`：指软件 **_运行时_** 出现的一种不希望或不可接受的 **_外部表现_**，**_dynamic concept_**

:point_right: `error/bug --> defect --> fault --> failure` :point_left:

---

### 缺陷管理的定义和方法

1. 缺陷管理： 在`SDLC`中发现`defect`并对已发现的`defect`进行跟踪，并确保每个已发现的`defect`都被正确地 **_处理_**
1. 一般需要跟踪管理工具帮助进行缺陷的全流程管理
1. 开发低缺陷软件的途径：
    1. 缺陷预防
    1. 缺陷排除

---

### 缺陷的产生原因

1. 程序编写错误
1. 编写程序未按照规定
1. 开发人员的态度
1. 软件越来越复杂
1. 需求变更过于频繁
1. 沟通上的问题
1. 进度上的压力
1. 管理上的失误

---

### 缺陷的特征

1. `has cause有原因`： 缺陷均有原因
1. :star2: `reproducible可重现性`： 无法重现的缺陷则无法被定位，无法被定位则无法被修复
1. `累积性/放大性`： 缺陷发现得越晚、处理得越晚，付出的代价越大
1. `引入新缺陷`： 修复一个缺陷可能会引入新的缺陷

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 缺陷的属性Attributes of Defect

---

1. :star2: `identifier标识`： 唯一标识一个缺陷的标识符（建议使用工具进行管理）
1. `type类型`： 根据缺陷自然属性划分的缺陷类型
1. :star2: `severity严重级`： 因缺陷引起的故障对软件产品的影响程度
1. :star2: `priority优先级`： 缺陷被修复的优先程度
1. `status状态`： 缺陷当前被跟踪的修复进展情况
1. `origin起源/由来`： 首次发现缺陷时所处的阶段
1. `source来源/源头`： 缺陷产生的阶段
1. `root cause根源/原因`： 缺陷产生的根本原因

:point_right: 严重级、优先级的级别数不宜过多，一般3至5个级别 :point_left:

---

### 缺陷的类型

1. 缺陷类型没有公认的、统一的标准（但有参考标准）
1. 常见的缺陷类型： 功能缺陷、用户界面缺陷、文档缺陷、软件配置缺陷、性能缺陷、接口缺陷等

---

### 缺陷的严重级

1. Blocker: Blocks development and/or testing work
1. Critical: crashes, loss of data, severe memory leak
1. Major: major loss of function
1. Minor: minor loss of function, or other problem where easy workaround is present
1. Trivial: cosmetic problem like misspelled words or misaligned text
1. Enhancement: Request for enhancement

---

### 缺陷的严重级的判断

1. 严重级（Severity）： 软件缺陷对软件质量的破坏程度，反应其对产品和用户的影响，即此软件缺陷的存在将对软件的功能和性能产生怎样的影响
1. 软件缺陷的严重性的判断 **应该从软件最终用户的观点** 做出判断，即判断缺陷的严重性要为用户考虑，考虑缺陷对用户使用造成的恶劣后果的严重性

:point_right: 注意“伪严重” :point_left:

---

### 缺陷的优先级

1. Resolve Immediately： 缺陷必须被立即解决
1. Normal Queue： 缺陷需要正常排队等待修复
1. Not Urgent： 缺陷可以在方便时进行修复

---

### 缺陷的优先级的判断

1. 优先级表示修复缺陷的重要程度和应该何时修复，是表示处理和修正软件缺陷的先后顺序的指标，即哪些缺陷需要优先修正，哪些缺陷可以稍后修正
1. 确定软件缺陷优先级，更多的是站在软件开发工程师的角度考虑问题，因为缺陷的修正顺序是个复杂的过程，有些不是纯粹的技术问题，而且开发人员更熟悉软件代码，能够比测试工程师更清楚修正缺陷的难度和风险（:warning: 该观点有争议 :warning:）

---

### 缺陷的优先级的判断（contd.）

>在提交Bug时，一般只定义bug的severity，而将priority交给project leader 或team leader来定义，由他们来决定该bug被修复的优先等级。某种意义上来说，priority的定义要依赖于severity，在大多数情况下，severity越严重，那这个bug的priority就越高。
>
>[Bug的严重程度、优先级如何定义](https://cloud.tencent.com/developer/article/1733135)

---

### 缺陷的状态

1. Submitted： 已提交的缺陷（已提交未排序）
1. Open： 确认“提交的缺陷”，等待处理
1. Rejected： 拒绝“提交的缺陷”，不需要修复或不是缺陷
1. Resolved： 缺陷被修复
1. Closed： 确认被修复的缺陷，将其关闭（确认完成修复）

---

### 缺陷的起源/由来

1. Requirement： 在需求阶段发现的缺陷
1. Architecture： 在构架阶段发现的缺陷
1. Design： 在设计阶段发现的缺陷
1. Code： 在编码阶段发现的缺陷
1. Test： 在测试阶段发现的缺陷

---

### 缺陷的来源/源头

1. Requirement： 由于需求的问题引起的缺陷
1. Architecture： 由于构架的问题引起的缺陷
1. Design： 由于设计的问题引起的缺陷
1. Code： 由于编码的问题引起的缺陷
1. Test： 由于测试的问题引起的缺陷（测试会引起缺陷吗 :question: ）
1. Integration： 由于集成的问题引起的缺陷

---

### 缺陷的根源/原因

1. 目标： 如：错误的范围，误解了目标，超越能力的目标等
1. 过程，工具和方法： 如：无效的需求收集过程，过时的风险管理过程，不适用的项目管理方法，没有估算规程，无效的变更控制过程等
1. 人： 如：项目团队职责交叉，缺乏培训。没有经验的项目团队，缺乏士气和动机不纯等
1. 缺乏组织和通讯： 如：缺乏用户参与，职责不明确，管理失败等

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 缺陷报告

---

### 缺陷报告的原则

1. 尽快报告缺陷
1. 准确且有效地描述缺陷

---

### 准确且有效地描述缺陷

1. :star2: 单一准确： 准确地识别软件缺陷（即将外在表现识别为内在缺陷），每个报告只针对一个软件缺陷
1. :star2: 可以重现： 提供精确操作步骤，使开发人员可以自己重现缺陷
1. :star2: 完整统一： 提供完整、前后统一的软件缺陷的重现步骤和信息
1. 短小简练： 尽可能使标题即能准确解释产生缺陷的现象，并使标题保持短小简练
1. 特定条件/上下文环境： 不要忽视这些看似细节的但又必要的特定条件
1. 补充完善/可被跟踪： `tester`重视并继续监视其被修复的全过程
1. 不做评价/客观描述： 描述不带个人观点，不对开发人员进行评价

---

### IEEE829-1998 - `Test Incident Report`

1. 软件缺陷报告标识符
1. 软件缺陷总结
1. 软件缺陷描述
1. 软件缺陷影响

---

### IEEE829-1998 - `Test Incident Report` - `Incident Description`

1. Inputs输入
1. Expected results期望结果
1. Actual results实现结果
1. Anomalies异常情况说明
1. Date and time时间日期
1. Procedure step缺陷发生的步骤
1. Environment测试环境/上下文环境
1. Attempts to repeat重现缺陷的次数
1. Testers测试人
1. Observers观察员

<!--TODO:
Attempts to repeat要记录什么信息？
-->

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 软件缺陷管理与软件缺陷管理系统

---

<!--
_backgroundColor: Beige
_color:
_class:
    - lead
-->

### 缺陷管理

---

+ 目标：对各阶段测试发现的缺陷进行跟踪管理，以保证各级缺陷的修复率达到标准
    1. 及时了解并跟踪每个被发现的缺陷
    1. 确保每个被发现的缺陷都能够被处理
    1. 收集缺陷数据并根据缺陷趋势曲线来识别测试过程是否结束
    1. 收集缺陷数据并在其上进行数据分析，作为组织的过程资产
+ 角色：项目经理、测试负责人、测试人员、开发人员、质量保证员

---

#### 缺陷管理的人员职责 -- 项目经理PM

1. 负责指派缺陷给相关责任人

---

#### 缺陷管理的人员职责 -- 测试负责人TM

1. 决定缺陷管理方式和工具，拟定决策评审计划
1. 管理所有缺陷关闭情况
1. 审核测试人员提交的缺陷
1. 对测试人员的工作质量进行跟踪与评价

---

#### 缺陷管理的人员职责 -- 测试人员（TE）

1. 负责报告系统缺陷记录，且协助项目人员进行缺陷定位
1. 负责验证缺陷修复情况，且填写缺陷记录中相应信息
1. 负责执行系统回归测试
1. 提交缺陷报告
1. 负责被测软件进行质量数据和分析

---

#### 缺陷管理的人员职责 -- 开发人员（DE）

1. 修改测试发现的缺陷，并提交成果物做再测试
1. 负责接收各自的缺陷记录，并且修改
1. 负责提供缺陷记录跟踪中其它相应信息

---

#### 缺陷管理的人员职责 -- 质量保证员（SQA）

1. 监控项目组缺陷管理规程执行情况

---

### 缺陷生命周期

1. 发现缺陷
1. 分配缺陷
1. 修复缺陷
1. 验证缺陷
1. 解决缺陷

---

<!--
_backgroundColor: Beige
_color:
_class:
    - lead
-->

### 缺陷管理系统

---

1. 缺陷管理系统是用来管理软件缺陷整个生命的工作流系统，跟踪缺陷从发生到被修正并发布的整个过程
1. 缺陷管理系统的作用
    1. 提高软件缺陷报告的质量
    1. 确保每一个缺陷能被处理
    1. 知识库

---

#### Bugzilla and ClearQuest

|工具名称|Bugzilla|ClearQuest|
|--|--|--|
|流程定制|支持|支持|
|查询功能|支持|支持|
|邮件通知|支持|支持|
|架构|B/S|C/S，B/S|
|平台|Linux, FreeBSD, Windows|Unix, Windows|
|数据库|MySQL, PostgreSQL<br/>Oracle and SQLite|Oracle, SQL Server|
|复杂度|简单|复杂|
|产品价格|免费|收费|

---

<!--
_backgroundColor: Beige
_color:
_class:
    - lead
-->

### 缺陷分析

---

#### 缺陷统计图表

1. 缺陷趋势图
1. 缺陷分布图
1. 缺陷情况处理统计表

---

#### 缺陷分析指标

1. 缺陷发现率： 将发现的缺陷数量作为时间的函数来评测，即创建缺陷趋势图
1. 缺陷潜伏期
1. 缺陷密度
1. 缺陷清除率

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 软件缺陷管理与CMM

><https://en.wikipedia.org/wiki/Capability_Maturity_Model>

---

### CMM

1. `SW-CMM(Capability Maturity Model for Software, 软件能力成熟度模型)`：简称`CMM`，对软件组织在定义、实施、度量、控制和改善其软件过程的实践中各个发展阶段的描述
1. `CMM`的核心：将软件开发视为一个过程，并根据这一原则对软件开发和维护进行过程监控和研究，以使其更加科学化、标准化，使企业能够更好地实现商业目标

---

### Level 1 - Initial级的缺陷管理

1. **_无章可循_**
1. **_无法跟踪_**：发现缺陷后修改，通常没有人记录缺陷
    1. 无法得知哪些缺陷已缺陷、哪些缺陷未缺陷
    1. 下一轮测试中才有可能知道那些所谓己被纠正了的缺陷是否真的被纠正了，更重要的是纠正过程是否引入了新的缺陷
1. **_大量精力_**：为了获得一个高质量的软件产品（如果可能的话），通常要在测试上花费大量的人力

---

### Level 2 - Repeatable级的缺陷管理

1. **_项目级规范_**：**_软件项目_** 根据自身的需要出发，制定 **_项目级_** 的缺陷管理过程
1. **_完备过程_**：项目组会完整地记录开发过程中的缺陷，监控缺陷的修改过程，并验证修改缺陷的结果

---

### Level 3 - Defined级的缺陷管理

1. **_组织级规范_**：**_软件组织_** 汇集以往项目的经验教训，制定 **_组织级_** 的缺陷管理过程
1. **_项目采用_**：
    1. 要求具体的项目根据组织级的缺陷管理过程定制本项目的缺陷管理过程
    1. 随着组织的不断发展完善，组织的过程会得到持续性的改进，所有项目的过程也都会相应的改进

---

### Level 4 - Managed(Capable)级的缺陷管理

1. **_识别异常_**：**_软件组织_** 根据已收集的缺陷数据采用`SPC(Statistical Process Control, 统计过程控制)`进行 **_统计分析_** ，从而识别出生产过程中的正常波动和异常波动
1. **_监控保障_**：通过分析并实施某种行动来防止已发生的异常再次发生，从而确保开发过程始终处于受控状态

---

### Level 5 - Optimizing(Efficient)级的缺陷管理

1. **_持续改进_** ：`level 5`比`level 4`更强调对 **_软件组织_** 的过程进行 **_持续改进_** ，从而使过程能力得到不断提升
1. **_持续更新_** ：**_软件组织_** 对缺陷在量化理解的基础上持续地改进组织级的开发过程、缺陷发现过程，引入新方法、新工具，加强经验交流，实现缺陷预防
1. **_共性抽象_** ：**_软件组织_** 对缺陷预防着点于缺陷的共性原因

:point_right: 从事件到原理 :point_left:

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok:End of This Slide:ok:
