---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: ""
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 集成测试Integration Testing

---

## Contents

1. :star2: 集成测试概述： 集成测试的定义、集成测试成单元测试的区别、集成测试与系统测试的区别、集成测试的主要任务、集成测试的层次、集成测试的原则
1. :star2: 集成测试的集成策略
1. 集成测试的测试用例设计策略
1. 集成测试的测试过程

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 集成测试概述Integration Testing Overview

---

### 集成测试的定义

+ 集成测试指的是将已通过单元测试的模块按照设计的要求组装成子系统或系统，然后进行测试的测试阶段
+ 集成测试是介于单元测试和系统测试之间的过渡阶段，是单元测试的扩展和延伸
+ “集成”： 将不断开发出的功能集成到系统中，构成系统，集成这个过程也称为“联调”，“联调”之后的测试，即为“集成测试”

---

+ 未经过单元测试的模块不应进行集成测试
+ 通过了单元测试和集成测试仅保证软件系统的功能得以实现，不能保证软件系统在实际运行时 **是否满足用户需求**、**是否仍存在缺陷**

---

### :star2: 集成测试与单元测试的区别

<!--FIXME: 量纲是什么意思
-->

1. 单元测试主要关注模块的内部，虽然它也关注模块接口，但它是从内部来查看接口，从个数、属性、量纲和顺序等方面查看输入的实参与形参的匹配情况
1. 集成测试查看接口时主要关注穿越接口的数据、信息是否正确，是否会丢失

---

### :star2: 集成测试与系统测试的区别

+ 被测对象不同
    1. 集成测试仅针对软件系统本身展开测试
    1. 系统测试涉及的系统不仅包括被测试的软件系统本身，还包括与之交互的第三方软硬件系统和外部数据
+ 测试定位和测试内容不同
    1. 集成测试主要 **_站在开发的角度_** 来评价系统是否实现了功能需求（主要针对功能进行测试）
    1. 系统测试主要 **_站在用户的角度_** 来评价系统是否满足了功能需求和非功能需求

---

+ 对应的构建阶段不同
    1. 集成测试对应概要设计阶段，对应的标准为《软件概要设计说明书》
    1. 系统测试对应系统分析阶段，对应的标准为《软件需求规格说明书》

><https://www.jianshu.com/p/f3ffbd37fd55>
---

### :star2: 集成测试的主要任务

1. 组装模块： 检验通过单元测试的模块是否能组装在一起
1. 检验架构： 检验组装后的软件系统的实现是否符合概要设计
1. 检验功能： 检验组装后的软件系统是否达到预期要求的功能
1. 检验接口： 检验模块间的接口是否正确交互、数据是否丢失
1. 检验误差： 检验单个模块的误差是否会累积放大到不可接受的程度

---

### 集成测试的层次

+ 模块内集成测试、子系统内集成测试和子系统间集成测试
+ 是否称为`“子系统”`
    1. 从程序上看：一般表现为该`“子系统”`是否映射成 **_一个或一个以上的_**、 **_独立的_** 进程，`“子系统”`间通过`IPC`或`RPC`接口通信
    1. 从功能上看：一般表现为该`“子系统”`是否能独立地提供一个完整的功能或服务

<!--
1. 传统软件： 模块内集成测试、子系统内集成测试和子系统间集成测试
1. 面向对象软件： 类内集成测试和类间集成测试

:warning: 实际上，面向对象软件与传统软件并没有明确的区别，是否有区别，主要取决于软件架构

:point_right: 是否可以称为`“子系统”`一般表现为该`“子系统”`是否映射成 **_一个或以上的_**、 **_独立的_** 进程，`“子系统”`间通过`IPC`或`RPC`接口连接
-->
---

### 集成测试的原则

1. :star2: 所有的公共接口都要被测试到
1. :star2: 集成测试应按一定的策略进行
1. :star2: 集成测试应以概要设计为基础
1. :star2: 关键模块必须进行充分的测试
1. :star2: 接口发生变更后，涉及的相关接口必须进行再测试
1. 集成测试应尽早开始
1. 集成测试的策略选择应当综合考虑质量、成本和进度之间的关系
1. 在模块与接口的划分上，测试人员应当和开发人员进行充分的沟通

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 集成测试策略

---

### 辅助模块

1. `driver驱动模块`：驱动模块指用以模拟待测模块的上级模块，驱动模块在集成测试中接受测试数据，把相关的数据传送给待测模块，启动待测模块，并输出相应的结果
1. `stub桩模块/存根模块`：桩模块指用以模拟待测模块工作过程中所调用的模块。桩模块由待测模块调用，它们一般只进行很少的数据处理，例如打印入口和返回，以便于检验待测模块与其下级模块的接口。

---

### 非渐增式集成与渐增式集成

1. 非渐增式集成/非增量式集成： 一步到位的集成测试策略，所有模块均完成单元测试后，组装成软件系统并进行集成测试
1. 渐增式集成/增量式集成： 将下一个要集成测试的模块与已完成集成测试的模块集连接进行集成测试，如此迭代直至所有模块完成集成测试

---

### 非渐增式集成策略

![height:300](./assets_image/non-incremental_integration.png)

1. 分别按`(b)~(g)`对`A~F`进行`unit testing`
1. 按`(a)`进行一次集成测试

---

### 渐增式集成策略 - 自顶向下集成

---

#### 自顶向下集成的原理

1. 从软件的顶层控制层逐步向下将各个模块集成在一起 ==> 逐步集成、逐步测试
    1. 深度优先（横向）
    1. 广度优先（纵向）

---

![height:300](./assets_image/incremental_integration.png)

1. 深度优先： M1 -> M2 -> M5 -> M8 -> M6 -> M3 -> M7 -> M4
1. 广度优先： M1 -> M2 -> M3 -> M4 -> M5 -> M6 -> M7 -> M8

---

#### 自顶向下集成的步骤

1. 对主控模块进行单元测试，被调用模块用`stub`代替真实模块
1. 按所选用的集成策略，选择某个真实模块代替`stub`

---

![height:250](./assets_image/breadth-first_t2b.png)

---

#### 自顶向下集成的优缺点

1. 优点
    1. 测试早期即对控制模块进行检验
    1. 逐步求精，测试人员早期即可看到系统雏形
1. 缺点
    1. 可能遇到逻辑问题，比如： 低层处理的`stub`不能反映真实情况、无法返回真实数据

---

### 渐增式集成策略 - 自底向上集成

---

#### 自底向上集成

<!--FIXME: 完善测试步骤-->

1. 自底向上集成测试原理： 从软件的最底层开始逐步向上形成完整的软件程序
1. 自底向上集成测试步骤
    1. 将低层模块组织成实现某个功能的模块群
    1. 开发一个`driver`
    1. 通过`driver`测试模块群
    1. 将`driver`替换成真实模块

---

![height:500](./assets_image/b2t.png)

---

#### 自底向上集成的优缺点

大多数情况下，自顶向下与自底向上的优缺点相反

---

### 其他集成测试策略 - 三明治集成测试

1. 结合自顶向下和自底向上
1. 三明治集成注重持续集成，尽可能早地进行集成测试
1. 自顶向下集成时，先完成的模块是后完成模块的`driver`
1. 自底向上集成时，先完成的模块是后完成模块的`stub`

---

### 其他集成测试策略 - 核心系统先行集成测试

1. 先进行核心部件的集成测试，再逐步向外扩展
1. 每加入一个外围部件产生一个产品基线，直至最后形成稳定的软件产品
1. 核心系统先行集成测试对应的集成过程是一条逐渐闭合的螺旋形曲线，代表产品逐步定型的过程
1. 优点： 对快速软件开发有较好的效果，适合较复杂的系统的集成测试，能保证一些重要的功能和服务的实现
1. 缺点： 应能明确区分核心部件和外围部件，部件内有高内聚、部件间有低耦合

---

### 其他集成测试策略 - 高频集成测试

1. **_每隔一段时间_** 对现有代码进行一次集成测试
1. 集成次数频繁，须借助自动化工具完成

---

### 集成测试策略的比较 - 非渐增式 vs 渐增式

1. 错误定位和修复
    + 非渐增式的接口问题集中暴露，可能存在定位和修复困难，从而更容易产生`集成地狱（integration hell）`现象
    + 渐增式接口错误分别暴露，较易于错误定位和修复
1. 测试完整性：渐增式的一些模块在逐步测试中得到较为频繁的测试，接口测试较为完整
1. 缺陷暴露时刻：渐增式的部分接口缺陷可能暴露较晚

---

### 集成测试策略的比较 - 自顶向下 vs 自底向上

1. 自顶向下测试的主要优点在于它可以自然地做到逐步求精，一开始便能让测试者看到系统的框架
1. 自底向上的优点在于，由于驱动模块模拟了所有调用参数，即使数据流并未构成有向的非环状图，生成测试数据也没有困难

---

### 集成测试策略的比较 - 三明治 and 核心系统先行

1. 三明治集成测试采用自顶向下、自底向上集成相结合的方式，并采取持续集成的策略，有助于尽早发现缺陷，也有利于提高工作效率
1. 核心系统先行集成测试能保证一些重要功能和服务的实现，对于快速软件开放很有效果

---

### 集成测试策略的选择

1. 传统软件企业： 一般采用自顶向下集成测试为主的集成策略
1. 敏捷软件企业： 基于持续集成原则，综合各种策略

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 集成测试的测试用例设计策略

---

1. 为系统运行而设计用例
    1. 为保证程序可以运行，测试用例应能覆盖程序最主要的执行路径
    1. 规格导出法、应用场景分析法、等价类划分法等
1. 为正向测试而设计用例
    1. 验证需求和设计是否得到满足（正向测试），主要根据概要设计进行测试用例设计
    1. 规格导出法、等价类划分法、应用场景分析法、状态图法和分类推理法等
1. 为逆向测试而设计用例
    1. 以已知的缺陷为依据设计测试用例，用于证明已知的缺陷已修复
    1. 错误猜测法、边界值法、随机数法等

---

1. 为满足特殊需求而设计用例
    1. 为非功能需求等特殊需求设计测试用例，
    1. 一般根据`SRS`导出规格，然后具体设计测试用例
1. 为高覆盖率而设计用例
    1. 为提高覆盖率而设计测试用例
    1. 逻辑覆盖法等
1. 基于模块接口依赖关系来设计用例
    1. 按接口依赖的层级关系设计测试用例

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 集成测试的过程

---

1. 计划阶段： 完成集成测试计划，制定集成测试策略
1. 设计实现阶段： 建立集成测试环境，完成测试设计和开发
1. 执行评估阶段： 执行集成测试用例，记录和评估测试结果

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok:End of This Slide:ok:
