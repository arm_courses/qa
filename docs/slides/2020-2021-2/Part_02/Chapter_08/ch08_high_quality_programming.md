---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_High Quality Programming_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 高质量编程High Quality Programming

---

## Contents

1. :star2: 代码风格与风格规范
1. :star2: 命名规则
1. 提高程序质量的编程技术

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 编程风格/代码风格Programming Style/Code Style/Coding Style/Coding Conventions

>1. <https://en.wikipedia.org/wiki/Programming_style>
>1. <https://en.wikipedia.org/wiki/Coding_conventions>

---

### 代码风格是什么

>`Programming style`, also known as `code style`, is _**a set of rules or guidelines**_ used when writing the source code for a computer program. It is often claimed that following a particular programming style will _**help programmers read and understand source code**_ conforming to the style, and _**help to avoid introducing errors**_.

:point_right: 代码风格自动扫描测试是静态测试的主要形式之一

---

### 代码风格的必要性

1. 增加代码的健壮性、可读性、可维护性、可移植性
1. 减少团队沟通的成本
1. 支持项目资源的复用

:point_right: 统一的、良好的代码风格，是一个优秀而且职业化的开发团队所必需的素质

---

### 代码风格的内容

1. 源文件间的组织结构、源文件内的排版结构
1. 标识符的命名规则
1. 接口参数的定义规则
1. 注释的规范/文档的规范
1. 其他与源代码相关的内容

>“Style” covers a lot of ground, from “use camelCase for variable names” to “never use global variables” to “never use exceptions.”

---

### 典型代码风格 -- Google Style Guides

1. <https://google.github.io/styleguide/>
1. <http://zh-google-styleguide.readthedocs.org/>

---

### 典型代码风格 -- Linux Kernel Coding Style

1. <https://www.kernel.org/doc/html/latest/process/coding-style.html>
1. <https://www.kernel.org/doc/html/latest/translations/zh_CN/process/coding-style.html>

---

### 典型代码风格 -- Alibaba Java Coding Guidelines

1. <https://github.com/alibaba/p3c>
1. <https://developer.aliyun.com/special/tech-java?spm=a2c41.13037006.0.0>

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 命名规则Naming Convention

>1. <https://en.wikipedia.org/wiki/Naming_convention_(programming)>
>1. <https://zh.wikipedia.org/wiki/%E5%91%BD%E5%90%8D%E8%A7%84%E5%88%99_(%E7%A8%8B%E5%BA%8F%E8%AE%BE%E8%AE%A1)>

---

### 基本命名规则 -- 匈牙利命名法Hungarian Notation

1. 据传由`MS`的匈牙利程序员 _Charles Simonyi_ 发明，在`Windows API`和`VC++`中得到广泛应用
1. 基本原则：`标识符名称 = 属性 + 数据类型 + 对象描述`，`属性`和`数据类型`使用小写字母，`对象描述`使用首字母大写

>1. <https://en.wikipedia.org/wiki/Hungarian_notation>
>1. <https://zh.wikipedia.org/wiki/%E5%8C%88%E7%89%99%E5%88%A9%E5%91%BD%E5%90%8D%E6%B3%95>

---

### 基本命名法规则-- 驼峰命名法Camel Case

1. `upper camel case大驼峰命名法`： 又称`Pascal Case帕斯卡命名法`，所有单词首字母大写
1. `lower camel case小驼峰命名法`： 首单词首字母小写，剩余单词首字母大写

>1. <https://en.wikipedia.org/wiki/Camel_case>
>1. <https://zh.wikipedia.org/wiki/%E9%A7%9D%E5%B3%B0%E5%BC%8F%E5%A4%A7%E5%B0%8F%E5%AF%AB>

---

### 基本命名规则 -- 下划线命名法Snake Case

1. 单词间用下划线连接
1. 变量单词均使用小写
1. 常量单词均使用大写

>1. <https://en.wikipedia.org/wiki/Snake_case>

---

### 命名规则 -- 公认规则/共性规则

1. 较短的单词可通过去掉“元音”形成缩写
1. 较长的单词可取单词的头几个字母形成缩写
    + configuration ==> config
1. 公认的单词缩写
    + temp ==> tmp
    + flag ==> flg
    + statistic ==> stat
    + increment ==> inc
    + message ==> msg

---

### 命名规则 -- 公认规则/共性规则（续）

1. 应该在源文件的开始之处，对文件中所使用的缩写或约定，特别是特殊的缩写，进行必要的注释说明
1. 标识符最好采用英文单词或其组合
1. 避免使用汉语拼音进行命名
1. 避免多个标识符仅以数字编号进行区分： `value_1`, `value_2`
1. 命名规则尽量与所采用的`OS`或开发工具的风格保持一致

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 提高程序质量的编程技术Programming Techniques to Improve Quality

---

### 表达式和基本语句Expressions and Statements

1. 避免使用过于复杂的复合表达式
    + `i = a >=b && c < d && c + f <= g + h`
    + `* stat_poi+++=1` ==> `(*stat_poi++) += 1`
1. 避免复合表达式与“数学表达式”混淆
    + `if ( a < b < c )`
1. 注意`conditional expression`
1. 注意`loop`效率
1. 避免多次使用同一个`literal` ==> 应定义`constant`

---

### 函数设计规则Function Design Style

---

#### 函数注释规则

1. 实现复杂逻辑的函数时，提供详细注释
1. 形参名无法提供准确的物理特性时，提供详细注释
1. 形参中存在以位为单元进行使用时，提供详细注释
1. 实参必须符合平台以外的要求时，提供详细注释（“注意事项”中）
1. 调用函数时必须符合特定的前置条件时，提供详细注释（“注意事项”中）
1. 函数使用全局变量，特别是修改全局变量时，提供详细注释（“注意事项”中）

---

#### 函数形参规则

1. 对仅作为输入的形参，尽量使用`const`修饰符
1. 形参数量建议控制在5个以内
1. 避免使用类型或数目不确定的形参，除非必要
1. `...`

---

#### 函数返回值规则

1. 有时候函数原本不需要返回值，但为了增加灵活性如支持链式表达，可以附加返回值
    + `char* strcpy( char* dest, const char* src );`
1. 尽量只有一个返回点
1. 不应返回指向栈内存的指针或引用

---

#### 函数实现规则

1. 参数合法性检查应由调用者还是被调用者负责？
    1. 负责在于调用者，但双方均应检查参数合法性
    1. 被调用者检查参数合法性是“防御性编程”的一种
1. 避免使用全局变量

---

### 内存管理规则

1. 恰当的内存分配方式： `text`, `stack`, `heap`
1. 只使用已分配已初始化的内存空间
1. 及时释放内存
1. 注意空指针/空引用和野指针
1. 注意数组下标越界
1. 注意指针和数组的区别

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 可复用的软件系统Reuseable Software System

---

### 软件设计原则Principles of Software Design

1. OCP (Open-Closed Principle, 开-闭原则)
1. LSP (Liskov Substitution Principle, 里氏代换原则)
1. DIP (Dependence Inversion Principle, 依赖倒转原则)
1. CARP (Composite/Aggregate Reuse Principle, 组合/聚合复用原则)
1. LoD (Law of Demeter, 迪米特法则)
1. ISP (Interface Separate Principle, 接口隔离原则)

---

### DP (Design Pattern) and GoF (Gang of Four)

>at 1994
>
><https://en.wikipedia.org/wiki/Design_Patterns>

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok:End of This Chapter:ok:
