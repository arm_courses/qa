---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Software Reliability Metric and Testing_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 软件可靠性度量和测试Software Reliability Metric and Testing

>软件可靠性是软件质量属性中重要的固有特性和关键要素

---

## Outline

1. :star2: 软件可靠性概述
1. 软件可靠性模型及其评价标准
1. 软件可靠性测试和评估
1. 提高软件可靠性的方法和技术
1. 软件可靠性研究的主要问题

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 软件可靠性概述

---

+ 软件系统规模越做越大越复杂，可靠性越来越难保证
+ 业务对系统运行的可靠性要求越来越高
    1. 一些关键的应用领域，如航空、航天等，其可靠性要求尤为重要
    1. 在银行等服务性行业，其软件系统的可靠性也直接关系到自身的声誉和生存发展竞争能力
+ 软件可靠性比硬件可靠性更难保证，会严重影响整个系统的可靠性

---

+ 在许多项目开发过程中，对可靠性没有提出明确的要求，软件开发商（部门）也不在可靠性方面花更多的精力
    1. 开发时，只注重速度、结果的正确性和用户界面的友好性等，而忽略了可靠性
    1. 投产后，发现大量可靠性问题，增加了维护困难和工作量，严重时只有束之高阁，无法投入实际使用

---

### 软件可靠性发展史

1. 软件可靠性的发展与软件工程、可靠性工程的发展密切相关，尤其与软件工程的关系更加紧密
1. 软件可靠性是软件工程学科派生的分支，同时继承和利用了硬件可靠性工程的理论和方法

---

1. 1950 ~ 1958： 没有专职的程序员，科学家即程序员、科学家即用户，没有软件可靠性概念
1. 1959 ~ 1967： 出现专职的程序员，`software crisis软件危机`时期
1. 1968 ~ 1978： 软件工程学科建立和发展，软件可靠性模型涌现
1. 1978 ~ now：
    1. 软件可靠性标准（1985 `IEC TC56软件可靠性标准工作组`）
    1. 软件可靠性工程（1988 `AT&T Bell Lab`《软件可靠性工程》）

---

### 软件可靠性的定义

+ `IEEE 24765:1983` ==> `GB/T-11457-1989` ==> `GB/T-11457-2006`：
    1. 在规定条件下，在规定的时间内软件不引起系统失效的概率
        + 该概率是系统输入和系统使用的函数，也是软件中存在的缺陷的函数
        + 系统输入将确定是否会遇到已存在的缺陷（如果有缺陷存在的话）
    1. 在规定的时间周期内所述条件下程序执行所要求的功能的能力

---

+ `ISO/IEC/IEEE 24765:2010`
    1. the probability that software will not cause the failure of a system for a specified time under specified conditions
        + NOTE The probability is a function of the inputs to and use of the system as well as a function of the existence of faults in the software. The inputs to the system determine whether existing faults, if any, are encountered.

---

#### 规定时间

1. 日历时间： 自然计时
1. 时钟时间： 自然计时，不含进程不存在的时间
1. 执行时间： 进程执行（`running`）计时

---

#### 规定条件

+ 程序存储环境
+ 运行硬件环境
+ 运行软件环境

---

### 软件可靠性的基本数学关系

1. 失效概率： 一段时间出现失效的可能性，当软件开始运行后，随着时间的延续，其失效概率逐渐增大，在长期运行之后将趋近于1，其可靠度则逐渐降低，并趋近于0
1. 失效率
    1. `MTBF(Mean Time Between Failures, 平均失效间隔时间)`
    1. `MTTF(Mean Time To Failure, 平均失效时间)`

---

### 软件可靠性的影响因素

1. 软件缺陷
1. 健壮性（容错能力）

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 软件可靠性模型及其评价标准

---

+ 软件可靠性模型是围绕着20世纪70年代的先驱工作者Telinski、Moranda、Shooman和Coutinbo等人的工作展开
+ 基本方法： 使用过去的失效数据建立可靠性模型，然后用所建立起来的模型估计现在和预测将来的软件可靠性
    1. 先验条件是给定过去某个时期内的软件失效次数或软件的失效时间间隔，根据模型使用的这两种数据我们将模型分成如下两类：
        1. 给定时间间隔内的失效数模型
        1. 两相临失效间的时间间隔模型

---

### 建立软件可靠性模型的目的和要求

+ 建立软件可靠性模型的目的是估计软件可靠性，提供开发状态、测试状态以及计划日程状态的参考定量数据，监视可靠性性能及其变化
+ 建立软件可靠性模型旨在根据软件可靠性相关测试数据，运用统计方法得出软件可靠性的预测值或估计值
+ 对软件可靠性模型的要求
    1. 软件可靠性模型应有适合具体项目开发过程的正确的假设
    1. 软件可靠性模型应在实际应用中是可行的、有效的

---

### 软件可靠性模型的分类

1. 时间域（Time Domain）：按时钟时间、执行时间（或ＣＰＵ时间）分类；
1. 类别（Category）：根据软件在无限的时间内运行时可能经历的故障数是有限的还是无限的进行分类；
1. 形式（Type）：根据软件在运行时间t时的失效数分布来分类。其中主要考虑泊松分布和二项式分布。
1. 种类（Class）：根据软件发生故障的故障密度对时间的函数形式分类（仅对有限故障类）。
1. 族（Family）：根据软件的故障密度对它的期望故障数的函数形式分类（仅对无限故障类）。

---

### 软件可靠性模型及其应用

1. Musa模型
    1. 基本模型
    1. 对数模型
1. Goel-Okumoto模型

---

|模型|假设|适用阶段|难易度|
|--|--|--|--|
|Mussa基本模型|1. 有限固有缺陷数<br/>2. 常数故障率<br/>3. 指数分布|集成测试后|易|
|Mussa对数模型|1. 无限固有缺陷数<br/>2. 对数分布<br/>3. 故障率随时间变化|单元测试到系统测试|易|
|Goel-Ukumoto|1. 非时齐缺陷分布<br/>2. 缺陷可能因修复而产生<br/>3. 指数、Weibull分布|集成测试后|中|

---

### 软件可靠性模型评价指标

1. 模型拟合性
1. 模型预计有效性
1. 模型偏差
1. 模型偏差趋势
1. 模型噪声

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 软件可靠性评测（测试和评估）

---

+ 软件可靠性评测是软件可靠性工作的重要组成部分：包括测试与评估两个方面的内容，既适用于软件开发过程，也可针对最终软件产品
+ 在软件开发过程中使用软件可靠性评测
    1. 可以更快速地找出对可靠性影响最大的错误
    1. 可以结合软件可靠性增长模型，估计软件当前的可靠性，以确认是否可以终止测试和发布软件
    1. 可以预计软件要达到相应的可靠性水平所需要的时间和测试量，论证在给定日期提交软件可能给可靠性带来的影响
+ 对于最终软件产品，对最终产品进行可靠性验证测试
    1. 确认软件的执行与需求的一致性
    1. 确定最终软件产品所达到的可靠性水平

---

### 软件可靠性测试

+ 软件可靠性评测指运用 **_统计技术_** 对软件可靠性测试和系统运行期间采集的软件失效数据进行处理并评估软件可靠性的过程
+ 软件可靠性评测的主要目的是测量和验证软件的可靠性，当然实施软件可靠性评测也是对软件测试过程的一种完善，有助于软件产品本身的可靠性增长。
+ 软件测试者可以使用很多方法进行软件测试
    1. 按行为或结构来划分输入域的划分测试
    1. 纯粹随机选择输入的随机测试
    1. 基于功能、路径、数据流或控制流的覆盖测试
    1. 其他方法

---

1. 对于给定的软件，每种测试方法都局限于暴露一定数量和一些类别的错误
1. 通过这些测试能够查找、定位、改正和消除某些错误，实现一定意义上的软件可靠性增长
1. 由于它们都是面向错误的测试，测试所得到的结果数据不宜 **_直接_** 用于软件可靠性评估

---

![height:500](./assets_image/softeware_reliability_testing_process_model.png)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 提高软件可靠性的方法和技术

---

1. 建立以可靠性为核心的质量标准
1. 选择合适的软件开发方法
1. 提高软件重用度
1. 使用合适的开发管理工具
1. 加强测试
1. 采用容错设计

---

### 建立以可靠性为核心的质量标准

+ 在软件项目规划和需求分析阶段就要建立以可靠性为核心的质量属性，包括：
    1. 功能
    1. 可靠性
    1. 可维护性
    1. 可移植性
    1. 安全性
    1. 吞吐率
    1. 等等
+ 虽然还没有一个衡量软件质量的完整体系，但还是可以通过一定的指标来指定标准基线

---

+ 软件质量从构成因素上可分为产品质量和过程质量
+ 软件质量还可将质量分为动态质量和静态质量
    1. 静态质量是通过审查各开发过程的成果来确认的质量，包括模块化程度、简易程度、完整程度等内容
    1. 动态质量是考察运行状况来确认的质量，包括
        + `MTBF(Mean Time Between Failures, 平均失效间隔时间)`
        + `MTTF(Mean Time To Failure, 平均失效时间)`
        + `MTRF(Mean Time To Repair Fault, 平均修复时间)`
        + 资源利用率

---

### 选择合适的软件开发方法

---

### :star2: 提高软件重用度

1. 软件重用不仅仅是指软件本身，也可以是软件的开发思想方法、文档，甚至环境、数据等
    1. 开发过程重用： 开发规范、各种开发方法、工具和标准等
    1. 软件组件重用： 文档、程序和数据等
    1. 知识重用： 相关领域专业知识的重用
1. 狭义的软件重用指的是软件组件重用

---

### 使用合适的开发管理工具

1. 仅靠人工管理不足以管理大型软件系统的开发项目
1. 开发管理工具辅助解决开发过程中的问题，提高开发效率和产品质量

---

### 加强测试

1. 测试规范包括以下三类文档：
    1. 测试设计规范：详细描述测试方法，规定该设计及其有关测试所包括的特性。还应规定完成测试所需的测试用例和测试规程，规定特性的通过/失败判定准则
    1. 测试用例规范：列出用于输入的具体值及预期输出结果。规定在使用具体测试用例时对测试规程的各种限制
    1. 测试规程规范：规定对于运行该系统和执行指定的测试用例来实现有关测试所要求的所有步骤

---

### 采用容错设计

1. 提高软件可靠性的技术： 避免缺陷和冗余容错

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 软件可靠性研究的主要问题

---

1. 观点、方法和工具
1. 软件可靠性模型： 模型、模型应用
1. 软件运行数据收集与监控
1. 软件测试用例自动生成
1. 软硬件混合系统可靠性模型

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
