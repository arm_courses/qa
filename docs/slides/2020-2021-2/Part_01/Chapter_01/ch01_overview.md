---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Overview_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 概述

## Overview

---

## Contents

1. 硬件与软件、软件的特征
1. 软件生命周期`SDLC`、敏捷`Agile`、开发运维一体化`DevOps`
1. :star2: 软件质量保证`SQA`与软件质量控制`SQC`
1. 软件质量保证工程师、软件开发工程师与软件测试工程师

<!--FIXME:
next version use SQC in SQM scope, and use ST in SDLC scope
-->

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 硬件与软件Hardware vs Software

---

### 软件的特征Features of the Software

1. 软件是开发产生的，而不能用传统方法批量制造
1. 软件不像硬件一样有磨损
1. 很多软件不能通过已有构件组装而成，只能个性化定义和开发

---

### 硬件和软件的失效曲线Failure Curve of Hardware and Software

![height:400](./assets_image/hardware_failure_curve.png)![height:400](./assets_image/software_failure_curve.png)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

### 软件产品生命周期`SPLC`与软件开发生命周期`SDLC`

+ `SDLC` = 软件定义阶段 + 软件开发阶段
+ `SPLC` = `SDLC` + 软件运维阶段
+ `SPLC` == 软件生命周期

---

#### 软件定义阶段/软件需求阶段/软件分析阶段

+ 集中于 **“做什么”（what to do）/“做对的事情”（do right thing）**
    1. 定义痛点 ==> 问题定义
    1. 定义需求 ==> 需求分析
    1. 确认需求 ==> 需求确认

---

#### 软件开发阶段/软件构造阶段

+ 集中于 **“如何做”（how to do）/“将事情做对”（do thing right）**
    1. 映射需求 ==> 设计（概要设计、详细设计）
    1. 实现需求 ==> 编码
    1. 构建需求 ==> 构建
    1. 验证需求 ==> 测试
    1. 交付需求 ==> 发布

⚠️ 有时将“软件定义阶段 + 设计”合并称为“计划阶段” ⚠️

---

#### 软件运维更新阶段

+ 集中于 **“如何实现价值”（how to achieve）** 和 **“如何迭代升级”（how to upgrade iteratively）**
    1. 实现需求价值 ==> 部署、运维（运行+维护）
    1. 发掘新需求 ==> 运维分析
    1. 无法满足需求 ==> 软件退出

---

![width:1140](./assets_image/DevOps-process-flow.png)

---

![height:600](./assets_image/iron_triangle_paradigm_shift.jpg)

---

![height:600](./assets_image/what-is-DevOps.png)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## :star2: 软件质量保证与软件质量控制SQA vs SQC

---

### 软件缺陷与软件质量Software Defect and Software Quality

+ 软件缺陷`software defect`(noun)：指存在于软件（程序、数据或文档）中的不希望或不可接受的偏差
+ 软件质量`software quality(SQ)`(noun)：指软件（程序、数据或文档）与明确的和隐含的定义的需求相一致的程度

<!--

### 软件缺陷Software Defect

A `Software Defect/Bug/Fault` is a condition in a software product which _**does not meet a software requirement (as stated in the requirement specifications)**_ or _**end-user expectation (which may not be specified but is reasonable)**_. In other words, a defect is _**an error**_ in _**coding**_ or _**logic**_ that causes a program to malfunction or to produce _**incorrect/unexpected**_ results.

><https://softwaretestingfundamentals.com/defect/>

-->

---

### 软件质量保证与软件质量控制/软件测试SQA and SQC/ST

1. `SQA (Software Quality Assurance)`(verb)： 定义和评估软件过程的适当性，通过提供证据来表明软件过程是合适的，其生产的软件产品质量符合预期目标
1. `SQC (Software Quality Control)` (verb)/`ST (Software Testing)`(verb)： 在软件业界，`SQC`与`ST`为同义词，验证软件在有限的测试用例集时是否可产生期望的结果

:warning: `SQ`, `QA`/`SQA`, `QC`/`SQC`是公认缩写，`ST`是自造缩写 :warning:

---

### 软件质量保证与软件测试SQA and ST

1. `QC`的概念早于`QA`约10年诞生
1. `SQA` vs `ST`
    1. :arrow_left: 观点一： `ST`是`SQA`工作的一个活动，即`ST`是`SQA`的一个子集
    1. :arrow_right: 观点二（主流观点）： `ST`主要针对 **结果**，`SQA`主要针对 **过程**，`ST`属于 **技术领域**，`SQA`属于 **工程领域**
    1. 其它观点

>1. [软件测试与质量保障之间的关系](https://blog.csdn.net/QA_Aliance/article/details/6778108)
>1. [质量保证、质量控制让你不再傻傻分不清楚](https://zhuanlan.zhihu.com/p/43732189)

---

![height:500](./assets_image/ISO9001_certificate.jpg)

>[ISO9001认证/ISO9000认证中文证书含义介绍](http://www.hzbh.com/show.asp?id=797)

---

![height:500](./assets_image/cmmi_dev_v1.3_certificate.jpg)

><http://www.only-china.com/About_524.html>

---

![height:600](./assets_image/qualified_certificate.jpg)

---

### :star2: 软件质量保证与软件测试的联系Relationships of SQA and ST

1. `SQA`和`ST`都是为了提升`SQ`
1. `SQA`和`ST`都属于`SQM`范畴
1. `ST`这一过程属于`SQA`所关注的过程之一
1. `SQA`的实现手段采用了`ST`中的静态方法（检查分析）

:warning: 软件质量保证 != 保证软件质量 :warning:

---

### :star2: 软件质量保证与软件测试的区别Differences between SQA and ST

1. `SQA`关注生产产品的过程（向管理层反馈是否按照过程规范执行过程活动），`ST`关注产品本身（向管理层反馈产品质量是否合格）
1. `SQA`属于事前预防、事中监督、事后定责的管理手段，`ST`属于事后检查、事后纠偏的技术手段
1. `SQA`主要属于工程管理领域，`ST`主要属于技术领域

---

### 软件质量保证、软件测试与软件质量管理SQA, ST and SQM

1. `SQA`： 监控软件工程流程和方法以确保质量的一系列手段（`ISO 9000`/`ISO 9001`，`CMM`/`CMMI`等模型），SQA涵盖整个`SDLC`
1. `ST`： 对过程中的产出物（代码和文档）进行走查或运行，以找出问题，报告质量，并对发现的问题的进行分析、追踪与回归测试
1. `SQM` $\approx$ `SQA` + `SQC`/`ST`

><https://baike.baidu.com/item/%E8%BD%AF%E4%BB%B6%E8%B4%A8%E9%87%8F%E4%BF%9D%E8%AF%81>
><https://zh.wikipedia.org/wiki/%E8%BD%AF%E4%BB%B6%E8%B4%A8%E9%87%8F%E4%BF%9D%E8%AF%81>

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 软件质量保证工程师、软件开发工程师与软件测试工程师SQA Engineer, Software Engineer and ST Engineer

---

### 开发团队Development Team

1. 开发团队：`software architect` + `software designer`/`software engineer` + `software developer`/`coder`
1. 大开发团队：开发团队 + `software tester`

---

### 软件质量保证工程师SQA Engineer

1. `SQA Engineer`不属于大开发团队，而是归属于一个独立的团队
1. 以独立审查方式，从第三方的角度监控`SDLC`的执行

---

### 软件测试工程师级别Evolution of ST Engineer

1. 测试员： 入门级工作，负责 **执行** 测试
1. 测试分析师： 负责测试生命周期 **计划与执行**，包括： 计划测试、设计测试、执行测试
1. 高级测试分析师： 测试团队中的 **主角**
1. 测试团队领导（测试总监）： 为测试而 **负责**
1. 测试顾问： **制定或协助** 制定测试策略、**监管** 测试
1. 高级测试顾问： 充当多个测试团队的顾问
1. 首席测试顾问： 定义、实现公司级的测试策略、监管公司级的测试

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok:End of This Chapter:ok:
