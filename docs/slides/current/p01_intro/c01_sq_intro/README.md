---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Introduction to Software Quality_"
footer: "@aRoming"
math: katex
---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Introduction to Software Quality软件质量概述

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [⭐ 软件与软件行业](#软件与软件行业)
2. [🌟 软件质量与软件缺陷](#软件质量与软件缺陷)
3. [⭐ 软件质量管理与PDCA](#软件质量管理与pdca)
4. [🌟 软件质量保证与软件质量控制](#软件质量保证与软件质量控制)
5. [⭐ 软件企业的组织架构](#软件企业的组织架构)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## ⭐ 软件与软件行业

---

🤔 为什么软件这么特别，有专门的"软件危机" 🤔

---

### 软件危机的独特原因

1. 需求难于无歧义描述
    1. 用户难于准确描述需求，开发难于理解用户的实质需求
    1. 要求使用形式化语言描述需求不现实
1. 需求变更代价"低"
    1. 软件需求变更的成本主要体现在用户或客户"看不见"的成本（时间）
    1. 要求用户或客户理解软件需求变更背后涉及的复杂度难度大

---

1. 难于工件化/制品化`artifact`
    1. 软件功能强大、逻辑灵活，每个组件都"独特"
    1. 软件与具体业务紧密相关，具体业务各异从而致使每个组件都"独特"

---

### 软件产品生命周期`SPLC`与软件开发生命周期`SDLC`

- `SPLC` = 软件定义阶段 + 软件开发阶段 + 软件运维阶段
- `SDLC` = 软件定义阶段 + 软件开发阶段
- `SPLC` = `SDLC` + 软件运维阶段
- `SPLC`也称为`SLC(Software Life Cycle, 软件生命周期)`

---

#### 软件定义阶段/软件需求阶段/软件分析阶段

- 集中于 **"做什么"（what to do）/"做对的事情"（do right thing）**
    1. 定义困难 ==> 问题定义
    1. 定义需求 ==> 需求分析
    1. 确认需求 ==> 需求确认

---

#### 软件开发阶段/软件构造阶段

- 集中于 **"如何做"（how to do）/"将事情做对"（do thing right）**
    1. 映射需求 ==> 概要设计、详细设计
    1. 实现需求 ==> 编码
    1. 构建需求 ==> 构建
    1. 验证需求 ==> 测试
    1. 交付需求 ==> 发布

⚠️ 有时将"软件定义阶段 + 设计"合并称为"计划阶段" ⚠️

---

#### 软件运维阶段

- 集中于 **"如何实现价值"（how to achieve）** 和 **"如何发现新需求"（how to upgrade）**
    1. 实现需求价值 ==> 部署、运维（运行+维护）
    1. 发掘新需求 ==> 业务分析、迭代周期
    1. 无法满足需求 ==> 软件退出

---

### `SPLC`, `Agile` and `DevOps`

![height:520](./.assets/image/splc_agile_cicd_devops.png)

---

### Dev, Ops, QA and DevOps

![height:480](./.assets/image/dev_ops_qa_and_devops.png)

❗ ⚠️ 图中的`QA`包括本课程的`SQA`和`SQC` ⚠️ ❗

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 软件质量与软件缺陷

---

- 软件 = 程序 + 数据 + 文档
- 软件质量`software quality(SQ)`(noun)：指软件与明确的和隐含的定义的需求相一致的程度
- 软件缺陷`software defect`(noun)：指存在于软件中的不希望或不可接受的偏差

<!--

### 软件缺陷Software Defect

A `Software Defect/Bug/Fault` is a condition in a software product which _**does not meet a software requirement (as stated in the requirement specifications)**_ or _**end-user expectation (which may not be specified but is reasonable)**_. In other words, a defect is _**an error**_ in _**coding**_ or _**logic**_ that causes a program to malfunction or to produce _**incorrect/unexpected**_ results.

><https://softwaretestingfundamentals.com/defect/>

-->



---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## ⭐ 软件质量管理与PDCA

>1. [PDCA](https://en.wikipedia.org/wiki/PDCA)
>1. [Project Management](https://en.wikipedia.org/wiki/Project_management)

---

### PDCA

1. "Plan–Do–Check–Act" or "Plan–Do–Check–Adjust"
1. `PDCA`是管理的基本方法

>1. It is also known as the `Deming circle/cycle/wheel`, the `Shewhart cycle`, the `control circle/cycle`, or `plan–do–study–act (PDSA)`
>1. Later in Deming's career, he modified `PDCA` to "Plan, Do, Study, Act" (`PDSA`) because he felt that "check" emphasized inspection over analysis.

---

![height:600](./.assets/image/pdca_cycle.png)

---

![width:1120](./.assets/image/pdca_multi-loop.png)

---

### Project Management Processes

![width:1120](./.assets/image/pm_phases.png)

---

### SQ and PM Iron Triangle

![height:450](./.assets/image/pm_triangle.png)

>[Iron Triangle — Triple Constraints of Project Management](https://medium.com/@harpreet.dhillon/iron-triangle-triple-constraints-of-project-management-e818e631826c)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 软件质量保证与软件质量控制

>1. [Software Quality Management](https://en.wikipedia.org/wiki/Software_quality_management)
>1. [Software Quality Control](https://softwaretestingfundamentals.com/software-quality-control/)
>1. [软件测试与质量保障之间的关系](https://blog.csdn.net/QA_Aliance/article/details/6778108)
>1. [质量保证、质量控制让你不再傻傻分不清楚](https://zhuanlan.zhihu.com/p/43732189)
>1. [Quality Assurance, Quality Control and Testing — the Basics of Software Quality Management](https://www.altexsoft.com/whitepapers/quality-assurance-quality-control-and-testing-the-basics-of-software-quality-management/)

---

### `SQM`, `SQP`, `SQA`, `SQC`

1. `SQM` = `SQP` + `SQA` + `SQC`
1. `SQP(Software Quality Planing)`：识别和定义质量管理框架、质量标准及其评估方法
1. `SQA(Software Quality Assurance)`：定义、评估和跟综软件过程的适当性，以向核心干系人表明软件过程是合适的、保证其生产的软件产品质量将符合预期目标
1. `SQC(Software Quality Control)`：验证开发的产品符合预期目标开展的一系列活动，提供产品质量的最终反馈

<!--
1. `testing`：验证软件在有限的测试用例集时是否可产生期望的结果
-->

---

![height:500](./.assets/image/ISO9001_certificate.jpg)

>[ISO9001认证/ISO9000认证中文证书含义介绍](http://www.hzbh.com/show.asp?id=797)

---

![height:500](./.assets/image/cmmi_dev_v1.3_certificate.jpg)

><http://www.only-china.com/About_524.html>

---

![height:600](./.assets/image/qualified_certificate.jpg)

---

1. `SQC` VS `testing`
    1. 本文观点：在软件业界，`SQC`与`testing`为同义词
    1. 其他观点：`SQC` = `testing` + `review`
1. `SQA` VS `SQC`
    1. 本文观点： `SQC`主要针对 **结果**，`SQA`主要针对 **过程**，`SQC`属于 **技术领域**，`SQA`属于 **工程项目领域**，`SQA`使用了`SQC`的技术
    1. 其他观点： `SQC`是`SQA`工作的一个活动，即`SQC`是`SQA`的一个子集
1. `QC`的概念早于`QA`约10年诞生

---

### 🌟 软件质量保证与软件质量控制的联系Relationships of SQA and SQC

1. `SQA`和`SQC`都是为了提升`SQ`
1. `SQA`和`SQC`都属于`SQM`范畴
1. `SQC`这一过程属于`SQA`所关注的过程之一，`SQC`活动在`SQA`的管控范围内
1. `SQA`的实现手段使用了`SQC`的技术

⚠️ 软件质量保证 != 保证软件质量 ⚠️

---

### 🌟 软件质量保证与软件质量控制的区别Differences between SQA and SQC

1. `SQA`关注生产产品的过程（反馈是否按照过程规范执行过程活动），`SQC`关注产品本身（反馈产品质量是否合格）
1. `SQA`关注`SPLC`全生命周期各阶段，`SQC`主要关注`SPLC`的`coding`和`testing`（也关注`requirement`和`design`阶段的产出物）

---

1. `SQA`属于事前预防、事中监督、事后定责的管理手段，`SQC`属于事末检查、事末纠偏的技术手段
    1. `SQA`：监控软件工程流程和方法以确保质量的一系列手段，`SQA`贯穿整个`SPLC`
    1. `SQC`：对过程中的产出物（代码和文档）进行走查或运行，以找出问题，报告质量，并对发现的问题的进行分析、追踪与回归测试
1. `SQA`主要属于工程管理领域，`SQC`主要属于技术领域

---

>1. <https://baike.baidu.com/item/%E8%BD%AF%E4%BB%B6%E8%B4%A8%E9%87%8F%E4%BF%9D%E8%AF%81>
>1. <https://zh.wikipedia.org/wiki/%E8%BD%AF%E4%BB%B6%E8%B4%A8%E9%87%8F%E4%BF%9D%E8%AF%81>

---

### Test vs Testing

1. test
    - countable noun
    - specificity: refer to a specific type of action
1. testing
    - uncountable noun
    - generalization: refers to a broader range of actions

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## ⭐ 软件企业的组织架构

---

### 开发团队Development Team

1. 开发团队：`architect` + `engineer`/`designer` + `developer`/`coder`
1. 大开发团队：开发团队 + `testing engineer`/`tester`

---

### 质量团队Quality Team

1. `QA团队`：`SQA Engineer`
1. 质量团队：`tester` + `SQA Engineer`

❗ ❗ ❗

1. 有些书籍或文档或组织将`tester`称为`QA小组`
1. 理论上，不存在一个实际的"质量团队"，因为`QA`应独立审查

❗ ❗ ❗

---

### 软件质量保证工程师SQA Engineer

1. `SQA Engineer`不属于大开发团队，而是归属于一个独立的团队
1. 以独立审查方式，从第三方的角度监督和控制`SPLC`(特别是`SDLC`)的执行

---

### 软件测试工程师级别Level of Testing Engineer

1. 测试员：入门级工作，负责 **执行** 测试
1. 测试分析师：负责测试生命周期 **计划与执行**，包括：计划测试、设计测试、执行测试、报告测试
1. 高级测试分析师：测试团队中的 **主角**
1. 测试团队领导（测试总监）：为测试而 **负责**
1. 测试顾问：**制定或协助** 制定测试策略、**监管** 测试
1. 高级测试顾问：充当多个测试团队的顾问
1. 首席测试顾问：定义、实现公司级的测试策略、监管公司级的测试

---

### Google开发团队角色

1. `SWE(Software Engineer, 软件开发工程师)`：负责功能实现和这些功能的质量，`SWE`和`SET`一起编写测试代码
1. `SET(Software Engineer in Test, 软件测试开发工程师)`：负责提供测试支持的开发人员，即，`SET`编写代码，通过这些代码提供的功能让`SWE`能够自已测试他们的功能
1. `TE(Test Engineer, 测试工程师)`：负责从用户角度的测试，即，验证是否满足用户的需求

>_"How Google Tests Software"_.

---

![height:600](./.assets/image/how_google_tests_software.jpeg)

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
