---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Software Quality Standards_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 软件质量管理标准Software Quality Management Standards

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [🌟 软件质量度量与软件质量模型](#软件质量度量与软件质量模型)
2. [🌟 标准类型与标准化组织](#标准类型与标准化组织)
3. [软件产品质量模型](#软件产品质量模型)
4. [软件过程质量模型](#软件过程质量模型)
5. [软件缺陷模型](#软件缺陷模型)
6. [`IEEE`软件工程标准](#ieee软件工程标准)
7. [其他标准](#其他标准)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 软件质量度量与软件质量模型

>1. [quality-models](https://github.com/AstroMatt/book-dev/blob/master/sonarqube/quality-models.rst)

---

1. 测量measure: **_ascertain_** the size, amount, or degree of something
1. 测量法measurement: a **_unit or system_** of measuring
1. 度量metric: a **_system or standard_** of measurement
1. 指标indicator: a thing that indicates the **_state or level_** of something

❗ metric有时也翻译成"指标"，如：monitor metrics监控指标 ❗

![height:260](./.assets/diagram/metric.png)

<!--

1. `measure`：度量（名词），是根据一定的规则赋予软件过程或产品属性的数值或类
1. `measure`：度量（动词），按照度量过程中的过程定义，对软件过程或软件产品实施度量，表示实际的动作
1. `measurement`：测量法（名词），是按照一定的尺度用度量（名词）给软件实体属性赋值的过程的方法
1. `metric`：度量，是已定义的测量方法和测量尺度
1. `indicator`：指标，用于评价或预测其他度量的度量

👉 meter- = to measure 👈
-->

---

1. 质量模型定义了度量体系，包括属性、指标、测试法等，通过测量产品或过程得到数值，进而得到指标值，从而回答质量的好坏
1. 三种度量方法论
    1. 从模型到对象：先建立（选择或创建）质量模型，再按质量模型测量对象
    1. 从对象到模型：先分析对象，再建立质量模型
    1. 结合应用上述方法
1. 软件质量模型分为软件产品质量模型和软件过程质量模型

---

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 从对象到模型

---

#### `GQM(Goal-Question-Metric, 目标问题度量法)`

1. `GQM`是什么
    - `GQM`是一种系统的、定量化的构建度量体系（模型）的方法
    - `GQM`是一种通过确定软件质量目标、并且连续监视这些目标是否达成、从而保证软件质量的方法
1. `GQM`的目标： 客户所希望的质量需求的 **_定量_** 的说明

><https://blog.csdn.net/binnacler/article/details/5776226>

---

#### `GQM`的设计步骤

1. 目标（概念级conceptual level）： 对一个项目的各个方面（产品、过程和资源）规定具体的目标，这些目标的表达应非常明确
1. 问题（操作级operational level）： 对每一个目标，要引出一系列能反映出这个目标是否达到要求的问题，并要求对这些问题进行回答。这些问题的答案将有助于使目标定量化
1. 度量（量化级quantitative level）： 将回答这些问题的答案映射到对软件质量等级的度量上，根据这种度量得出软件目标是否达到的结论，或确认哪些做好了，哪些仍需改善

<!--
1. 数据： 收集数据，要为收集和分析数据做出计划
-->

---

1. G：确定一种新的编程语言Jae的使用效果
1. Q
    1. 使用Jae语言的程序员是谁？
    1. 使用Jae语言编写的软件代码质量如何？
    1. 使用Jae语言编写代码的生产率如何？
1. M
    1. 具有N年编程经验的开发者的百分比
    1. 每千行代码中的缺陷数
    1. 每月编写代码的行数

><https://blog.csdn.net/binnacler/article/details/5776226>

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 软件质量度量

---

#### 度量的目的和价值

- 目的：管理的需要，利用度量改进产品和过程
- 价值
    - 度量已发生的事物是为了进行评估和跟综
    - 度量未发生的事物关联的事物是为了进行预测

👉 无法记录则无法度量，无法度量则无法管理 👈

---

#### 度量对于软件及软件学科的价值

1. 可度量性是一个学科是否达到成熟的一大标志，度量使软件开发逐渐趋向专业、标准和科学
1. **_CMU-SEI: Software Measurement Guidebook_**
    1. 通过软件度量增加理解
    1. 通过软件度量管理软件项目，主要是计划与估算、跟踪与确认
    1. 通过软件度量指导软件过程改善，主要是理解、评估和包装，软件度量对于不同的实施对象，具有不同的效用

👉 度量的作用 ==> 可知、可控 👈

---

#### 度量对软件公司的作用的具体表现

1. 改善产品质量
1. 改善产品交付
1. 提高生产能力
1. 降低生产成本
1. 建立项目估算的基线
1. 了解使用新的软件工程方法和工具的效果和效率
1. 提高顾客满意度
1. 创造更多利润
1. 构筑员工自豪感

---

#### 度量对项目经理的作用的具体表现

1. 分析产品的错误和缺陷
1. 评估现状
1. 建立估算的基础
1. 确定产品的复杂度
1. 建立基线
1. 从实际上确定最佳实践

---

#### 度量对软件开发人员的作用的具体表现

1. 可建立更加明确的作业目标
1. 可作为具体作业中的判断标准
1. 便于有效把握自身的软件开发项目
1. 便于在具体作业中实施渐进性软件开发改善活动

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 🌟 标准类型与标准化组织

>1. <https://www.jianshu.com/p/8c0cdcb21789>

---

1. `standard标准`
1. `specification规约、规范`
1. `convention习俗、惯例、协定`
1. `style风格`

---

<!--TODO:
应该从两个维度划分：区域维度和领域维度？
-->

1. 国际标准： 由国际机构颁布的，供各国 **_参考_** 的标准
1. 国家标准： 由国家机构颁布的，适用于本国的标准
1. 行业标准： 由行业机构、学术团体或国防机构颁布的，适用于某个业务领域的标准
1. 企业规范： 由企业颁布的，适用于本企业的规范
1. 项目规范： 由项目组织颁布的，适用于本项目的规范，一般为具体的操作规范，主要特点有：目标明确、项目专用、使用范围

---

### 国际机构/国际标准

1. 🌟 国际标准化组织（International Standards Organization, `ISO`）
    1. founded: February 23, 1947, London, United Kingdom
    1. headquarters: Geneva, Switzerland
1. 🌟 国际电工委员会（International Electrotechnical Commission, `IEC`）
    1. founded: June 26, 1906, London, United Kingdom
    1. headquarters: Geneva, Switzerland
1. 🌟 国际电信联盟（International Telecommunication Union, `ITU`）
    1. founded: May 17, 1865
    1. headquarters: Geneva, Switzerland

---

1. 🌟 欧洲计算机制造联合会（European Computer Manufactures Association, `ECMA`）
    1. founded: 1961
    1. headquarters: Geneva, Switzerland
1. 欧洲电信标准协会（European Telecommunications Standards Institute, `ETSI`）
    1. founded: 1988
    1. headquarters: Sophia Antipolis, France

---

1. 🌟 万维网联盟（World Wide Web Consortium, `W3C`）
    1. founded: October 1, 1994
    1. headquarters: Cambridge, MA, USA
1. 🌟 Linux基金会（The Linux Foundation, `LF`）
    1. founded: 2000
    1. headquarters: San Francisco, CA, USA
1. 云原生计算基金会(Cloud Native Computing Foundation, `CNCF`)
    1. founded: 2015
    1. parent organization: `LF`

---

#### U.S.-based国际组织

1. 美国计算机协会（Association for Computing Machinery, `ACM`）
    1. founded: 1947
    1. headquarters: New York, USA
1. 美国项目管理协会（Project Management Institute, `PMI`）
    1. founded: 1969
    1. headquarters: Pennsylvania, USA

---

### 国家机构/国家标准

1. 🌟 中华人民共和国国家标准（GuoBiao, `GB`）：`GB`、`GB/T`、`GB/Z`
1. 🌟 美国国家标准协会（American National Standards Institute, `ANSI`）
1. 美国国家标准技术学会（National Institute of Standards and Technology, `NIST`）
1. 美国联邦信息处理标准（Federal Information Processing Standards, `FIPS`）：`NIST`针对联邦政府计算机系统制订的标准和准则

---

1. 英国国家标准协会（British Standard Institute, `BSI`）
1. 德国国家标准协会（Deutsches Institut für Normung, `DIN`）
1. 日本工业标准调查会（Japanese Industrial Standards Committee, `JISC`）
1. 日本工业标准（Japanese Industrial Standard, `JIS`）：由`JISC`制定的日本国家级标准

---

### 行业组织/行业标准

1. 🌟 美国电气和电子工程师学会（Institute Of Electrical and Electronics Engineers, `IEEE`）
1. 中华人民共和国国家军用标准（GuoJunBiao, `GJB`）
1. 美国能源信息署（U.S. Energy Information Administration, `EIA`）
1. 美国国防部标准（Department of Defense-Standards, `DOD-STD`）
1. 美国军用标准（Military-Standards, `MIL-S`）

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 软件产品质量模型

---

### `Bass`

1. 分类方法：one way to classify quality attributes distinguishes those characteristics that are discernible through execution of the software (external quality) from those that are not (internal quality)
1. 该分类方法未包括功能的正确性

>_"Software Requirement (3e) - 14.Beyond functionality"_

---

1. external quality外部质量属性（10个属性）
    - primarily important to users
    - describe characteristics that are observed when the software is executing
1. internal quality内部质量属性（5个属性）
    - more significant to development and maintenance
    - are not directly observed during execution of the software
    - that a developer or maintainer perceives while looking at the design or code to modify it, reuse it, or move it to another platform

---

#### external quality

|quality|description|
|--|--|
|Availability     |The extent      to            which       the system      's services are available when |and where they are needed|
|Installability   |How easy        it     is     to              correctly    install, uninstall, and reinstall the application|
|Integrity        |The extent      to            which       the system       protects against data inaccuracy and loss|
|Interoperability |How easily  the system        can             interconnect and exchange data with other systems or components|
|Performance      |How quickly     and           predictably the system       responds to user inputs or other events|

---

|quality|description|
|--|--|
|Reliability      |How long    the system        runs            before       experiencing a failure|
|Robustness       |How well    the system        responds        to           unexpected operating conditions|
|Safety           |How well    the system        protects        against      injury or damage|
|Security         |How well    the system        protects        against      unauthorized access to the application and its data|
|Usability        |How easy        it     is for people          to           learn, remember, and use the system|

---

#### internal quality

|quality|description|
|--|--|
|Efficiency    |How efficiently the           system     uses      computer  resources                                                              |
|Modifiability |How easy        it         is to         maintain, change  , enhance  , and restructure the system                                  |
|Portability   |How easily      the           system     can       be        made      to work in other operating environments                      |

---

|quality|description|
|--|--|
|Reusability   |To  what        extent        components can       be        used      in other systems                                             |
|Scalability   |How easily      the           system     can       grow      to        handle more users, transactions, servers, or other extensions|
|Verifiability |How readily     developers    and        testers   can       confirm   that the software was implemented correctly                  |

---

### 🌟 `McCall`

><https://www.geeksforgeeks.org/mccalls-quality-model/>

---

![height:600](./.assets/image/mccall_quality_factors.jpg)

---

1. 3个维度，11个属性

---

#### 产品运行维度Product Operation

1. 正确性`correctness`：满足需求规格和实现用户任务目标的程度
1. 高效性`efficiency`：完成其功能所需的计算资源和代码的程度
1. 完整性`integrity`：对未授权人员访问软件或数据的可控制程度
1. 可靠性`reliability`：在一定时间内、一定条件下无故障地执行功能的能力
1. 可用性`usability`：学习、操作、理解软件功能所需的工作量

---

#### 产品修正维度Product Revision

1. 可维护性`maintainability`：定位和修复程序中一个错误所需的工作量
1. 灵活性`flexibility`：修改一个运行的程序所需的工作量
1. 可测试性`testability`：测试一个程序以确保她完成所期望的功能所需的工作量

---

#### 产品转移维度Product Transition

1. 可移植性`portability`：将一个程序从一个硬件和（或）软件系统环境移植到另一个环境所需的工作量
1. 可复用性`re-usability`：使一个程序可以在另外一个应用程序中复用的程度
1. 互操作性`interoperability`：连接一个系统和另一个系统所需的工作量

---

### 🌟 `ISO/IEC 9126`(replaced by `ISO/IEC 25010:2011`)

1. `ISO`在`McCall`等人所提出的软件质量模型的基础上，于1991年发布`ISO/IEC 9126`质量模型国际标准
1. `ISO/IEC 9126`：6大类，21个属性six product quality characteristics, and 21 sub-characteristics

1. `ISO/IEC 25010:2011`：8大类，31个属性eight product quality characteristics, and 31 sub-characteristics

---

1. 功能性`functionality`：适合性、准确性、互操作性/互用性、依从性、安全性
1. 可靠性`reliability`：成熟性、容错性、可恢复性
1. 可用性`usability`：可理解性、易学性、易操作性
1. 高效性`efficiency`：时间特性、资源特性
1. 可维护性`maintainability`：可分析性、可改变性、稳定性、可测试性
1. 可移植性`portability`：适应性、可安装性、一致性、可替换性

>[wikipedia.ISO/IEC 9126 Software engineering — Product quality.](https://en.wikipedia.org/wiki/ISO/IEC_9126)

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 软件过程质量模型

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 🌟 ISO质量管理体系ISO 9000 Family

---

- 通过`ISO 9000`认证，指的是通过`ISO 9001`认证
    - `ISO 9000`是基础与词汇表
    - `ISO 9004`无需认证
    - `ISO 19011`是指南
    - `ISO 9002`和`ISO 9003`已废弃
- `ISO 9001`：设计、开发、生产、安装、服务的`QM`模型
    1. 属于`ISO 9000`簇标准体系，**适用于所有工程行业**
    1. `ISO 9001`定义了通过认证所必须实现的需求，企业针对每一条需求应建立相应的政策和过程，并依照这些政策和过程开展生产活动

---

- `ISO/IEC 90003`（替代`ISO 9000-3`）： 基于软件行业的特殊性制定的`ISO 9001`的 **_使用指南_**，不是`ISO 9001`注册/认证时的评估准则，是一个行动参考指南

>1. [What is The ISO 9000 Standards Series?](https://asq.org/quality-resources/iso-9000)
>1. [ISO 9002 & ISO 9003 are History](https://advisera.com/9001academy/knowledgebase/iso-9002-iso-9003-are-history/)

---

#### ISO9000质量管理体系The ISO 9000 family of QMS(Quality Management Systems)

1. `ISO 9000:2015`: Quality Management Systems - Fundamentals and Vocabulary (definitions)
1. `ISO 9001:2015`: Quality Management Systems - Requirements
1. `ISO 9004:2018`: Quality Management - Quality of an Organization - Guidance to Achieve Sustained Success (continuous improvement)
1. `ISO 19011:2018`: Guidelines for Auditing Management Systems

---

1. `ISO 9001:1987`: Model for quality assurance in design, development, production, installation, and servicing was for companies and organizations whose activities included the creation of new products.
1. `ISO 9002:1987`: Model for quality assurance in production, installation, and servicing had basically the same material as ISO 9001 but **_without covering the creation of new products_**.
1. `ISO 9003:1987`: Model for quality assurance in final inspection and test covered only the final inspection of finished product, **_with no concern for how the product was produced_**.

---

#### Industry-Specific Interpretations

1. `ISO/IEC 90003:2014`: provides guidelines for the application of ISO 9001 to computer software.
1. `TickIT`: The TickIT guidelines are an interpretation of ISO 9000 produced by the UK Board of Trade to suit the processes of the information technology industry, especially software development.

>[ISO 9000](https://en.wikipedia.org/wiki/ISO_9000#2015_version)

---

#### 现行GB/T 19000与ISO 9000对应（采用关系）

---

1. `GB/T 19000-2018` <==> `ISO 9000:2015`
1. `GB/T 19001-2016` <==> `ISO 9001:2015`
    1. `GB/T 19002-2018`：`GB/T 19001-2016`应用指南
    1. `GB/T 18305-2016`：汽车生产件及相关服务件组织应用`GB/T 19001-2016`特别要求
    1. `GB/T 27865-2011`：危险货物包装 包装、中型散装容器、大包装`GB/T 19001`的应用指南
    1. `GB/T 19003-2008`：软件工程`GB/T 19001-2000`应用于计算机软件的指南
    1. `GB/T 19080-2003`：食品与饮料行业`GB/T 19001-2000`应用指南

---

1. `GB/T 19003-2008` <==> `ISO/IEC 90003:2004`
1. `GB/T 19004-2011` <==> `ISO 9004-2009`
    - `GB/T 19004-2020` <==> `ISO 9004:2018`(2021/06/01起实施)
1. `GB/T 19011-2013` <==> `ISO 19011:2011`

---

#### TickIT项目TickIT Project

- 1980s末，软件开发过程的特殊性使软件企业在应用`ISO 9001`标准时陷入了困境
- 软件企业需要在通用认证过程的基础上补充附加的要求，导致了`TickIT`认证项目的产生
- `TickIT`项目帮助软件企业建立与其业务过程相关的质量体系，并使该体系满足`ISO 9001`的要求

---

- `TickIT`项目要求企业的第三方质量管理体系认证应由经认可的认证机构实施，而认证审核活动应通过使用那些在软件行业及其过程方面有直接经验的审核员完成
- `TickIT`项目由`TickIT`办公室进行管理，它是`BSI(Britain Standard Institute, 英国标准学会)`专门负责所有信息系统和通信标准化工作的部门
- `IRCA TickIT`审核员项目通过向第三方认证提供`TickIT`审核员及审核员培训课程来支持`TickIT`认证项目的实施。
- 审核员IT能力要求基本指南由英国计算机协会的`TickIT`委员会制定

---

![height:600](./.assets/image/Tick_IT.png)

---

#### `ISO 9001`的20条需求

---

1. 管理职责
1. 质量系统
1. 合同复审
1. 设计控制
1. 文档和数据控制
1. 对客户提供产品控制
1. 产品标识和可跟踪性
1. 过程控制
1. 审查和测试
1. 审查、度量和测试设备的控制

---

1. 审查和测试状态
1. 对不符合标准的产品的控制
1. 改正和预防行为
1. 处理、存储、包装、保存、交付
1. 质量记录的控制
1. 内部质量审计
1. 培训
1. 服务
1. 统计技术
1. 采购

---

#### `ISO 9000-3`核心内容

1. 合同评审
1. 需求规格说明
1. 开发计划
1. 质量计划
1. 设计和实现
1. 测试和确认
1. 验收
1. 复制、交付和安装
1. 维护

---
<!--3rd leading page-->
<!--
_backgroundColor:
_color:
_class:
    - lead
-->

### 🌟 `CMM`与`CMMI`

---

- `CMM`关注软件行业，当前的全称为`SW-CMM(Capability Maturity Model for Software, 软件能力成熟度模型)`
- `CMM(Capability Maturity Model, 能力成熟度模型)`由`CMU-SEI`受美国国防部委托研究，于1987年发布并开始实施评估
- `CMMI(Capacity Maturity Model Integrated, 能力成熟度集成模型)`是`CMM`扩展到其他行业的演进版，于1997年发布
- `CMM`的评估对象是"组织"并且未提供实现所需的具体知识和技能，因此，诞生了：
    - `TSP(Team Software Process, 团队软件过程)`
    - `PSP(Personal Software Process, 个体软件过程)`

---

- `CMM`是对软件组织在定义、实现、度量，控制和改善其软件过程的进程中各个发展阶段的描述
- 通过5个不断进化的层次来评定软件生产的历史与现状，即5等级、18过程域、52目标、300多关践实践
- 关系到软件项目成功与否的众多因素中，软件度量、工作量估计、项目规划、进展控制、需求变化、风险管理等，都是与工程管理直接相关的因素

---

![height:500](./.assets/image/cmm_levels_01.png)

---

![height:600](./.assets/image/cmm_levels_02.png)

---

|等级|特征|主要解决问题|关键域|结果|
|--|--|--|--|--|
|V优化级|软件过程的 **_量化反馈_** 和 **_新的思想和技术_** 促进过程的 **_不断改进_**|保持优化的机构|缺陷预防，过程变更和技术变更管理||
|IV已管理级|收集软件过程、产品质量的详细度量，对软件过程和产品质量有定量的理解和控制|技术变更、问题分析、问题预防|定量的软件过程管理、产品质量管理||

---

|等级|特征|主要解决问题|关键域|结果|
|--|--|--|--|--|
|III已定义级|已经将软件管理和过程文档化，标准化，同时综合成该组织的标准软件过程，所有的软件开发都使用该标准软件过程|过程度量、过程分析量化质量计划|组织过程定义，组织过程焦点，培训大纲，软件集成管理，软件产品工程，组织协调，专家评审|生产率和质量|

---

|等级|特征|主要解决问题|关键域|结果|
|--|--|--|--|--|
|II可重复级|建立了基本的项目管理来跟踪进度，费用和功能特征，制定了必要的项目管理，能够利用以前类似项目应用取得成功|培训、测试、技术常规和评审过程关注、标准和过程|需求管理，项目计划，项目跟踪和监控，软件子合同管理，软件配置管理，软件质量保证|风险|

---

|等级|特征|主要解决问题|关键域|结果|
|--|--|--|--|--|
|I初始级|软件过程混乱无序，对过程几乎没有定义，成功依靠个人的才能和经验，反应式管理方式|项目管理、项目策划、配置管理软件质量保证||

---

![height:600](./.assets/image/cmm_levels_03.png)

---

#### `KPA(Key Process Area, 关键过程域/关键域)`

---

![height:600](./.assets/image/cmm_contents.png)

---

#### `CMM1初始级Initial`

+ process is unpredictable, poorly controlled and reactive
+ 初始级处于`CMM`的最低级
+ 基本上没有健全的软件工程管理制度
+ 每件事情都以特殊的方法来做

---

#### `CMM2可重复级Repeatable`

+ processes are characterized for specific projects and organization is often reactive
+ 有基本管理行为和设计，管理技术基于 **_相似项目_** 的 **_经验_**
+ 6个`KPA`
    1. ⭐ `RM(Requirements Management, 需求管理)`
    1. `SPP(Software Project Planning, 软件项目计划)`
    1. `SPTO(Software Project Tracking and Oversight, 软件项目跟踪与监控)`
    1. `SSM(Software Subcontract Management, 软件子合同管理)`
    1. ⭐ `SQA(Software Quality Assurance, 软件质量保证)`
    1. ⭐ `SCM(Software Configuration Management, 软件配置管理)`

---

#### `CMM3已定义级Defined`

+ projects tailor their processes from the organization's development methodology
+ 7个`KPA`
    1. `OPF(Organization Process Focus, 组织过程焦点)`
    1. `OPD(Organization Process Definition, 组织过程定义)`
    1. `TP(Training Program, 培训程序)`
    1. `ISM(Integrated Software Management, 集成软件管理)`
    1. `SPE(Software Product Engineering, 软件产品工程)`
    1. `IC(Intergroup Coordination, 组间协调)`
    1. ⭐ `PR(Peer Reviews, 同行评审)`

---

#### `CMM4已管理级Managed`

+ processes are measured and controlled
+ 2个`KPA`
    1. ⭐ `QPM(Quantitative Process Management, 定量过程管理)`
    1. ⭐ `SQM(Software Quality Management, 软件质量管理)`

---

#### `CMM5优化级Optimizing`

+ focus on process improvements
+ 3个`KPA`
    1. `DP(Defect Prevention, 缺陷预防)`
    1. `TCM(Technology Change Management, 技术变更管理)`
    1. `PCM(Process Change Management, 过程变更管理)`

---

#### `PSP(Personal Software Process, 个体软件过程)`与`TSP(Team Software Process, 团队软件过程)`

---

##### `PSP`

+ `PSP`是一种可用于控制、管理和改进个人工作方式的自我持续改进过程，是一个包括软件开发表格、指南和规程的结构化框架
+ `PSP`与具体的技术（程序设计语言、工具或者设计方法）相对独立，其原则能够应用到几乎任何的软件工程任务之中
+ `PSP`能够：
    1. 说明个体软件过程的原则
    1. 帮助软件工程师做出准确的计划
    1. 确定软件工程师为改善产品质量要采取的步骤
    1. 建立度量个体软件过程改善的基准
    1. 确定过程的改变对软件工程师能力的影响

---

![height:600](./.assets/image/PSP.png)

---

##### `TSP`

+ 实践证明，仅有`PSP`还是不够，因此，`CMM/SEI`又在此基础上发展出了`TSP`方法
+ `TSP`指导项目组
    1. 成员如何有效地规划和管理所面临的项目开发任务
    1. 管理人员如何指导软件开发队伍
+ `TSP`实施集体管理与自己管理自己相结合的原则，最终目的在于指导开发人员如何在最少的时间内，以预计的费用生产出高质量的软件产品。所采用的方法是对群组开发过程定义、度量和改进。

---

##### 实施`TSP`的3个先决条件

1. 高层主管和各级经理的支持，以取得必要的资源
1. 项目组开发人员已经过`PSP`的培训并有按`TSP`工作的愿望和热情
1. 整个开发组织在总体上应至少处于`CMM2`或以上，开发小组的规模以3～20人为宜

---

![height:600](./.assets/image/TSP.png)

---

#### `CMMI(Capacity Maturity Model Integrated, 能力成熟度集成模型)`

>1. <https://en.wikipedia.org/wiki/Capability_Maturity_Model_Integration>
>1. <https://dqsindia.com/cmmi/getting-started/>

---

+ `CMMI`是`CMM`的最新版本
+ `CMM`是一种单一的模型，一般应用于软件工程
+ `CMM`应用于软件工程外领域存在的问题
    1. 不能集中其不同过程改进的能力以取得更大成绩
    1. 要进行一些重复的培训、评估和改进活动，因而增加了许多成本
    1. 遇到不同模型中有一些对相同事物说法不一致，或活动不协调，甚至相抵触
+ 随着`CMM`的推广与模型本身的发展，该模型演绎成为一种被广泛应用的综合性模型，因此改名为`CMMI`

---

+ Originally `CMMI` addresses three areas of interest:
    1. Product and service development - CMMI for Development (`CMMI-DEV`)
    1. Service establishment, management, - CMMI for Services (`CMMI-SVC`)
    1. Product and service acquisition - CMMI for Acquisition (`CMMI-ACQ`)
+ In version 2.0 these three areas (that previously had a separate model each) were merged into a single model

---

![height:600](./.assets/image/CMMI_01.png)

---

![height:400](./.assets/image/CMMI_02.png)

---

![height:600](./.assets/image/quality_umbrella_CMMI+TSP+PSP.jpg)

---

#### 🌟 `CMM`/`CMMI`中的质量保证框架

+ `SQA`是`CMM 2`中6个`KPA`之一，在`CMMI`中该`KPA`升级为管理级中的`PPQA(Process and Product Quality Assurance, 过程与产品质量保证过程)`
+ `SQA`包括评审和审计软件产品和活动，以验证它们是否符合适用的规程和标准，还包括向软件项目和其他有关的管理者提供评审和审计的结果
+ `CMM`/`CMMI`的思想，是一切从顾客需求出发，从整个组织层面上实施过程质量管理，正符合了`TQM（Total Quality Management, 全面质量管理）`的基本原则

---

![height:600](./.assets/image/TQM.png)

---

+ `CMM`/`CMMI`为满足`SQA`这个关键过程域的要求需要达到以下4个目标：
    1. 软件质量保证活动是有计划的
    1. 软件产品和活动与适用的标准、规程和需求的符合性要得到客观验证
    1. 相关的小组和个人要被告知软件质量保证的活动和结果
    1. 高级管理者处理在软件项目内部不能解决的不符合问题

---

#### `CMM`/`CMMI`的`SQA`的具体实施方法

1. 定义项目类型和生命周期
1. 建立`SQA`计划，确定项目审计内容
1. 生成`SQA`报告
1. 审计`SQA`报告
1. 独立汇报

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 软件缺陷模型
<!--TODO: update it...-->

---

### `bug` VS `defect` VS `fault` VS `failure`

1. static concept
    1. `bug臭虫`：指 **_存在于_** 软件（起初专指程序）中的不希望或不可接受的 **_错误_**
    1. `defect缺陷`：指 **_存在于_** 软件中的不希望或不可接受的 **_偏差_**
1. dynamic concept
    1. `fault故障`：指软件 **_运行时_** 出现的不希望或不可接受的  **_内部状态_**
    1. `failure失效`：指软件 **_运行时_** 出现的不希望或不可接受的 **_外部表现_**

---

1. `bug/defect` --> `fault` --> `failure`
1. `bug`和`defect`目前一般视为同义词，`bug`偏口语，`defect`偏书面语

---

### 软件缺陷的特征Characters of Software Defect

1. 🌟 `has cause有原因`： 缺陷均有原因
1. 🌟 `reproducible可重现性`： 无法重现的缺陷则无法被定位，无法被定位则无法被修复
1. 🌟 `introduce new defects引入新缺陷`： 修复一个缺陷可能会引入新的缺陷
1. `cumulative累积性/magnification放大性`： 缺陷发现得越晚、处理得越晚，付出的代价越大

---

### 软件缺陷的属性Attributes of Software Defect

1. 🌟 `identifier标识`：唯一标识一个缺陷的标识符（建议使用工具进行管理）
1. ⭐ `title标题`：缺陷的一句话描述
1. `type类型`：根据缺陷自然属性划分的缺陷类型
1. 🌟 `severity严重级`：因缺陷引起的故障的影响程度
1. 🌟 `priority优先级`：缺陷被修复的优先程度
1. 🌟 `status状态`：缺陷当前被跟踪的所处状态

---

1. `origin起源/由来`：首次发现缺陷时所处的阶段
1. `source来源/源头`：产生缺陷的阶段
1. ⭐ `root cause根源/原因`：缺陷产生的根本原因（修复阶段填写❓）

👉 严重级、优先级的级别数不宜过多，一般3至5个级别 👈

---

### 软件缺陷的主体Body of Software Defect

1. 简要描述
1. 软件及其版本
1. 运行上下文/运行环境
1. 重现步骤
1. 期望结果
1. 实际结果
1. 其他信息

---

#### 软件缺陷的类型

1. 缺陷类型没有公认的、统一的标准
1. 常见的缺陷类型：功能缺陷、用户界面缺陷、文档缺陷、软件配置缺陷、性能缺陷、接口缺陷等

---

#### 软件缺陷的严重级

1. Blocker: Blocks development and/or testing work
1. Critical: crashes, loss of data, severe memory leak
1. Major: major loss of function
1. Minor: minor loss of function, or other problem where easy workaround is present
1. Trivial: cosmetic problem like misspelled words or misaligned text
1. Enhancement: Request for enhancement

---

#### 软件缺陷的严重级的判断

1. 严重级（Severity）：软件缺陷对软件质量的破坏程度，反应其对产品和用户的影响，即此软件缺陷的存在将对软件的功能和性能产生怎样的影响
1. 软件缺陷的严重性的判断 **应从软件最终用户的观点** 做出判断，即判断缺陷的严重性要为用户考虑，考虑缺陷对用户使用造成的恶劣后果的严重性

👉 避免"伪严重" 👈

---

#### 软件缺陷的修复优先级

1. Resolve Immediately： 缺陷必须被立即解决
1. Normal Queue： 缺陷需要正常排队等待修复
1. Not Urgent： 缺陷可以在方便时进行修复

---

#### 软件缺陷的修复优先级的判断

1. 优先级表示修复缺陷的重要程度和应该何时修复，是表示处理和修正软件缺陷的先后顺序的指标，即哪些缺陷需要优先修正，哪些缺陷可以稍后修正
1. 确定软件缺陷优先级，更多的是站在软件开发工程师的角度考虑问题，因为缺陷的修正顺序是个复杂的过程，有些不是纯粹的技术问题，而且开发人员更熟悉软件代码，能够比测试工程师更清楚修正缺陷的难度和风险（❗ 该观点有争议 ❗）

---

>在提交Bug时，一般只定义bug的severity，而将priority交给project leader 或team leader来定义，由他们来决定该bug被修复的优先等级。某种意义上来说，priority的定义要依赖于severity，在大多数情况下，severity越严重，那这个bug的priority就越高。
>
>[Bug的严重程度、优先级如何定义](https://cloud.tencent.com/developer/article/1733135)

---

#### 软件缺陷的状态

1. Submitted： 已提交的缺陷（已提交未排序）
1. Open： 确认"提交的缺陷"，等待处理
1. Rejected： 拒绝"提交的缺陷"，不需要修复或不是缺陷
1. Resolved： 缺陷被修复
1. Closed： 确认被修复的缺陷，将其关闭（确认完成修复）

---

#### 软件缺陷的起源/由来

1. Requirement： 在需求阶段发现的缺陷
1. Architecture： 在构架阶段发现的缺陷
1. Design： 在设计阶段发现的缺陷
1. Code： 在编码阶段发现的缺陷
1. Test： 在测试阶段发现的缺陷

---

#### 软件缺陷的来源/源头

1. Requirement：由于需求的问题引起的缺陷
1. Architecture：由于构架的问题引起的缺陷
1. Design：由于设计的问题引起的缺陷
1. Code：由于编码的问题引起的缺陷
1. Test：由于测试的问题引起的缺陷（测试会引起缺陷吗❓）
1. Integration：由于集成的问题引起的缺陷

---

#### 缺陷的根源/原因

1. 目标：如，错误的范围，误解了目标，超越能力的目标等（应该是"需求"❓）
1. 过程，工具和方法：如，无效的需求收集过程，过时的风险管理过程，不适用的项目管理方法，没有估算规程，无效的变更控制过程等
1. 人： 如，项目团队职责交叉，缺乏培训。没有经验的项目团队，缺乏士气和动机不纯等
1. 缺乏组织和通讯： 如，缺乏用户参与，职责不明确，管理失败等

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `IEEE`软件工程标准

---

- `IEEE`软件工程标准
    1. 由`TCSE(Technical Committee on Software Engineering, 软件工程技术委员会)`的`SESS(Software Engineering Standards Subcommittee, 软件工程标准工作小组)`创建
    1. 围绕在顾客标准、资源与技术标准、流程标准、产品标准4个对象上
    1. 每个标准分为需求分析、建议惯例、指南

---

1. `IEEE 24765`
1. `IEEE 12207 SLCP(Software Life Cycle Processes)`

---

1. `IEEE 730: SQA(Software Quality Assurance)`
1. `IEEE 828: SCM(Software Configuration Management)`
1. `IEEE 29119: STD(Software Test Documentation)`
1. `IEEE 29148: SRS(Software Requirements Specification)`
1. `IEEE 1012: V&V(Software Verification and Validation)`
1. `IEEE 1016: SDD(Software Design Description)`
1. `IEEE 16326: SPM(Software Project Management)`
1. `IEEE 24748: SUD(Software User Documentation)`
1. `IEEE 1028 SRA(Software Reviews and Audits`

---

### `IEEE 730 Software Quality Assurance Plan`

><https://en.wikipedia.org/wiki/Software_quality_assurance>

<!--
FIXME: 完善730的内容
-->

---

### `IEEE 12207 Software Life Cycle Processes`

---

#### 版本历史

1. `IEEE/ISO/IEC 12207:1995`: the first iteration, published in July 1995
    1. `IEEE/ISO/IEC 12207:1995/Amd 1`:2002, an amended version of the prior, published in May 2002
    1. `IEEE/ISO/IEC 12207:1995/Amd 2`:2004, an amended version of the prior, published in November 2004
1. `IEEE Std. 12207-2008`: published in February 2008
1. `IEEE/ISO/IEC 12207-2017`: published in November 2017

---

#### 各版本分类

- `IEEE/ISO/IEC 12207:1995`定义了17个过程，分别属于3类：
    1. `Primary Process主要过程`
    1. `Supporting Process支持过程`
    1. `Organizational Process组织过程`
- `IEEE/ISO/IEC 12207:1995/Amd 1`定义了22个过程
- `IEEE/ISO/IEC 12207:2008`定义了4类

---

#### `IEEE/ISO/IEC 12207:1995`

---

![height:600](./.assets/image/IEEE12207_SDLC.png)

---

#### `IEEE/ISO/IEC 12207:2008`

1. 协议过程群

><http://www.ccidcom.com/xingyebaogao/20080928/B3efAuOIIM5qsys4.html>

---

>ISO/IEC/IEEE 12207:2017 divides software life cycle processes into four main process groups: agreement, organizational project-enabling, technical management, and technical processes.
>
>>[Wikipedia.ISO/IEC 12207](https://en.wikipedia.org/wiki/ISO/IEC_12207#Processes)

---

### `IEEE 1012 Software Verification and Validation`

- `verification验证`： 用来评价某一系统或某一组件的过程，来判断给定阶段的产品是否满足该阶段开始时施加的条件
    1. 验证活动在一定的程度上是一种普通的测试活动
- `validation确认`： 开发过程中间或结束时对某一系统或某一组件进行评价的过程，以确认它是否满足规定的需求

<!--TODO: add content of IEEE 1012-->

---

### `IEEE 1028 Software Reviews and Audits`

<!--TODO: add content of IEEE 1012-->

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 其他标准

---

### `ISO/IEC 15504 Information technology - Process assessments`

1. `ISO/IEC 15504-2:2003 Part 2: Performing an assessment`
1. `ISO/IEC 15504-3:2004 Part 3: Guidance on performing an assessment`
1. `ISO/IEC 15504-4:2004 Part 4: Guidance on use for process improvement and process capability determination`

---

>ISO/IEC 15504 was initially derived from process lifecycle standard ISO/IEC 12207 and from maturity models like Bootstrap, Trillium and the Capability Maturity Model (CMM).
>
>ISO/IEC 15504 **_has been superseded by: ISO/IEC 33001:2015 Information technology - Process assessment - Concepts and terminology_** as of March, 2015 and is no longer available at ISO.  
>>[Wikipedia.ISO/IEC 15504](https://en.wikipedia.org/wiki/ISO/IEC_15504)

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
