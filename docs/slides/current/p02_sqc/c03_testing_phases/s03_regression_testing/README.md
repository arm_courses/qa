---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Regression Testing_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# 回归测试Regression Testing

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [回归测试的测试策略](#回归测试的测试策略)
2. [回归测试的测试过程](#回归测试的测试过程)
3. [回归测试的测试技术](#回归测试的测试技术)
4. [回归测试的测试人员](#回归测试的测试人员)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 回归测试的定义和目的

---

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 回归测试的测试策略

---

### 测试用例的策略

1. 测试用例库的维护
1. 回归测试包的选择
    1. 重新测试全部测试用例
    1. 基于风险选择测试用例
    1. 基于操作剖面选择测试用例
    1. 基于修改部分选择测试用例

---

### 测试过程的策略

1. 识别软件中修改的部分
1. 建立新的测试用例库：去掉不再适用的
1. 依据策略选择测试用例进行测试
1. 如有必要
    1. 增加新的测试用例至测试用例库
    1. 依据策略选择测试用例进行测试

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 回归测试的测试技术

---

1. 对应测试阶段对应的测试技术
1. 错误预测

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 回归测试的测试人员

---

对应测试阶段对应的人员

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
