---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: ""
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 软件质量保证与测试

---

# 课程基本信息Course Basic Information

1. 课程名称： 软件质量保证与测试
1. 课程类型： 专业必修
1. 课程学分： 3
1. 课程学时： 54
1. 考核方式： 考试

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
