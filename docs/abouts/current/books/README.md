# Books

## 👑 软件测试的艺术The Art of Software Testing

1. edition:     3rd
1. authors:     Glenford J. Myers, Corey Sandler, Tom Badgett
1. publisher:   John Wiley & Sons
1. date:        Sep 23, 2011

>1. [Wiki. Glenford Myers.](https://en.wikipedia.org/wiki/Glenford_Myers)

## 👑 Google软件测试之道How Google Tests Software

1. edition:    1st
1. authors:    James A. Whittaker, Jason Arbon, Jeff Carollo
1. publisher:  Addison-Wesley Professional
1. date:       2012

## 👍 敏捷测试高效实践：测试架构师成长记
