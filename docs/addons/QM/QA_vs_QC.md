# QA vs QC

质量管理工作通常可分为质量计划（Quality Plan，`QP`）、质量保证（Quality Assurance, `QA`）、质量控制（Quality Control，`QC`）三个阶段。

在一些中、小型制造企业，员工往往身兼多职，同时做了QA和QC的工作，因此部分品管人员不能完全分清QA和QC的各自职责范围。

## 小结

+ 联系：
    1. `QA`和`QC`都是为了提升质量
    1. `QA`和`QC`都属于`QM`范畴
    1. `QC`这一过程属于`QC`关注的过程之一，实现`QA`的主要技术手段是`QC`中技术（如：`review`、`test`等）
+ 区别：
    1. `QA`关注生产产品的过程（向管理层反馈是否按照过程规范执行过程活动，向管理层、客户提供信心），`QC`关注产品本身（向管理层反馈产品质量是否合格，向客户提供合格的产品）
    1. `QA`主要属于工程领域，`QC`主要属于技术领域
    1. `QA`确保产品的合格率，`QC`判断具体产品是否合格

## 参考文献Bibliographies

1. [QA和QC有什么区别？](https://zhuanlan.zhihu.com/p/133702740)
