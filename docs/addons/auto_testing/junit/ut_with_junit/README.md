---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "Unit Testing with JUnit"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Unit Testing with JUnit

>1. [廖雪峰.java教程-单元测试.](https://www.liaoxuefeng.com/wiki/1252599548343744/1255945269146912)
>1. [XUnit Test Patterns](http://xunitpatterns.com/index.html)

---

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Unit Testing Automation](#unit-testing-automation)
2. [JUnit](#junit)

<!-- /code_chunk_output -->

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Unit Testing Automation

---

### Why Unit Testing Must be Automated

1. 质量不是被测试出来的，但未经测试也不可能产生高质量 ==> 单元测试必须由开发人员完成 ==> 测试需要时间，需要花费精力 ==> 在进度和质量之间确定合适的平衡点、提高效率 ==> 单元测试测试用例要被反复执行 ==> 自动化提升整体的效率
1. 测试本身也可能有缺陷 ==> 需要书面记录并迭代更新
1. 软件由多人协作完成 ==> 需要书面记录

---

### 良好的测试脚本的特点

1. 强适应：一次编写、重复测试
1. 易迭代：一次编写、迭代适应
1. 易学习：自说明

<!--
1. 动态测试：
    1. 开发测试脚本 ==> 一次编写多次运行
    1. 目标： 代码提交前执行测试检查
1. 静态测试：
    1. 开发代码扫描工具 ==> 一次配置多次运行
    1. 目标： 规范开发行为和结果
-->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## JUnit

---

1. initial release in 2002
1. `JUnit`作为一个工具
    1. `JUnit 3`: ???
        1. 基于约束（继承约束、命名约束）
    1. `JUnit 4`: Feb 16, 2006?
        1. 基于`Annotation`
        1. `JUnit4.4`开始引入`hamcrest`依赖
1. `JUnit`作为一个平台
    1. `JUnit 5`: Sep 10, 2017

---

### Fathers of JUnit — Kent Beck 1961, Also Be Father of XP and TDD

![height:500](./assets_image/Kent_Beck_no_Workshop_Mapping_XP.jpg)

---

### Fathers of JUnit — Erich Gamma 1961, Also Be Father of Design Patterns

![height:500](./assets_image/erich_gamma.jpg)

---

### `JUnit 3` vs `JUnit 4`

>1. [Junit 3 vs Junit 4 Comparison](http://asjava.com/junit/junit-3-vs-junit-4-comparison/)

---

#### `JUnit 3`

```java {.line-numbers}
import junit.framework.Assert;
import junit.framework.TestCase;

public class TournamentTest extends TestCase{
  Tournament tournament;

  public void setUp() throws Exception {
    System.out.println("Setting up ...");
    tournament = new Tournament(100, 60);
  }

  public void tearDown() throws Exception {
    System.out.println("Tearing down ...");
    tournament = null;
  }

  public void testGetBestTeam() {
    Assert.assertNotNull(tournament);
    Team team = tournament.getBestTeam();
    Assert.assertNotNull(team);
    Assert.assertEquals(team.getName(), "Test1");
  }
}
```

---

#### `JUnit 4`

```java {.line-numbers}
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TournamentTest {
  Tournament tournament;

  @Before
  public void init() throws Exception {
    System.out.println("Setting up ...");
    tournament = new Tournament(100, 60);
  }

  @After
  public void destroy() throws Exception {
    System.out.println("Tearing down ...");
    tournament = null;
  }

  @Test
  public void testGetBestTeam() {
    Assert.assertNotNull(tournament);
    Team team = tournament.getBestTeam();
    Assert.assertNotNull(team);
    Assert.assertEquals(team.getName(), "Test1");
  }
}
```

---

### Junit4 VS JUnit5

1. `JUnit Platform`: It defines the TestEngine API for developing new testing frameworks that run on the platform.
1. `JUnit Jupiter`: It has all new JUnit annotations and TestEngine implementation to run tests written with these annotations.
1. `JUnit Vintage`: To support running JUnit 3 and JUnit 4 written tests on the JUnit 5 platform.

>1. [JUnit 5 vs JUnit 4](https://howtodoinjava.com/junit5/junit-5-vs-junit-4/)
>1. [A Guide to JUnit 5 Extensions](https://www.baeldung.com/junit-5-extensions)
>1. [List Of JUnit Annotations: JUnit 4 Vs JUnit 5.](https://www.softwaretestinghelp.com/junit-annotations-tutorial/)

---

```java {.line-numbers}
//JUnit 4
@Test
@Before, @After
@BeforeClass, @AfterClass

//JUnit 5
@Test
@BeforeEach, @AfterEach
@BeforeAll, @AfterALL
```

<!--
❗ 每运行一个`@Test`方法时，`JUnit`均会实例化一个新的该测试类的对象 ❗
-->
>[List Of JUnit Annotations: JUnit 4 Vs JUnit 5.](https://www.softwaretestinghelp.com/junit-annotations-tutorial/)

---

```java {.line-numbers}
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ ExceptionTest.class, TimeoutTest.class })
public class JUnit4Example
{
}
```

---

```java {.line-numbers}
import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

@Suite
@SelectClasses({ ExceptionTest.class, TimeoutTest.class })
@SelectPackages("com.howtodoinjava.junit5.examples")
public class JUnit5Example
{
}
```

---

### Cores of JUnit

1. `TestCase`：创建和执行测试用例
1. `Assert`：自动校验实际值和期望值
1. `Runner`：组织和执行测试
1. `TestResult`：测试报告

---

### Main Annotations of JUnit

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
