# Documentation

```bash {.line-numbers cmd=true output=text hide=false run_on_save=true}
tree -I *_code_chunk -I *.html -I *.pdf -d -L 1 .
```

```bash {.line-numbers}
.
├── abouts
├── addons
├── exercises
├── experiments
├── glossaries
├── lectures
└── slides
```
