# 软件测试领域术语

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [基本术语](#基本术语)
    1. [软件质量控制（`Software Quality Control`, `SQC`）/软件测试（`Software Testing`, `ST`）](#软件质量控制software-quality-control-sqc软件测试software-testing-st)
    2. [测试用例（`Test Case`, `TC`）](#测试用例test-case-tc)
2. [驱动模块（`Driver`）](#驱动模块driver)
3. [桩模块（`Stub`）](#桩模块stub)
4. [变异测试（`Mutation Testing`）](#变异测试mutation-testing)
5. [测试类型-测试用例设计方法](#测试类型-测试用例设计方法)
    1. [白盒测试（`White-Box Testing`）](#白盒测试white-box-testing)
    2. [黑盒测试（`Black-Box Testing`）](#黑盒测试black-box-testing)
    3. [灰盒测试（`Grey-Box Testing`）](#灰盒测试grey-box-testing)
    4. [基本路径测试法（`Basis Path Testing`）](#基本路径测试法basis-path-testing)
6. [测试类型-测试阶段](#测试类型-测试阶段)
    1. [单元测试`Unit Testing`](#单元测试unit-testing)
    2. [集成测试`Integration Testing`](#集成测试integration-testing)
    3. [确认测试``](#确认测试)
    4. [系统测试`System Testing`](#系统测试system-testing)
    5. [验收测试（`Acceptance Testing`）](#验收测试acceptance-testing)
    6. [回归测试（`Regression Testing`）](#回归测试regression-testing)
7. [测试类型-其他](#测试类型-其他)
    1. [静态测试（`Static Testing`）](#静态测试static-testing)
    2. [动态测试（`Dynamic Testing`）](#动态测试dynamic-testing)
    3. [人工测试（`Manual Testing`, `MT`）](#人工测试manual-testing-mt)
    4. [自动化测试（`Automation Testing`, `AT`）](#自动化测试automation-testing-at)
8. [具体测试](#具体测试)
    1. [功能测试`Function Test`](#功能测试function-test)
    2. [性能测试`Performance Test`](#性能测试performance-test)
    3. [负载测试`Load Test`/容量测试](#负载测试load-test容量测试)
    4. [压力测试`Stress Test`](#压力测试stress-test)
    5. [冒烟测试/预测试`Smoke Test`](#冒烟测试预测试smoke-test)
    6. [配置测试`Configuration Test`](#配置测试configuration-test)
    7. [安全测试`Security Test`](#安全测试security-test)
    8. [保护测试`Protection Test`](#保护测试protection-test)
    9. [界面测试`User Interface Test`](#界面测试user-interface-test)
    10. [可用性测试`Usability Test`](#可用性测试usability-test)
    11. [可安装性测试`Installation Test`](#可安装性测试installation-test)
    12. [备份测试`Backup Test`](#备份测试backup-test)
    13. [健壮性测试`Robustness Test`](#健壮性测试robustness-test)
    14. [文档测试`Documentation Test`](#文档测试documentation-test)
    15. [在线帮助测试`Online Help Test`](#在线帮助测试online-help-test)
9. [研发环境Environment](#研发环境environment)
    1. [开发环境Development Environment（`dev`）](#开发环境development-environmentdev)
    2. [测试环境Test Environment（`test`）](#测试环境test-environmenttest)
        1. [系统集成测试环境System Integration Test Environment（`sit`）](#系统集成测试环境system-integration-test-environmentsit)
        2. [特性验收测试环境Feature Acceptance Test Environment（`fat`）](#特性验收测试环境feature-acceptance-test-environmentfat)
        3. [用户验收测试环境User Acceptance Test Environment（`uat`）](#用户验收测试环境user-acceptance-test-environmentuat)
    3. [生产环境Production Environment（`pro`）](#生产环境production-environmentpro)
        1. [预发布环境Pre-Production Environment（`pre`）](#预发布环境pre-production-environmentpre)
        2. [灰度发布环境Gray-Scale Environment](#灰度发布环境gray-scale-environment)

<!-- /code_chunk_output -->

## 基本术语

### 软件质量控制（`Software Quality Control`, `SQC`）/软件测试（`Software Testing`, `ST`）

在软件行业，软件质量控制通常指的就是软件测试。

软件测试是使用人工或自动的手段来运行或测定被测软件（或其组成部分）的过程，其结果是得到预期结果与实际结果之间的差距。软件测试是发现软件缺陷、提高软件可信度、提高软件质量的一种重要手段。

参考定义：

>Software quality control is a function that checks whether a software component, or supporting artifact meets requirements, or is "fit for use". Software Quality Control is commonly referred to as Testing
>
>>[**_`Wikipeida - Software Quality Control`_**.](https://en.wikipedia.org/wiki/Software_quality_control)

参考定义：

>软件测试是一个过程或一系列过程，用来确认计算机代码完成了其应该完成的功能，不执行其不该有的操作。
>
>>《软件测试的艺术》（第3版）

### 测试用例（`Test Case`, `TC`）

测试用例也称为：测试案例。

测试用例指为具体的软件测试而设计的关于一个测试任务的描述，该描述的4个核心元素包括执行条件（前置条件）、测试步骤、输入数据（测试数据）和预期结果，该描述的核心目的是核实是否满足某个特定的需求。

参考定义：

>In software engineering, a test case is a specification of the inputs, execution conditions, testing procedure, and expected results that define a single test to be executed to achieve a particular software testing objective, such as to exercise a particular program path or to verify compliance with a specific requirement.
>
>测试用例是为具体软件测试目标而定义的关于一个测试的规格说明书，该规格说明书包括输入、执行条件、测试流程和预期结果。
>
>>**_`ISO/IEC/IEEE 24765:2010(E)`_**. 368-368.

参考定义：

>- a） 为具体的目标（例如，为练习具体的程序路径或验证对特定需求的遵循性）而开发的一组测试输入、执行条件和预料的结果；  
>- b） 对于测试项，规定输入、预料的结果和一组执行条件的文档。
>
>>**_`GB/T 11457－2006`_**. 140-140.

### 驱动模块（`Driver`）

驱动模块指用以模拟待测模块的上级模块，驱动模块在集成测试中接受测试数据，把相关的数据传送给待测模块，启动待测模块，并输出相应的结果。

### 桩模块（`Stub`）

桩模块指用以模拟待测模块工作过程中所调用的模块。桩模块由待测模块调用，它们一般只进行很少的数据处理，例如打印入口和返回，以便于检验待测模块与其下级模块的接口。

参考定义：

>桩是指一种特殊的软件模块，模拟某个特定模块的功能与接口，用于测试调用该特定模块或依赖于该特定模块的其他模块，在测试过程中用来代替该模块的执行。
>
>>**_`C-SWEBOK-2018`_**.61-61.

### 变异测试（`Mutation Testing`）

变异测试是指对特定版本的程序进行有目的地变更，用以评估测试用例本身的质量。

参考定义：

>变异是指对特定版本的程序进行有目的地变更，用以评估测试用例检测变更的能力
>
>>**_`C-SWEBOK-2018`_**.61-61.

## 测试类型-测试用例设计方法

### 白盒测试（`White-Box Testing`）

白盒测试也称为： 玻璃盒`Glass-Box Testing`、透明盒测试`Clear Box Testing`、逻辑驱动测试`Logisc-Driven Testing`、结构测试`structural testing`、基于代码的测试`Code-Based Testing`。

白盒测试关注测试对源码覆盖程度。

🔭 参考定义：

>如果测试用例的生成依赖于软件的设计与编码等信息，则将此类测试技术归类为白盒测试。
>
>>**_`C-SWEBOK-2018`_**. 67-67.

### 黑盒测试（`Black-Box Testing`）

黑盒测试也称为：数据驱动测试`Data Driven Testing`、输入输出驱动的测试`Input Output Driven Testing`、基于规格说明书的测试`Specification Based Testing`（❗ 这里的规格说明书不仅指`SRS`，还包括`SDD`等经评审形成基线的文档 ❗）。

黑盒测试：不依赖于软件的设计、代码等软件内部信息（实现信息），仅依赖于软件的输入/输出、从用户的角度设计测试用例的方法。

>如果测试用例的生成仅仅依赖于软件的输入/输出，则将此类测试技术归类为黑盒测试。
>
>>**_`C-SWEBOK-2018`_**. 67-67.

### 灰盒测试（`Grey-Box Testing`）

⚠️ 灰盒测试在逻辑上存在一定的矛盾（既关注内部结构，又不那么关注内部结构），现在一般将接口测试定义为灰盒测试，但仍存在有分歧，因为接口测试一般参照《接口文档》进行测试用例设计 ⚠️

### 基本路径测试法（`Basis Path Testing`）

基本路径测试法也称为：独立路径测试法`Independent Path Testing`、结构化测试法`Structured Testing`。

基本路径测试法指通过独立路径集设计测试用例从而达到等价于对整个路径空间进行测试的效果的一种白盒测试用例设计方法。

## 测试类型-测试阶段

### 单元测试`Unit Testing`

单元测试也称为：模块测试`Module Testing`

- 定义：在模块或高内聚形成的大粒度模块的内部范围内，以软件中的最小可测试单元或基本组成单元为基本测试单元进行测试的测试阶段
- 目的：验证各模块内是否正确实现了详细设计中的模块功能、性能、接口和设计约束等要求，发现各模块内部可能存在的各种缺陷
- 特点：
    - 需要从程序的内部结构出发设计测试用例
    - 多个模块可以平行地独立进行单元测试
    - 单元测试应由开发人员编写，供开发人员使用

参考定义：

>单元测试用于测试单一软件模块的功能。针对不同的测试上下文，被测模块可以是单一的子程序，也可以是由多个小模块高内聚而形成大粒度模块。  
>>**_`C-SWEBOK-2018`_**.64-64.

>A unit test is a test written by the programmer to verify that a relatively small piece of code is doing what it is intended to do. They are narrow in scope, they should be easy to write and execute, and their effectiveness depends on what the programmer considers to be useful. The tests are intended for the use of the programmer, they are not directly useful to anybody else, though, if they do their job, testers and users downstream should benefit from seeing less bugs.

### 集成测试`Integration Testing`

集成测试也称为： 组装测试、联合测试或联调测试。

- 定义：将已通过单元测试的模块按照一定的策略循环地进行组装然后测试（最终组装成子系统或系统进行测试）的测试阶段
- 目的：验证模块组装后是否正确实现了概要设计中的功能、性能、接口和设计约束等要求，发现模块间可能存在的各种缺陷
- 特点：

参考定义：

>集成测试用于验证模块之间的交互关系。
>
>>**_`C-SWEBOK-2018`_**.64-64.

### 确认测试``

确认测试也称为：有效性测试。

- 定义：在模拟的环境下，验证软件的所有功能和性能及其他特性是否与用户的预期要求一致。通过了确认测试之后的软件，才具备了进入系统测试阶段的资质
- 目的：
- 特点：

### 系统测试`System Testing`

- 定义：在真实的环境下（与生产环境同构），验证完整的程序系统能否和外部系统（包括硬件、外设、网络和系统软件、支持平台等）正确配置、连接和运行，并满足用户的所有需求

参考定义：

>系统测试是将需测试的软件，作为整个基于计算机系统的一个元素，与计算机硬件、外设、某些支持软件、数据和人员等其他系统元素及环境结合在一起测试。
>
>>[**_`中文维基百科 - 系统测试`_**](https://zh.wikipedia.org/wiki/系统测试)

参考定义：

>系统测试关注整个系统的行为。
>
>>**_`C-SWEBOK-2018`_**.64-64.

### 验收测试（`Acceptance Testing`）

验收测试也称为：交付测试。

- 定义：按照项目任务书或合同、供需双方约定的验收依据文档进行的对整个系统的测试与评审，决定是否接收或拒收系统
- 目的：

参考定义：

>验收测试是指确认一系统是否符合设计规格或契约之需求内容的测试。
>
>>[**_`验收测试`_**](https://zh.wikipedia.org/zh-cn/%E9%AA%8C%E6%94%B6%E6%B5%8B%E8%AF%95)

### 回归测试（`Regression Testing`）

- 定义：指修复缺陷后重新进行的测试，该测试用以验证修改后是否引入新缺陷或导致其他相关组件产生缺陷
- 目的：确保缺陷已被修复并且没有引入新的缺陷

## 测试类型-其他

### 静态测试（`Static Testing`）

狭义的静态测试也称为：静态程序分析（`Static Program Analysis`）、静态代码分析（`Static Code Analysis`）、静态分析（`Static Analysis`）。

广义的静态测试还包括：人工的评审（`Review`）等。

静态测试指在不运行被测软件的情况下，通过分析或检查软件（源代码或目标代码或文档或数据）以验证软件的正确性、找出其中的缺陷。

参考定义：

>静态测试是指在不运行计算机程序的条件下，进行程序分析的方法
>
>[**_`中文维基百科 - 静态程序分析`_**](https://zh.wikipedia.org/zh-cn/%E9%9D%9C%E6%85%8B%E7%A8%8B%E5%BA%8F%E5%88%86%E6%9E%90)

### 动态测试（`Dynamic Testing`）

动态测试也称为：动态程序分析（`Dynamic Program Analysis`）、动态分析（`Dynamic Analysis`）

动态测试是指通过运行待测程序，从而检查运行结果与预期结果的差异的一类测试方法。

参考定义：

>Dynamic testing (or dynamic analysis) is a term used in software engineering to describe the testing of the dynamic behavior of code.
>
>>[**_`Wikipedia - Dynamic Testing`_**.](https://en.wikipedia.org/wiki/Dynamic_testing)

### 人工测试（`Manual Testing`, `MT`）

人工测试也称为：手动测试。

人工测试指需要人工检查介入到测试过程的一类测试方法。

### 自动化测试（`Automated Testing`/`Test Automation`, `AT`）

>自动化测试指的是使用独立于待测软件的其他软件来自动执行测试、比较实际结果与预期并生成测试报告这一过程。
>
>>[**_`中文维基百科 - 自动化测试`_**.](https://zh.wikipedia.org/wiki/%E8%87%AA%E5%8A%A8%E5%8C%96%E6%B5%8B%E8%AF%95)

## 具体测试

### 功能测试`Function Test`

### 性能测试`Performance Test`

### 负载测试`Load Test`/容量测试

### 压力测试`Stress Test`

### 冒烟测试/预测试`Smoke Test`

### 配置测试`Configuration Test`

### 安全测试`Security Test`

### 保护测试`Protection Test`

### 界面测试`User Interface Test`

### 可用性测试`Usability Test`

### 可安装性测试`Installation Test`

### 备份测试`Backup Test`

### 健壮性测试`Robustness Test`

### 文档测试`Documentation Test`

### 在线帮助测试`Online Help Test`

## 研发环境Environment

### 开发环境Development Environment（`dev`）

一般情况下，开发环境是程序员的本地环境。

程序员在本地环境中进行本地的单元测试和本地的集成测试（该集成测试可能是子系统的集成测试）。

### 测试环境Test Environment（`test`）

一般情况下，测试环境是远程环境。

#### 系统集成测试环境System Integration Test Environment（`sit`）

#### 特性验收测试环境Feature Acceptance Test Environment（`fat`）

#### 用户验收测试环境User Acceptance Test Environment（`uat`）

### 生产环境Production Environment（`pro`）

#### 预发布环境Pre-Production Environment（`pre`）

#### 灰度发布环境Gray-Scale Environment
