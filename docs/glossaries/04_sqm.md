# 软件质量管理领域术语

## 软件质量管理（`Software Quality Management`, `SQM`）

参考定义：

>软件质量管理旨在确保软件产品、服务和生命周期过程既能满足软件质量目标，又能实现利益相关者满意度。
>
>SQM可以划分为４个子类型，包括软件质量规划（SQP）、软件质量保证（SQA）、软件质量控制（SQC）和软件过程改进（SPI）。
>
>>**_`C-SWEBOK:2018`_**.157-157.

参考定义：

>Software quality management (SQM) is a management process that aims to develop and manage the quality of software in such a way so as to best ensure that the product meets the quality standards expected by the customer while also meeting any necessary regulatory and developer requirements, if any.
>
>[**_`Wikipedia - Software Quality Management`_**.](https://en.wikipedia.org/wiki/Software_quality_management)

## 软件质量工程体系（`Software Quality Engineering System`）

软件质量工程体系即适用于软件行业的质量管理体系。

## 质量管理体系（`Quality Management System`, `QMS`）

参考定义：

>质量管理体系：
>
>1. 质量管理体系包括组织确定其目标以及为获得期望的结果确定其过程和所需资源的活动。
>1. 质量管理体系管理相互作用的过程和所需的资源，以向有关相关方提供价值并实现结果。
>1. 质量管理体系能够使最高管理者通过考虑其决策的长期和短期影响而优化资源的利用。
>1. 质量管理体系给出了在提供产品和服务方面，针对预期和非预期的结果确定所采取措施的方法。
>
>>**_`GB/T 19000-2016/ISO 9000:2015`_**.1-1

## 软件质量计划（`Software Quality Plan`, `SQP`）

软件质量计划也称为：软件质量保证计划Software Quality Assurance Plan

>Quality planning works at a more granular, project-based level, defining the quality attributes to be associated with the output of the project and how those attributes should be assessed.
>
>[**_`Wikipedia - Quality Planning`._**](https://en.wikipedia.org/wiki/Software_quality_management#Quality_planning)

## 软件过程改进（`Software Process improvement`, `SPI`）

软件过程改进属于业务流程改进（Business Process Improvement, `BPI`）在软件领域的应用。

## 能力成熟度模型（`Capability Maturity Model`, `CMM`）

`CMM`是`SW-CMM(Software - Capability Maturity Model)`的简称。

`CMM`是由卡内基美隆大学（`Carnegie Mellon University`, `CMU`）的软件工程学院（`Software Engineering Institute`, `SEI`）于1991年正式发表的一个用于软件开发组织的`QMS`。

> `CMM`涵盖一个成熟的软件发展组织所应具备的重要功能与项目，它描述了软件发展的演进过程，从毫无章法、不成熟的软件开发阶段到成熟软件开发阶段的过程。以`CMM`的架构而言，它涵盖了规划、软件工程、管理、软件开发及维护等技巧，若能确实遵守规定的关键技巧，可协助提升软件部门的软件设计能力，达到成本、时程、功能与品质的目标。
>
>`SW-CMM`的核心是以软件开发流程作为全面品质管理（`Total Quality Management`, `TQM`）改善的架构，提升组织管理软件开发的能力。
>
>>[**_`中文维基百科 - 能力成熟度模型`._**](https://zh.wikipedia.org/zh-cn/%E8%83%BD%E5%8A%9B%E6%88%90%E7%86%9F%E5%BA%A6%E6%A8%A1%E5%9E%8B)

## 能力成熟度模型集成（`Capability Maturity Model Integration`,`CMMI`）

>>[**_`中文维基百科 - 能力成熟度模型集成`._**](https://zh.wikipedia.org/zh-cn/%E8%83%BD%E5%8A%9B%E6%88%90%E7%86%9F%E5%BA%A6%E6%A8%A1%E5%9E%8B%E9%9B%86%E6%88%90)

<!--1. 关键过程域: KPA,Key Process Area-->

## 全面质量管理（`Total Quality Management`, `TQM`）

全面质量管理指在社会全面的推动下，企业中所有部门、所有组织、所有人员都以产品质量为核心，把专业技术、管理技术、数理统计技术集合在一起，建立起一套科学、严密、高效的质量保证体系，控制生产过程中影响质量的全过程和各因素，以优质的工作、最经济的办法提供满足用户需要的产品的全部活动。

## 活动（`Activity`）

>活动：相关任务的集合
>
>**_`GB/T 19003-2008`_**.2-2

## 基线（`Base Line`）

>基线是指一组已经过正式审核的规格说明或软件产品，表现为软件生命周期 内特定时刻被证实确定下来的一组配置项及其配置标识，它们可作为下一步开发 的基础，只有通过正式的修改管理过程方能加以修改（即如需对基线中包含的配 置项进行修改，必须经过证实的申请、审查和认可）。
>
>C-SWEBOK:2018. 61-61

## 软件配置项（`Software Configuration Item`, `SCI`）

>某个配置中的实体，它满足一项最终使用功能，并在给定的参考点上能够唯一地加以标识。
>
>`IEEE/ISO/IEC 12007:1995`

## 软件配置管理（`Software Configuration Management`, `SCM`）

>软件配置管理标识软件的各组成部分，对各部分的变更进行管控（版本管理与控制），维护各组成部分之间的联系，使得软件在开发过程中任一时刻的状态都可被追溯。
>
>**_`C-SWEBOK-2018`_**.92-92.
