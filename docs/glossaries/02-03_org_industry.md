# 行业组织/行业标准相关术语

## 中华人民共和国国家军用标准（GuoJunBiao, `GJB`）

## 美国军用标准（Military-Standards, `MIL-S`）

## 美国电气和电子工程师学会（Institute Of Electrical and Electronics Engineers, `IEEE`）

## 美国能源信息署（U.S. Energy Information Administration, `EIA`）

## 美国国防部标准（Department of Defense-Standards, `DOD-STD`）
