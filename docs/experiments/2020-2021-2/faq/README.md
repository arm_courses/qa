# Experiment FAQ

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Eclipse](#eclipse)
    1. [:question: 如何指定`Eclipse`的运行`JVM`](#question-如何指定eclipse的运行jvm)
    2. [:broken_heart: `Eclipse`更改`project`的`Java Build Path`时，出现`Could not write file xxx\.classpath. xx\.classpath (Access is denied)`](#broken_heart-eclipse更改project的java-build-path时出现could-not-write-file-xxxclasspath-xxclasspath-access-is-denied)
    3. [:broken_heart: `Eclipse`运行`JUnit Test`时，出现`class not found`](#broken_heart-eclipse运行junit-test时出现class-not-found)
2. [Jenkins](#jenkins)
    1. [:question: `Windows`下，通过`service`方式运行`Jenkins2`，如何修改`JENKINS_HOME`路径](#question-windows下通过service方式运行jenkins2如何修改jenkins_home路径)
    2. [:broken_heart: `Windows`下，通过`service`方式运行`Jenkins2`，首次运行时，出现`解锁Jenkins`页面，要求输入密码，但提示的路径下没有`initialAdminPassword`文件或该文件为空文件](#broken_heart-windows下通过service方式运行jenkins2首次运行时出现解锁jenkins页面要求输入密码但提示的路径下没有initialadminpassword文件或该文件为空文件)

<!-- /code_chunk_output -->

## Eclipse

### :question: 如何指定`Eclipse`的运行`JVM`

:key: 编辑`<eclipse install folder>/eclipse.ini`，在`-vmargs`之上插入以下代码

```bash {.line-numbers}
-vm
<JVM path> # eg. C:\Program Files\Java\jdk1.6.0_02\bin\
```

1. :warning: `-vm`与`<JVM path>`不能在同一行内
1. :warning: `-vm`须在`-vmargs`前面

>Note the format of the `-vm` option - it is important to be exact:
>
>1. The `-vm` option and its value (the path) must be on separate lines.
>1. The value must be the full absolute or relative path to the Java executable, not just to the Java home directory.
>1. The `-vm` option must occur after the other Eclipse-specific options (such as `-product`, `--launcher.*`, etc), but before the `-vmargs` option, since everything after `-vmargs` is passed directly to the JVM.
>1. For the 32-bit Eclipse executable (eclipse.exe on Windows) a 32-bit JVM must be used and for the 64-bit Eclipse executable a 64-bit JVM must be used. 32-bit Eclipse will not work with a 64-bit JVM.
>
><https://wiki.eclipse.org/Eclipse.ini>

### :broken_heart: `Eclipse`更改`project`的`Java Build Path`时，出现`Could not write file xxx\.classpath. xx\.classpath (Access is denied)`

:key: 在`Windows Explorer`中将工程目录下的`.classpath`的“文件属性”的`隐藏属性`去掉。

### :broken_heart: `Eclipse`运行`JUnit Test`时，出现`class not found`

:key: `Preferences -> Java -> Compiler -> Building -> Build path problems`中将`incomplete build path`和`Circular dependencies`的值设置成`warning`

## Jenkins

### :question: `Windows`下，通过`service`方式运行`Jenkins2`，如何修改`JENKINS_HOME`路径

:key: 打开`"jenkins_install_folder" >>> jenkins.xml`（`"jenkins_install_folder"`为`jenkins`的安装目录），将`<env name="JENKINS_HOME" value="%LocalAppData%\Jenkins\.jenkins"/>`中的`value`值修改为要修改的路径，如`<env name="JENKINS_HOME" value="C:\etc\.jenkins"/>`

:point_right: `JENKINS_HOME`是`jenkins`运行数据存放目录，需要确保运行`jenkins`的账户有`read/write`权限

### :broken_heart: `Windows`下，通过`service`方式运行`Jenkins2`，首次运行时，出现`解锁Jenkins`页面，要求输入密码，但提示的路径下没有`initialAdminPassword`文件或该文件为空文件

:key: 修改`JENKINS_HOME`路径并重启`jenkins`服务，确保运行`jenkins`的账户有`read/write`权限
