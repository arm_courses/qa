# QA-E02：基于PMD的静态测试Static Testing Based On PMD

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [实验基本信息](#实验基本信息)
2. [实验前提条件与储备知识](#实验前提条件与储备知识)
3. [实验注意事项](#实验注意事项)
4. [实验所需资源](#实验所需资源)
5. [实验操作指引](#实验操作指引)
    1. [实验内容01](#实验内容01)
        1. [:microscope: 实验内容01具体任务](#microscope-实验内容01具体任务)
        2. [:ticket: 实验内容01参考截图](#ticket-实验内容01参考截图)
            1. [实验内容01参考截图（Eclipse）](#实验内容01参考截图eclipse)
    2. [实验内容02](#实验内容02)
        1. [:microscope: 实验内容02具体任务](#microscope-实验内容02具体任务)
        2. [:ticket: 实验内容02参考截图](#ticket-实验内容02参考截图)
            1. [Eclipse参考截图](#eclipse参考截图)
    3. [实验内容03](#实验内容03)
        1. [:microscope: 实验内容03具体任务](#microscope-实验内容03具体任务)
        2. [**:ticket: 实验内容03参考截图**](#ticket-实验内容03参考截图)
6. [延伸阅读](#延伸阅读)

<!-- /code_chunk_output -->

## 实验基本信息

1. 实验性质：设计性
1. 实验学时：2
1. 实验目的：
    1. 掌握`静态测试（static testing）`的相关概念与知识
    1. 熟悉`Java`语言的`代码规范`，增强代码编写规范意识
    1. 熟悉`PMD`静态代码分析工具
1. 实验内容与要求：
    1. 实验内容01： `PMD plugin`安装
    1. 实验内容02： 使用`PMD`对代码进行静态分析
    1. 实验内容03： 安装`Alibaba P3C`并对代码进行静态分析
1. 实验条件：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 软件环境：`Eclipse`或`Intellij IDEA`（简称：`IDEA`）

## 实验前提条件与储备知识

1. 掌握`Java`基本编程
1. 掌握`Eclipse`或`IDEA`基本操作
1. 了解`coding style`基本概念

## 实验注意事项

1. 实验指引中使用尖括号`<>`表示键盘按键，如：`<CTRL>`代表control键，`<j>`代表j键
1. 实验中请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”
1. 实验内容与实验指引中各子内容包含三部分：
    1. **:microscope: 实验内容具体任务** ==> 实验的具体任务及步骤
    1. **:ticket: 实验内容参考截图** ==> 实验的参考结果截图
    1. **:bookmark: 实验内容相关知识** ==> 实验涉及的相关知识

## 实验所需资源

1. `IDE`: `Eclipse` or `IDEA`
1. `static testing tools`：[`PMD`](https://pmd.github.io/), [`P3C`](https://github.com/alibaba/p3c)
1. `source code`: [pmd-eclipse-plugin/test-projects/project1/](https://github.com/pmd/pmd-eclipse-plugin/tree/master/test-projects/project1)

## 实验操作指引

### 实验内容01

#### :microscope: 实验内容01具体任务

1. `Eclipse`在线安装方式安装`PMD`
    1. `Help >>> Install New Software...`
    1. enter this update site URL: <https://dl.bintray.com/pmd/pmd-eclipse-plugin/updates/>
    1. install `PMD for eclipse`
1. `Intellij IDEA`在线安装方式安装`PMD`（:warning: `Intellij IDEA`的`PMD plugin`没有`Suspect Cut and Paste`功能，但`Intellij IDEA Ultimate`本身具有类似的功能）
    1. `File >>> Settings >>> Plugins`
    1. search `PMD`
    1. select and install `PMDPlugin`

#### :ticket: 实验内容01参考截图

##### 实验内容01参考截图（Eclipse）

![](./assets_image/install_pmd_for_eclipse.png)

### 实验内容02

#### :microscope: 实验内容02具体任务

在`pmd-eclipse-plugin/test-projects/project1/`的基础上设计一个简单的程序进行`Suspect Cut and Paste`和`Check Code`分析

1. `git clone`或`download` <https://github.com/pmd/pmd-eclipse-plugin.git>
1. `Eclipse`
    1. 在`Eclipse`中导入`pmd-eclipse-plugin/test-projects/project1/`
    1. 对`/project1/src/project1/TestViolation.java`进行简单编码
    1. 右击`project1`并运行`PMD >>> Find Suspect Copy and Paste`（`language`选择`java`）
    1. 右击`project1`并运行`PMD >>> Check Code`
1. `Intellij IDEA`
    1. 在`Intellij IDEA`中导入`pmd-eclipse-plugin/test-projects/project1/`
    1. 对`/project1/src/project1/TestViolation.java`进行简单编码
    1. 右击`project1`并运行`Analyze >>> Locate Duplicates`（:warning: `Intellij IDEA Community`没有该功能）
    1. 右击`project1`并运行`Run PMD >>> Pre Defined >>> All`

#### :ticket: 实验内容02参考截图

##### Eclipse参考截图

![](./assets_image/demo_pmd_code.png)

![](./assets_image/PMD_find_suspect_copy_and_paste_01.png)

![](./assets_image/PMD_find_suspect_copy_and_paste_02.png)

![](./assets_image/PMD_find_suspect_copy_and_paste_result.png)

![](./assets_image/PMD_check_code.png)

![](./assets_image/PMD_check_code_result.png)

### 实验内容03

#### :microscope: 实验内容03具体任务

在线安装方式安装`Alibaba P3C`并对`实验内容02`的代码进行静态分析

1. `Eclipse`
    1. `Help >>> Install New Software...`
    1. enter this update site URL: <https://p3c.alibaba.com/plugin/eclipse/update>
    1. install `Smartfox Eclipse Plugin`
1. `Intellij IDEA`
    1. `File >>> Settings >>> Plugins`
    1. search `Alibaba`
    1. select and install `Alibaba Java Coding Guidelines`

#### **:ticket: 实验内容03参考截图**

![](./assets_image/install_alibaba_p3c.png)

![](./assets_image/P3C_analysis.png)

![](./assets_image/P3C_analysis_result.png)

## 延伸阅读

1. PMD
    1. PMD wikipedia: <https://en.wikipedia.org/wiki/PMD_(software)>
    1. PMD homepage: <https://pmd.github.io/>
        1. PMD eclipse plugin guide: <https://pmd.github.io/latest/pmd_userdocs_tools.html#eclipse>
        1. PMD For Eclipse: <https://github.com/pmd/pmd-eclipse-plugin>
        1. PMD Java Rules: <https://pmd.github.io/latest/pmd_rules_java.html>
1. Alibaba Java Coding Guidelines
    1. Alibaba Java Coding Guidelines pmd implements and IDE plugin: <https://github.com/alibaba/p3c>
    1. 华山版《Java开发手册》独家讲解: <https://developer.aliyun.com/live/1201?spm=a2c6h.14059151.1371164.1.555057388W9RmS>
    1. 《阿里巴巴Java开发规约》插件使用详细指南: <https://developer.aliyun.com/article/705264?spm=a2c6h.14059151.1371227.5.555057388W9RmS>

<!--
1. <https://github.com/ChristianWulf/qa-eclipse-plugin>
<https://sourceforge.net/projects/pmd/files/pmd-eclipse/update-site/>
-->
