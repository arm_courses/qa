# QA-E01：基于JUnit的单元测试Unit Testing Based On JUnit

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [实验基本信息](#实验基本信息)
2. [实验前提条件与储备知识](#实验前提条件与储备知识)
3. [延伸阅读](#延伸阅读)

<!-- /code_chunk_output -->

## 实验基本信息

1. 实验性质：设计性
1. 实验学时：4
1. 实验目的：
    1. 掌握`unit test`的基本概念、主流工具
    1. 掌握`JUnit`的基本使用
    1. 掌握`EclEmma`的基本使用
1. 实验内容与要求：
    1. 实验内容01： 安装部署`Eclipse`、`JUnit`、`EclEmma`
    1. 实验内容02： 使用`JUnit`对`Jeesite`中的`Java Class`进行`unit test`
    1. 实验内容03： 使用`EclEmma`对`unit test`进行覆盖分析
1. 实验条件：
    1. 硬件环境：`PC`(`Personal Computer`)
    1. 软件环境：`Eclipse` + `JUnit` + `EclEmma`

## 实验前提条件与储备知识

1. 掌握`Java`基本编程
1. 掌握`Eclipse`的基本操作

## 延伸阅读

1. EclEmma homepage: <https://www.eclemma.org/>
